﻿using HesaEngine.SDK;
using HesaEngine.SDK.GameObjects;
using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GodDammAIO.Awareness.Trackers
{
    internal class WardTracker
    {
        #region Static Fields
        private List<WardInfo> wards = new List<WardInfo>();
        #endregion

        private bool timer = true;
        private bool range = false;

        public WardTracker()
        {
            var wardMenu = Awareness.Menu.AddSubMenu("Wards");
            wardMenu.Add(new MenuCheckbox("timer", "Draw remaining time", true)).OnValueChanged += (sender, value) => { timer = value; };
            wardMenu.Add(new MenuKeybind("range", "Draw wards range", SharpDX.DirectInput.Key.Z, MenuKeybindType.Toggle)).OnValueChanged += (sender, value) => { range = value; };

            Drawing.OnEndScene += DrawWards;
            GameObject.OnCreate += GameObject_OnCreate;
            GameObject.OnDelete += GameObject_OnDelete;
        }

        #region Methods
        private void DrawWardMinimap(Vector3 center, Color color)
        {
            /*
            var centerMap = center.WorldToMinimap();
            var a = new Vector2(centerMap.X - 5, centerMap.Y);
            var b = new Vector2(centerMap.X + 5, centerMap.Y);
            var c = new Vector2(centerMap.X, centerMap.Y + 10);

            Drawing.DrawLine(a, b, 2f, color);
            Drawing.DrawLine(b, c, 2f, color);
            Drawing.DrawLine(c, a, 2f, color);
            */
        }

        private void DrawWards(EventArgs args)
        {
            var removeList = new List<WardInfo>();

            foreach (var wardInfo in wards)
            {
                if (!wardInfo.Available)
                {
                    removeList.Add(wardInfo);
                    continue;
                }
                var buff = wardInfo.Ward.Buffs.FirstOrDefault();
                if (buff == null)
                {
                    removeList.Add(wardInfo);
                    continue;
                }

                var remaining = buff.EndTime - Game.Time;
                if (remaining > 0 || wardInfo.IsPink || wardInfo.IsBlue)
                {
                    var radius = range ? 1100 : 60;
                    Drawing.DrawCircle(wardInfo.Position, radius, wardInfo.Color, 1);
                    DrawWardMinimap(wardInfo.Position, wardInfo.Color);

                    if (timer)
                    {
                        var location = Drawing.WorldToScreen(wardInfo.Position);
                        if (!wardInfo.IsPink && !wardInfo.IsBlue)
                        {
                            Drawing.DrawText(string.Format("{0:0}", remaining), new Vector2(location.X, location.Y), wardInfo.Ward.IsEnemy ? Color.Red : Color.White);
                        }
                    }
                }
                else
                {
                    removeList.Add(wardInfo);
                }
            }
            if (removeList.Any())
            {
                wards = wards.Except(removeList).ToList();
            }
        }

        private void GameObject_OnCreate(GameObject sender, EventArgs args)
        {
            var ward = sender as Obj_AI_Minion;

            if (ward == null || !sender.Name.ToLower().Contains("ward"))
            {
                return;
            }

            if (ward.Name.ToLower().Contains("wardcorpse"))
            {
                return;
            }

            if (ward.IsAlly)
            {
                //return;
            }

            //Console.WriteLine("Ward created: " + ward.BaseSkinName);

            switch (ward.BaseSkinName)
            {
                case "VisionWard":
                    wards.Add(new WardInfo(ward, true, false));
                break;
                case "YellowTrinket":
                    wards.Add(new WardInfo(ward, false, false));
                break;
                case "BlueTrinket":
                    wards.Add(new WardInfo(ward, false, true));
                break;
                default:
                    wards.Add(new WardInfo(ward, false, false));
                break;
            }
        }

        private void GameObject_OnDelete(GameObject sender, EventArgs args)
        {
            var ward = sender as Obj_AI_Minion;

            var wardInfo = wards.FirstOrDefault(w => w.Ward == ward);

            if (wardInfo != null)
            {
                wardInfo.Available = false;
            }
        }
        #endregion

        internal class WardInfo
        {
            #region Constructors and Destructors

            public WardInfo(Obj_AI_Minion ward, bool isPink, bool isBlue)
            {
                Ward = ward;
                Color = isPink ? Color.Magenta : (isBlue ? Color.CornflowerBlue : Color.Yellow);
                Available = true;
                Position = ward.Position;
                IsPink = isPink;
                IsBlue = isBlue;
            }

            #endregion

            #region Public Properties
            
            public bool Available { get; set; }

            public Color Color { get; set; }

            public bool IsPink { get; set; }

            public bool IsBlue { get; set; }

            public Vector3 Position { get; set; }

            public Obj_AI_Minion Ward { get; set; }

            #endregion
        }
    }
}