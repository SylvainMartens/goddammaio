﻿using System;
using System.Collections.Generic;
using System.Linq;
using SharpDX;
using HesaEngine.SDK;
using GodDammAIO.Evade.Spells;
using HesaEngine.SDK.GameObjects;
using GodDammAIO.Evade.Utils;
using GodDammAIO.Evade.Helpers;
using HesaEngine.SDK.Enums;
using SpellType = GodDammAIO.Evade.Spells.SpellType;
using HesaEngine.SDK.Args;

namespace GodDammAIO.Evade.Tests
{
    class SpellTester
    {
        public static Menu menu;
        public static Menu selectSpellMenu;

        private static AIHeroClient myHero { get { return ObjectManager.Player; } }

        private static Dictionary<string, Dictionary<string, SpellData>> spellCache = new Dictionary<string, Dictionary<string, SpellData>>();

        public static Vector3 spellStartPosition = myHero.ServerPosition;
        public static Vector3 spellEndPostion = myHero.ServerPosition + (myHero.Direction.To2D().Perpendicular() * 500).To3D();

        public static float lastSpellFireTime = 0;

        public SpellTester()
        {
            menu = Menu.AddMenu("Spell Tester");

            selectSpellMenu = menu.AddSubMenu("Select Spell");

            Menu setSpellPositionMenu = menu.AddSubMenu("Set Spell Position");
            setSpellPositionMenu.Add(new MenuCheckbox("SetDummySpellStartPosition", "Set Start Position").SetValue(false));
            setSpellPositionMenu.Add(new MenuCheckbox("SetDummySpellEndPosition", "Set End Position").SetValue(false));
            setSpellPositionMenu.Item("SetDummySpellStartPosition").ValueChanged += OnSpellStartChange;
            setSpellPositionMenu.Item("SetDummySpellEndPosition").ValueChanged += OnSpellEndChange;
            


            Menu fireDummySpellMenu = menu.AddSubMenu("Fire Dummy Spell");
            fireDummySpellMenu.Add(new MenuKeybind("FireDummySpell", "Fire Dummy Spell Key").SetValue(new KeyBind(SharpDX.DirectInput.Key.O, MenuKeybindType.Hold)));

            fireDummySpellMenu.Add(new MenuSlider("SpellInterval", "Spell Interval").SetValue(new Slider(0, 5000, 2500)));
            
            LoadSpellDictionary();

            Game.OnUpdate += Game_OnGameUpdate;
            Drawing.OnDraw += Drawing_OnDraw;
        }

        private void Drawing_OnDraw(EventArgs args)
        {
            foreach (var spell in SpellDetector.drawSpells.Values)
            {
                Vector2 spellPos = spell.currentSpellPosition;

                if (spell.heroID == myHero.NetworkId)
                {
                    if (spell.spellType == SpellType.Line)
                    {
                        if (spellPos.Distance(myHero) <= myHero.BoundingRadius + spell.radius
                            && EvadeUtils.TickCount - spell.startTime > spell.info.spellDelay
                            && spell.startPos.Distance(myHero) < spell.info.range)
                        {
                            Draw.RenderObjects.Add(new Draw.RenderCircle(spellPos, 1000, Color.Red,
                                (int)spell.radius, 10));
                            DelayAction.Add(1, () => SpellDetector.DeleteSpell(spell.spellID));
                        }
                        else
                        {
                            Drawing.DrawCircle(new Vector3(spellPos.X, myHero.Position.Y, spellPos.Y), (int)spell.radius, Color.White, 5);
                        }
                    }
                    else if (spell.spellType == SpellType.Circular)
                    {
                        if (EvadeUtils.TickCount - spell.startTime >= spell.endTime - spell.startTime)
                        {
                            if (myHero.ServerPosition.To2D().InSkillShot(spell, myHero.BoundingRadius))
                            {
                                Draw.RenderObjects.Add(new Draw.RenderCircle(spellPos, 1000, Color.Red, (int)spell.radius, 5));
                                DelayAction.Add(1, () => SpellDetector.DeleteSpell(spell.spellID));
                            }
                        }
                    }
                    else if (spell.spellType == SpellType.Cone)
                    {
                        // SPELL TESTER
                        if (EvadeUtils.TickCount - spell.startTime >= spell.endTime - spell.startTime)
                        {
                            if (myHero.ServerPosition.To2D().InSkillShot(spell, myHero.BoundingRadius))
                            {
                                TacticalMap.ShowPing(PingCategory.Danger, myHero.Position);
                                DelayAction.Add(1, () => SpellDetector.DeleteSpell(spell.spellID));
                            }
                        }
                    }
                }
            }
        }

        private void Game_OnGameUpdate()
        {
            if (menu.Item("FireDummySpell").GetValue<bool>() == true)
            {
                float interval = menu.Item("SpellInterval").GetValue<int>();

                if (EvadeUtils.TickCount - lastSpellFireTime > interval)
                {
                    var charName = selectSpellMenu.Get<MenuCombo>("DummySpellHero").CurrentValueText;
                    var spellName = selectSpellMenu.Get<MenuCombo>("DummySpellList").CurrentValueText;
                    var spellData = spellCache[charName][spellName];

                    if (!ObjectCache.menuCache.cache.ContainsKey(spellName + "DodgeSpell"))
                    {
                        SpellDetector.LoadDummySpell(spellData);
                    }

                    SpellDetector.CreateSpellData(myHero, spellStartPosition, spellEndPostion, spellData);
                    lastSpellFireTime = EvadeUtils.TickCount;
                }
            }
        }

        private void OnSpellEndChange(object sender, OnValueChangeEventArgs e)
        {
            e.Process = false;

            spellEndPostion = myHero.ServerPosition;
            Draw.RenderObjects.Add(new Draw.RenderCircle(spellEndPostion.To2D(), 1000, Color.Red, 100, 20));
        }

        private void OnSpellStartChange(object sender, OnValueChangeEventArgs e)
        {
            e.Process = false;

            spellStartPosition = myHero.ServerPosition;
            Draw.RenderObjects.Add(new Draw.RenderCircle(spellStartPosition.To2D(), 1000, Color.Red, 100, 20));
        }

        private void LoadSpellDictionary()
        {
            foreach (var spell in Spells.SpellDatabase.Spells)
            {
                if (spellCache.ContainsKey(spell.charName))
                {
                    var spellList = spellCache[spell.charName];
                    if (spellList != null && !spellList.ContainsKey(spell.spellName))
                    {
                        spellList.Add(spell.spellName, spell);
                    }
                }
                else
                {
                    spellCache.Add(spell.charName, new Dictionary<string, SpellData>());
                    var spellList = spellCache[spell.charName];
                    if (spellList != null && !spellList.ContainsKey(spell.spellName))
                    {
                        spellList.Add(spell.spellName, spell);
                    }
                }
            }

            selectSpellMenu.AddSeparator("Select A Dummy Spell To Fire");

            var heroList = spellCache.Keys.ToArray();
            selectSpellMenu.Add(new MenuCombo("DummySpellHero", "Hero")
               .SetValue(new StringList(heroList, 0)));

            var selectedHeroStr = selectSpellMenu.Get<MenuCombo>("DummySpellHero").CurrentValueText;
            var selectedHero = spellCache[selectedHeroStr];
            var selectedHeroList = selectedHero.Keys.ToArray();

            selectSpellMenu.Add(new MenuCombo("DummySpellList", "Spell")
               .SetValue(new StringList(selectedHeroList, 0)));

            selectSpellMenu.Item("DummySpellHero").ValueChanged += OnSpellHeroChange;
        }

        private void OnSpellHeroChange(object sender, OnValueChangeEventArgs e)
        {
            //var previousHeroStr = e.GetOldValue<StringList>().SelectedValue;
            var menuCombo = (MenuCombo)sender;
            if (menuCombo == null) return;

            var selectedHeroStr = menuCombo.CurrentValueText;
            var selectedHero = spellCache[selectedHeroStr];
            var selectedHeroList = selectedHero.Keys.ToArray();

            selectSpellMenu.Item("DummySpellList").SetValue(new StringList(selectedHeroList, 0));
        }
    }
}