﻿using GodDammAIO.Menus;
using HesaEngine.SDK;
using HesaEngine.SDK.Enums;
using HesaEngine.SDK.GameObjects;
using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using static GodDammAIO.Configs;

namespace GodDammAIO.Champions
{
    internal class Katarina : ChampionScript
    {
        bool Rult;
        bool _isChannelingImportantSpell;
        bool _isUlting;
        Obj_AI_Base _LastMinion;

        public Katarina()
        {
            Q = new Spell(SpellSlot.Q, 625, TargetSelector.DamageType.Magical);
            W = new Spell(SpellSlot.W, 380, TargetSelector.DamageType.Magical);
            E = new Spell(SpellSlot.E, 725, TargetSelector.DamageType.Magical);
            R = new Spell(SpellSlot.R, 550, TargetSelector.DamageType.Magical);
            Q.SetTargetted(0.5f, 1500f);
            E.SetTargetted(0.1f, 75f);

            CreateChampionMenu();
            ComboOption.AddQ();
            ComboOption.AddW();
            ComboOption.AddE();
            ComboOption.AddR();

            LastHitOption.AddQ();

            LaneClearOption.AddQ();
            LaneClearOption.AddW();
            LaneClearOption.AddE();

            KillStealOption.AddBool("useKS", "Enable Kill Steal", true);

            FleeOption.AddW();

            DrawOption.AddQ();
            DrawOption.AddW();
            DrawOption.AddE();
            DrawOption.AddR();
            DrawOption.AddEvent();

            Game.OnUpdate += Game_OnUpdate;
            Obj_AI_Base.OnBuffGained += Obj_AI_Base_OnBuffGained;
            Obj_AI_Base.OnBuffLost += Obj_AI_Base_OnBuffLost;
        }

        private void Obj_AI_Base_OnBuffLost(Obj_AI_Base sender, HesaEngine.SDK.Args.Obj_AI_BaseBuffLostEventArgs args)
        {
            if (sender.IsMe && args.Buff.Name.ToLower() == "katarinarsound" || args.Buff.Name.ToLower() == "katarinar")
            {
                Orbwalker.Move = true;
                Orbwalker.Attack = true;
                _isUlting = false;
            }
        }

        private void Obj_AI_Base_OnBuffGained(Obj_AI_Base sender, HesaEngine.SDK.Args.Obj_AI_BaseBuffGainedEventArgs args)
        {
            if (sender.IsMe && args.Buff.Name.ToLower() == "katarinarsound" || args.Buff.Name.ToLower() == "katarinar" || _isChannelingImportantSpell)
            {
                Orbwalker.Move = false;
                Orbwalker.Attack = false;
                _isUlting = true;
            }
        }

        private void Game_OnUpdate()
        {
            if (KillStealOption.GetBool("useKS"))
            {
                Killsteal();
            }

            /* COMBO */
            if (IsComboMode)
            {
                Combo();
            }

            /* HARASS
            if (Orb.ActiveMode.Equals(Orbwalker.OrbwalkingMode.Harass))
            {
                Harass.ActivatedHarass();
            } TODO: IMPLEMENT */
            
            if (IsLaneClearMode)
            {
                Laneclear();
            }

            /* LAST HIT */
            if (IsLastHitMode)
            {
                LastHit();
            }

            /* JUNGLE CLEAR
            if (IsJungleClearMode)
            {
                Jungleclear();
            } TODO: IMPLEMENT */

            /* FLEE */
            if (IsFleeMode)
            {
                Flee();
            }
        }

        IEnumerable<Obj_AI_Base> GetDaggers()
        {
            return ObjectManager.Get<Obj_AI_Base>().Where(a => a.Name == "HiddenMinion" && a.IsValid() && a.Health == 100);
        }

        Vector3 GetClosestDagger()
        {
            var dagger = GetDaggers();
            if (!dagger.Any() || dagger == null || dagger.Count() <= 0) return new Vector3();
            var t = dagger.Where(p => p.Distance(Player) >= 125).OrderBy(p => p.Distance(Player.Position)).FirstOrDefault();
            return t == null ? new Vector3() : t.Position;
        }

        Vector3 GetBestDaggerPoint(Vector3 position, Obj_AI_Base target)
        {
            if (target.Position.IsInRange(position, 150)) return position;
            return position.Extend(target, 150);
        }

        private void Combo()
        {

            var q = ComboOption.UseQ;
            var w = ComboOption.UseW;
            var e = ComboOption.UseE;
            var r = ComboOption.UseR;

            var qtarget = TargetSelector.GetTarget(Q.Range);
            var wtarget = TargetSelector.GetTarget(W.Range);
            var rtarget = TargetSelector.GetTarget(R.Range);
            var etarget = TargetSelector.GetTarget(E.Range);
            
            //-----------------------------------------Basic Combo----------------------------------------------------------------------------//
            var d = GetClosestDagger();

            if (q && _isUlting != true)
            {
                if (qtarget != null)
                {
                    Q.Cast(qtarget);
                }
            }
            if (!Q.IsReady() && etarget != null && e && _isUlting != true && etarget.Position.IsInRange(d, W.Range))
            {
                E.Cast(GetBestDaggerPoint(d, etarget));
            }
            else if (!Q.IsReady() && etarget != null && _isUlting != true)

                if (W.IsReady() && wtarget != null && _isUlting != true)
                {
                    if (wtarget.IsValidTarget(W.Range))
                    {
                        W.Cast();
                    }
                }
            if (r && rtarget != null && _isUlting != true && !E.IsReady() && !W.IsReady())
            {
                R.Cast(rtarget);
            }
            /*if (E.IsReady() && etarget.IsInRange(Player.Position, E.Range) && etarget != null)
            {
                if (etarget.Position.IsInRange(d, W.Range)) E.Cast(GetBestDaggerPoint(d, etarget));
                else if (Player.Distance(etarget) >= W.Range && etarget != null && d != null)
                    E.Cast(etarget.Position);
            }
          */
            // EWQR *
            //----------------------------------------------------------------------------------------------------------------------------------//
        }

        void Flee()
        {
            var w = FleeOption.UseW && W.IsReady();
            if (w)
            {
                W.Cast();
            }
        }

        void Killsteal()
        {
            var d = GetClosestDagger();

            if (KillStealOption.GetBool("useKS"))
            {
                if (Q.IsReady() && E.IsReady())
                {
                    var _Target = ObjectManager.Heroes.Enemies.FirstOrDefault(e => e.IsValidTarget(E.Range) && Player.GetSpellDamage(e, SpellSlot.Q) + Player.GetSpellDamage(e, SpellSlot.E) + 100 >= e.Health);
                    if (_Target.IsValidTarget(E.Range))
                    {
                        _isUlting = false;
                        E.Cast(_Target.Position);
                        Q.Cast(_Target);
                        if (W.IsReady() && _Target != null)
                        {
                            W.Cast(_Target);
                        }
                    }
                }
                if (E.IsReady())
                {
                    var _Target = ObjectManager.Heroes.Enemies.FirstOrDefault(e => e.IsValidTarget(E.Range) && Player.GetSpellDamage(e, SpellSlot.E) + Player.GetAutoAttackDamage(e) + 50 >= e.Health);
                    if (_Target.IsValidTarget(E.Range))
                    {
                        if (_Target.Position.IsInRange(d, W.Range))
                        {
                            _isUlting = false;
                            E.Cast(GetBestDaggerPoint(d, _Target));
                        }
                        else
                        {
                            _isUlting = false;

                            E.Cast(_Target.Position);


                            if (W.IsReady() && _Target != null)
                            {
                                W.Cast(_Target);

                            }
                        }
                    }
                }
                if (Q.IsReady())
                {
                    var _Target = ObjectManager.Heroes.Enemies.FirstOrDefault(e => e.IsValidTarget(Q.Range) && Player.GetSpellDamage(e, SpellSlot.Q) + 120 >= e.Health);
                    if (_Target.IsValidTarget(Q.Range))
                    {
                        _isUlting = false;
                        Q.Cast(_Target);
                        if (W.IsReady() && _Target != null)
                        {
                            W.Cast(_Target);

                        }
                    }
                }
            }
        }

        void Laneclear()
        {
            var d = GetClosestDagger();
            var q = LaneClearOption.UseQ && Q.IsLearned && Q.IsReady();
            var e = LaneClearOption.UseE && E.IsLearned && E.IsReady();
            var w = LaneClearOption.UseE && E.IsLearned && W.IsReady();

            var minion = MinionManager.GetMinions(Q.Range);
            foreach (var m in minion)
            {
                if (q && m.IsValidTarget() && m.IsMinion)
                {
                    Q.Cast(m);
                }
                if (e && m.IsMinion && m.IsValidTarget())
                {
                    if (m.Position.IsInRange(d, W.Range))
                    {
                        E.Cast(GetBestDaggerPoint(d, m));

                    }
                    else
                    {

                        E.Cast(m.Position);
                    }
                }
                else if (w && m.IsMinion && m.IsValidTarget())
                {

                    W.Cast(m);
                }
            }
        }

        Obj_AI_Base _GetMinion(Spell daSpell)
        {
            var _Minions = MinionManager.GetMinions(daSpell.Range, MinionTypes.All, MinionTeam.Enemy).OrderBy(x => x.Health);
            if (_Minions.Count() > 0)
            {
                var Temp = _Minions.FirstOrDefault();
                if (Temp != null && Temp.IsValidTarget(daSpell.Range) && !Temp.IsDead)
                {
                    if (Temp.Equals(_LastMinion))
                    {
                        if (_Minions.Count() > 1) { Temp = _Minions.Skip(1).First(); } else { return null; }
                    }

                    if (daSpell.GetDamage(Temp) >= MinionHealthPrediction.GetHealthPrediction(Temp, Game.GameTimeTickCount, (int)Math.Ceiling(daSpell.Delay)))
                    {
                        _LastMinion = Temp;
                        return Temp;
                    }
                }
            }
            return null;
        }

        void LastHit()
        {
            if (LastHitOption.UseQ)
            {
                if (Q.IsReady())
                {
                    var Target = _GetMinion(Q);
                    if (Target != null && Target.IsValid())
                    {
                        Core.DelayAction(() => Q.Cast(Target), 1);
                    }
                }
            }
        }

    }
}