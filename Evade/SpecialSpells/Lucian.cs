﻿using GodDammAIO.Evade.Spells;
using HesaEngine.SDK.GameObjects;
using HesaEngine.SDK.Args;
using GodDammAIO.Evade.Helpers;
using HesaEngine.SDK;

namespace GodDammAIO.Evade.SpecialSpells
{
    class Lucian : ChampionPlugin
    {
        static Lucian()
        {

        }

        public void LoadSpecialSpell(SpellData spellData)
        {
            if (spellData.spellName == "LucianQ")
            {
                SpellDetector.OnProcessSpecialSpell += ProcessSpell_LucianQ;
            }
        }

        private static void ProcessSpell_LucianQ(Obj_AI_Base hero, GameObjectProcessSpellCastEventArgs args, SpellData spellData, SpecialSpellEventArgs specialSpellArgs)
        {
            if (spellData.spellName == "LucianQ")
            {
                if (args.Target.IsValid<Obj_AI_Base>())
                {
                    var target = args.Target as Obj_AI_Base;

                    float spellDelay = ((float)(350 - ObjectCache.gamePing)) / 1000;
                    var heroWalkDir = (target.ServerPosition - target.Position).Normalized();
                    var predictedHeroPos = target.Position + heroWalkDir * target.MovementSpeed * (spellDelay);
                    
                    SpellDetector.CreateSpellData(hero, args.Start, predictedHeroPos, spellData, null, 0);

                    specialSpellArgs.noProcess = true;
                }
            }
        }
    }
}