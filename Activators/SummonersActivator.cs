﻿using HesaEngine.SDK;

namespace GodDammAIO.Activators
{
    internal class SummonersActivator
    {
        static Menu SummonersMenu;
        SummonerBarrierActivator BarrierActivator;
        SummonerExhaustActivator ExhaustActivator;
        SummonerHealActivator HealActivator;
        SummonerSmiteActivator SmiteActivator;
        SummonerIgniteActivator IgniteActivator;

        public SummonersActivator(Menu menu)
        {
            SummonersMenu = menu.AddSubMenu("Summoners");

            BarrierActivator = new SummonerBarrierActivator(SummonersMenu);
            ExhaustActivator = new SummonerExhaustActivator(SummonersMenu);
            HealActivator = new SummonerHealActivator(SummonersMenu);
            IgniteActivator = new SummonerIgniteActivator(SummonersMenu);
            SmiteActivator = new SummonerSmiteActivator(SummonersMenu);
        }
    }
}