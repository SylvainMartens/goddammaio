﻿using GodDammAIO.Menus;
using HesaEngine.SDK;
using HesaEngine.SDK.Enums;
using HesaEngine.SDK.GameObjects;
using System.Linq;
using static GodDammAIO.Configs;

namespace GodDammAIO.Champions
{
    internal class Ashe : ChampionScript
    {
        public Ashe()
        {
            Q = new Spell(SpellSlot.Q);
            W = new Spell(SpellSlot.W, 1255f);
            E = new Spell(SpellSlot.E, 5000f);
            R = new Spell(SpellSlot.R, 2000f);

            W.SetSkillshot(0.25f, 60f, 2000f, true, SkillshotType.SkillshotCone);
            E.SetSkillshot(0.25f, 300f, 1400f, false, SkillshotType.SkillshotLine);
            R.SetSkillshot(0.25f, 130f, 1600f, true, SkillshotType.SkillshotLine);

            CreateChampionMenu();

            ComboOption.AddQ();
            ComboOption.AddBool("ComboSaveMana", "Save Mana to Cast Q");
            ComboOption.AddW();
            ComboOption.AddE();
            ComboOption.AddR();

            HarassOption.AddW();
            HarassOption.AddMana();
            HarassOption.AddTargetList();

            LaneClearOption.AddW();
            LaneClearOption.AddSlider("LaneClearWCount", "Use W| Min Hit Count >= x", 3, 1, 5);
            LaneClearOption.AddMana();

            JungleClearOption.AddQ();
            JungleClearOption.AddW();
            JungleClearOption.AddMana();
            
            FleeOption.AddMove();

            KillStealOption.AddW();
            KillStealOption.AddR();
            KillStealOption.AddTargetList();

            MiscOption.AddR();
            MiscOption.AddBool("AutoR", "Auto Cast Ult");
            //MiscOption.AddBool("Interrupt", "Interrupt Danger Spells");
            MiscOption.AddKey("SemiR", "Semi-manual R Key", SharpDX.DirectInput.Key.T);
            MiscOption.AddBool("AntiGapCloser", "Anti GapCloser");
            MiscOption.AddSlider("AntiGapCloserHp", "AntiGapCloser |When Player HealthPercent <= x%", 30);
            MiscOption.AddGapcloserTargetList();

            DrawOption.AddW();
            DrawOption.AddR();
            DrawOption.AddFarm();
            DrawOption.AddEvent();

            Game.OnUpdate += OnUpdate;
            Orbwalker.AfterAttack += AfterAttack;
            //Interrupt.
            AntiGapcloser.OnEnemyGapcloser += OnEnemyGapcloser;
        }

        private static void OnUpdate()
        {
            if (Player.IsDead || Player.IsRecalling())
            {
                return;
            }

            if (FleeOption.DisableMove && IsFleeMode)
            {
                Orbwalker.Move = false;
                return;
            }

            Orbwalker.Move = true;

            if (MiscOption.GetKey("SemiR"))
            {
                OneKeyR();
            }

            AutoRLogic();
            KillSteal();

            if (IsComboMode)
            {
                Combo();
            }

            if (IsHarassMode)
            {
                Harass();
            }

            if (IsFarmMode)
            {
                FarmHarass();

                if (IsLaneClearMode)
                    LaneClear();

                if (IsJungleClearMode)
                    JungleClear();
            }
        }

        private static void OneKeyR()
        {
            Orbwalker.MoveTo(Game.CursorPosition);

            if (R.IsReady())
            {
                var select = TargetSelector.GetSelectedTarget();
                var target = TargetSelector.GetTarget(R.Range);

                if (select != null && !target.HasBuffOfType(BuffType.SpellShield) && target.IsValidTarget(R.Range))
                {
                    R.PredCast(target);
                }
                else if (select == null && target != null && !target.HasBuffOfType(BuffType.SpellShield) && target.IsValidTarget(R.Range))
                {
                    R.PredCast(target);
                }
            }
        }

        private static void AutoRLogic()
        {
            if (MiscOption.GetBool("AutoR") && R.IsReady())
            {
                foreach (var target in ObjectManager.Heroes.Enemies.Where(x => x.IsValidTarget(R.Range)))
                {
                    if (!(target.DistanceToPlayer() > Orbwalker.GetRealAutoAttackRange(Player)) ||
                        !(target.DistanceToPlayer() <= 700) ||
                        !(target.Health > Player.GetAutoAttackDamage(target)) ||
                        !(target.Health < R.GetDamage(target) + Player.GetAutoAttackDamage(target) * 3) ||
                        target.HasBuffOfType(BuffType.SpellShield))
                    {
                        continue;
                    }

                    R.PredCast(target);
                    return;
                }
            }
        }

        private static void KillSteal()
        {
            if (KillStealOption.UseW && W.IsReady())
            {
                foreach (var target in ObjectManager.Heroes.Enemies.Where(x => x.IsValidTarget(W.Range)))
                {
                    if (!target.IsValidTarget(W.Range) || !(target.Health < W.GetDamage(target)))
                        continue;

                    if (target.DistanceToPlayer() <= Orbwalker.GetRealAutoAttackRange(Player) && Player.HasBuff("AsheQAttack"))
                    {
                        continue;
                    }

                    if (!target.IsUnKillable())
                    {
                        W.PredCast(target);
                        return;
                    }
                }
            }

            if (KillStealOption.UseR && R.IsReady())
            {
                foreach (var target in ObjectManager.Heroes.Enemies.Where(x => x.IsValidTarget(2000) && KillStealOption.GetKillStealTarget(x.ChampionName)))
                {
                    if (!(target.DistanceToPlayer() > 800) || !(target.Health < R.GetDamage(target)) || target.HasBuffOfType(BuffType.SpellShield))
                        continue;

                    if (!target.IsUnKillable())
                    {
                        R.PredCast(target);
                        return;
                    }
                }
            }
        }

        private static void Combo()
        {
            if (ComboOption.UseR && R.IsReady())
            {
                foreach (var target in ObjectManager.Heroes.Enemies.Where(x => x.IsValidTarget(1200)))
                {
                    if (target.IsValidTarget(600) && Player.CountEnemiesInRange(600) >= 3 && target.CountAlliesInRange(200) <= 2)
                    {
                        R.PredCast(target);
                    }

                    if (Player.CountEnemiesInRange(800) == 1 &&
                        target.DistanceToPlayer() > Orbwalker.GetRealAutoAttackRange(Player) &&
                        target.DistanceToPlayer() <= 700 &&
                        target.Health > Player.GetAutoAttackDamage(target) &&
                        target.Health < R.GetDamage(target) + Player.GetAutoAttackDamage(target) * 3 &&
                        !target.HasBuffOfType(BuffType.SpellShield))
                    {
                        R.PredCast(target);
                    }

                    if (target.DistanceToPlayer() <= 1000 &&
                        (!target.CanMove || target.HasBuffOfType(BuffType.Stun) ||
                        R.GetPrediction(target).Hitchance == HitChance.Immobile))
                    {
                        R.PredCast(target);
                    }
                }
            }

            if (ComboOption.UseW && W.IsReady() && !Player.HasBuff("AsheQAttack"))
            {
                if ((ComboOption.GetBool("ComboSaveMana") &&
                     Player.Mana > (R.IsReady() ? R.ManaCost : 0) + W.ManaCost + Q.ManaCost) ||
                    !ComboOption.GetBool("ComboSaveMana"))
                {
                    var target = TargetSelector.GetTarget(W.Range, TargetSelector.DamageType.Physical);

                    if (target.IsValidTarget(W.Range))
                    {
                        W.PredCast(target);
                    }
                }
            }

            if (ComboOption.UseE && E.IsReady())
            {
                var target = TargetSelector.GetTarget(1000, TargetSelector.DamageType.Physical);

                if (target.IsValidTarget(1000))
                {
                    var EPred = E.GetPrediction(target);

                    if ((NavMesh.GetCollisionFlags(EPred.CastPosition) == CollisionFlags.Grass ||
                         NavMesh.IsGrass(target.ServerPosition)) && !target.IsVisible)
                    {
                        E.Cast(EPred.CastPosition);
                    }
                }
            }
        }

        private static void Harass()
        {
            if (HarassOption.HasEnoughMana)
            {
                if (HarassOption.UseW && W.IsReady() && !Player.HasBuff("AsheQAttack"))
                {
                    var target = TargetSelector.GetTarget(Q.Range, TargetSelector.DamageType.Physical, true, ObjectManager.Heroes.Enemies.Where(x => !HarassOption.GetHarassTarget(x.ChampionName)));

                    if (target.IsValidTarget(W.Range))
                    {
                        W.PredCast(target);
                    }
                }
            }
        }

        private static void FarmHarass()
        {
            if (MyManaManager.SpellHarass)
            {
                Harass();
            }
        }

        private static void LaneClear()
        {
            if (LaneClearOption.HasEnoughMana)
            {
                if (LaneClearOption.UseW && W.IsReady())
                {
                    var minions = MinionManager.GetMinions(Player.Position, W.Range);

                    if (minions.Any())
                    {
                        var wFarm = MinionManager.GetBestCircularFarmLocation(minions.Select(x => x.Position.To2D()).ToList(), W.Width, W.Range);

                        if (wFarm.MinionsHit >= LaneClearOption.GetSlider("LaneClearWCount"))
                        {
                            W.Cast(wFarm.Position, true);
                        }
                    }
                }
            }
        }

        private static void JungleClear()
        {
            if (JungleClearOption.HasEnouguMana)
            {
                if (JungleClearOption.UseW && !Player.HasBuff("AsheQAttack"))
                {
                    var mobs = MinionManager.GetMinions(Player.Position, W.Range, MinionTypes.All, MinionTeam.Neutral, MinionOrderTypes.MaxHealth);

                    if (mobs.Any())
                    {
                        var mob = mobs.FirstOrDefault();

                        if (mob != null)
                        {
                            W.Cast(mob.Position, true);
                        }
                    }
                }
            }
        }

        private static void AfterAttack(AttackableUnit unit, AttackableUnit ArgsTarget)
        {
            if (!unit.IsMe || Player.IsDead || ArgsTarget == null || ArgsTarget.IsDead || !ArgsTarget.IsValidTarget())
                return;

            if (IsComboMode && ArgsTarget.ObjectType == GameObjectType.AIHeroClient)
            {
                if (ComboOption.UseQ && Q.IsReady())
                {
                    var target = (AIHeroClient)ArgsTarget;

                    if (!target.IsDead && !target.IsZombie)
                    {
                        if (Player.HasBuff("asheqcastready"))
                        {
                            Q.Cast(true);
                            Orbwalker.ResetAutoAttackTimer();
                        }
                    }
                }
            }
            else if (IsJungleClearMode && ArgsTarget.ObjectType == GameObjectType.obj_AI_Minion)
            {
                if (JungleClearOption.HasEnouguMana && JungleClearOption.UseQ && Q.IsReady())
                {
                    var mobs = MinionManager.GetMinions(Player.Position, Orbwalker.GetRealAutoAttackRange(Player), MinionTypes.All, MinionTeam.Neutral, MinionOrderTypes.MaxHealth);

                    if (mobs.Any())
                    {
                        foreach (var mob in mobs)
                        {
                            if (!mob.IsValidTarget(Orbwalker.GetRealAutoAttackRange(Player)) || !(mob.Health > Player.GetAutoAttackDamage(mob) * 2))
                            {
                                continue;
                            }

                            if (Player.HasBuff("asheqcastready"))
                            {
                                Q.Cast(true);
                                Orbwalker.ResetAutoAttackTimer();
                            }
                        }
                    }
                }
            }
        }

        private static void OnEnemyGapcloser(ActiveGapcloser Args)
        {
            if (!Args.Sender.IsEnemy || !R.IsReady() || MiscOption.GetBool("AntiGapCloser") || Player.HealthPercent > MiscOption.GetSlider("AntiGapCloserHp"))
            {
                return;
            }

            if (MiscOption.GetGapcloserTarget(Args.Sender.ChampionName) && Args.End.DistanceToPlayer() <= 300)
            {
                R.PredCast(Args.Sender);
            }
        }
    }
}
