﻿using HesaEngine.SDK;
using SharpDX.DirectInput;
using System.Collections.Generic;
using static GodDammAIO.Configs;

namespace GodDammAIO.Menus
{
    internal static class JungleClearOption
    {
        static JungleClearOption()
        {
            JungleClearMenu = ChampionMenu.AddSubMenu("Jungle Clear");
        }

        internal static Dictionary<string, bool> CachedBoolValues = new Dictionary<string, bool>();
        internal static Dictionary<string, int> CachedIntValues = new Dictionary<string, int>();

        internal static void CacheBoolValue(MenuCheckbox checkbox, bool addEvent = true)
        {
            if (checkbox == null) return;
            if (!CachedBoolValues.ContainsKey(checkbox.Name))
                CachedBoolValues.Add(checkbox.Name, checkbox.Checked);
            else
                CachedBoolValues[checkbox.Name] = checkbox.Checked;
            if (addEvent)
                checkbox.OnValueChanged += (sender, value) => { CacheBoolValue(sender, false); };
        }

        internal static void CacheBoolValue(MenuKeybind keybind, bool addEvent = true)
        {
            if (keybind == null) return;
            if (!CachedBoolValues.ContainsKey(keybind.Name))
                CachedBoolValues.Add(keybind.Name, keybind.Active);
            else
                CachedBoolValues[keybind.Name] = keybind.Active;
            if (addEvent)
                keybind.OnValueChanged += (sender, value) => { CacheBoolValue(sender, false); };
        }

        internal static void CacheIntValue(MenuSlider slider, bool addEvent = true)
        {
            if (slider == null) return;
            if (!CachedIntValues.ContainsKey(slider.Name))
                CachedIntValues.Add(slider.Name, slider.CurrentValue);
            else
                CachedIntValues[slider.Name] = slider.CurrentValue;
            if (addEvent)
                slider.OnValueChanged += (sender, value) => { CacheIntValue(sender, false); };
        }

        internal static void CacheIntValue(MenuCombo JungleClear, bool addEvent = true)
        {
            if (JungleClear == null) return;
            if (!CachedIntValues.ContainsKey(JungleClear.Name))
                CachedIntValues.Add(JungleClear.Name, JungleClear.CurrentValue);
            else
                CachedIntValues[JungleClear.Name] = JungleClear.CurrentValue;
            if (addEvent)
                JungleClear.OnValueChanged += (sender, value) => { CacheIntValue(sender, false); };
        }

        internal static void AddQ(bool enabled = true)
        {
            AddBool("JungleClearQ", "Use Q", enabled);
        }

        internal static void AddW(bool enabled = true)
        {
            AddBool("JungleClearW", "Use W", enabled);
        }

        internal static void AddE(bool enabled = true)
        {
            AddBool("JungleClearE", "Use E", enabled);
        }

        internal static void AddR(bool enabled = true)
        {
            AddBool("JungleClearR", "Use R", enabled);
        }
        
        internal static void AddMana(int defalutValue = 30)
        {
            JungleClearMenu.Add(new MenuSlider("JungleClearMana", "When Player ManaPercent >= x%", new Slider(0, 100, defalutValue)));
        }

        internal static bool HasEnouguMana => Player.ManaPercent >= GetSlider("JungleClearMana");

        ///
        internal static void AddBool(string name, string text, bool enabled = true)
        {
            CacheBoolValue(JungleClearMenu.Add(new MenuCheckbox(name, text, enabled)));
        }

        internal static void AddSlider(string name, string defaultName, int defaultValue, int minValue, int maxValue)
        {
            CacheIntValue(JungleClearMenu.Add(new MenuSlider(name, defaultName, new Slider(defaultValue, minValue, maxValue))));
        }

        internal static void AddKey(string name, string text, Key key, MenuKeybindType type = MenuKeybindType.Hold, bool enabled = false)
        {
            CacheBoolValue(JungleClearMenu.Add(new MenuKeybind(name, text, new KeyBind(key, type, enabled))));
        }

        internal static void AddList(string name, string defaultName, string[] values, int defaultValue = 0)
        {
            CacheIntValue(JungleClearMenu.Add(new MenuCombo(name, defaultName, new StringList(values, defaultValue))));
        }

        internal static bool GetBool(string name)
        {
            if (CachedBoolValues.ContainsKey(name)) return CachedBoolValues[name];
            return false;
        }

        internal static int GetSlider(string name)
        {
            if (CachedIntValues.ContainsKey(name)) return CachedIntValues[name];
            return int.MaxValue;
        }

        internal static bool GetKey(string name)
        {
            if (CachedBoolValues.ContainsKey(name)) return CachedBoolValues[name];
            return false;
        }

        internal static int GetList(string name)
        {
            if (CachedIntValues.ContainsKey(name)) return CachedIntValues[name];
            return int.MaxValue;
        }

        internal static bool UseQ => GetBool("JungleClearQ");
        internal static bool UseW => GetBool("JungleClearW");
        internal static bool UseE => GetBool("JungleClearE");
        internal static bool UseR => GetBool("JungleClearR");
    }
}