﻿using HesaEngine.SDK;
using HesaEngine.SDK.Enums;
using HesaEngine.SDK.GameObjects;
using SharpDX;
using System.Collections.Generic;
using System.Linq;
using static GodDammAIO.Configs;

namespace GodDammAIO.Champions
{
    internal class Akali : ChampionScript
    {
        Item Bilge = new Item(ItemId.Bilgewater_Cutlass, 550);
        Item Botrk = new Item(ItemId.Blade_of_the_Ruined_King, 550);
        Item Hextech = new Item(ItemId.Hextech_Gunblade, 700);
        Item Tiamat = new Item(ItemId.Tiamat_Melee_Only, 400);
        Item Hydra = new Item(ItemId.Ravenous_Hydra_Melee_Only, 400);
        Item Youmuus = new Item(ItemId.Youmuus_Ghostblade, Player.AttackRange + Player.BoundingRadius + 300);
        List<Item> ItemList;

        #region Combo Settings
        bool ComboQ = true;
        bool ComboE = true;
        bool ComboW = true;
        bool ComboR = true;
        #endregion

        #region Harass Settings
        bool HarassQ = true;
        #endregion

        #region LaneClear Settings
        bool ClearQ = true;
        bool ClearE = true;
        int ClearMana = 25;
        #endregion

        #region LastHit Settings
        bool LastQ = true;
        bool LastE = false;
        #endregion

        #region JungleClear Settings
        bool JungleQ = true;
        bool JungleE = true;
        int JungleMana = 10;
        #endregion

        #region KillSteal Settings
        bool KsQ = true;
        bool KsR = true;
        #endregion

        #region Misc Settings
        bool GapR = true;
        bool FleeW = true;
        bool UseItems = true;
        #endregion

        #region Drawings Settings
        bool DrawQ = true;
        bool DrawR = true;
        #endregion

        public Akali()
        {
            ChampionMenu = MainMenu.AddSubMenu("Akali");

            ComboMenu = ChampionMenu.AddSubMenu("Combo");
            ComboMenu.Add(new MenuCheckbox("ComboQ", "Use Q", ComboQ)).OnValueChanged += (sender, value) => { ComboQ = value; };
            ComboMenu.Add(new MenuCheckbox("ComboW", "Use W", ComboW)).OnValueChanged += (sender, value) => { ComboW = value; };
            ComboMenu.Add(new MenuCheckbox("ComboE", "Use E", ComboE)).OnValueChanged += (sender, value) => { ComboE = value; };
            ComboMenu.Add(new MenuCheckbox("ComboR", "Use R", ComboR)).OnValueChanged += (sender, value) => { ComboR = value; };

            HarassMenu = ChampionMenu.AddSubMenu("Harass");
            HarassMenu.Add(new MenuCheckbox("HarassQ", "Use Q", HarassQ)).OnValueChanged += (sender, value) => { HarassQ = value; };

            LaneClearMenu = ChampionMenu.AddSubMenu("Lane Clear");
            LaneClearMenu.Add(new MenuCheckbox("ClearQ", "Use Q", ClearQ)).OnValueChanged += (sender, value) => { ClearQ = value; };
            LaneClearMenu.Add(new MenuCheckbox("ClearE", "Use E", ClearE)).OnValueChanged += (sender, value) => { ClearE = value; };
            LaneClearMenu.Add(new MenuSlider("ClearMana", "Energy Min. %", 1, 100, ClearMana)).OnValueChanged += (sender, value) => { ClearMana = value; };

            LasthitMenu = ChampionMenu.AddSubMenu("Last Hit");
            LasthitMenu.Add(new MenuCheckbox("LastQ", "Use Q", LastQ)).OnValueChanged += (sender, value) => { LastQ = value; };
            LasthitMenu.Add(new MenuCheckbox("LastE", "Use E", LastE)).OnValueChanged += (sender, value) => { LastE = value; };

            JungleClearMenu = ChampionMenu.AddSubMenu("Jungle Clear");
            JungleClearMenu.Add(new MenuCheckbox("JungleQ", "Use Q", JungleQ)).OnValueChanged += (sender, value) => { JungleQ = value; };
            JungleClearMenu.Add(new MenuCheckbox("JungleE", "Use E", JungleE)).OnValueChanged += (sender, value) => { JungleE = value; };
            JungleClearMenu.Add(new MenuSlider("JungleMana", "Energy Min %", 1, 100, JungleMana)).OnValueChanged += (sender, value) => { JungleMana = value; };

            KillStealMenu = ChampionMenu.AddSubMenu("Kill Steal");
            LasthitMenu.Add(new MenuCheckbox("KsQ", "Use Q", KsQ)).OnValueChanged += (sender, value) => { KsQ = value; };
            LasthitMenu.Add(new MenuCheckbox("KsR", "Use R", KsR)).OnValueChanged += (sender, value) => { KsR = value; };

            MiscMenu = ChampionMenu.AddSubMenu("Misc");
            MiscMenu.Add(new MenuCheckbox("GapR", "Use R for Anti Gapclose", GapR)).OnValueChanged += (sender, value) => { GapR = value; };
            MiscMenu.Add(new MenuCheckbox("FleeW", "Use W on Flee", FleeW)).OnValueChanged += (sender, value) => { FleeW = value; };
            MiscMenu.Add(new MenuCheckbox("UseItems", "Use Agressive Items", UseItems)).OnValueChanged += (sender, value) => { UseItems = value; };

            DrawingsMenu = ChampionMenu.AddSubMenu("Drawings");
            DrawingsMenu.Add(new MenuCheckbox("DrawQ", "Draw Q", DrawQ)).OnValueChanged += (sender, value) => { DrawQ = value; };
            DrawingsMenu.Add(new MenuCheckbox("DrawR", "Draw R", DrawR)).OnValueChanged += (sender, value) => { DrawR = value; };

            Q = new Spell(SpellSlot.Q, 580f, TargetSelector.DamageType.Magical);
            W = new Spell(SpellSlot.W, 680f, TargetSelector.DamageType.Magical);
            E = new Spell(SpellSlot.E, 295f, TargetSelector.DamageType.Physical);
            R = new Spell(SpellSlot.R, 680f, TargetSelector.DamageType.Magical);

            Q.SetTargetted(0.317f, 1000f);
            R.SetTargetted(0.1f, float.MaxValue);

            ItemList = new List<Item>
            {
                Tiamat,
                Hydra,
                Bilge,
                Botrk,
                Hextech,
                Youmuus
            };

            Orbwalker.AfterAttack += Orbwalker_AfterAttack;
            AntiGapcloser.OnEnemyGapcloser += AntiGapcloser_OnEnemyGapcloser;
            Drawing.OnDraw += Drawing_OnDraw;
            Game.OnUpdate += Game_OnUpdate;
        }

        private Color circlesColor = new ColorBGRA(255, 255, 255, 100);
        private void Drawing_OnDraw(System.EventArgs args)
        {
            if (Player.IsDead || Shop.IsShopOpen || MainMenu.IsOpen) return;
            if (DrawQ && Q.IsReady())
            {
                Drawing.DrawCircle(Player.Position, Q.Range, circlesColor);
            }
            if (DrawR && R.IsReady())
            {
                Drawing.DrawCircle(Player.Position, R.Range, circlesColor);
            }
        }

        private void AntiGapcloser_OnEnemyGapcloser(ActiveGapcloser gapcloser)
        {
            if (gapcloser.Sender == null || !gapcloser.Sender.IsEnemy || gapcloser.Sender.IsUnderEnemyTurret()) return;
            if (GapR && R.IsReady() && gapcloser.Sender.IsValidTarget(R.Range))
            {
                R.Cast(gapcloser.Sender);
            }
        }

        private void Orbwalker_AfterAttack(AttackableUnit unit, AttackableUnit target)
        {
            if (Orbwalk.ActiveMode == Orbwalker.OrbwalkingMode.Combo ||
                Orbwalk.ActiveMode == Orbwalker.OrbwalkingMode.Harass ||
                Orbwalk.ActiveMode == Orbwalker.OrbwalkingMode.JungleClear)
            {
                if (E.IsReady() && ComboE)
                {
                    E.Cast();
                }
            }
        }

        private void Game_OnUpdate()
        {
            if (Orbwalk.ActiveMode == Orbwalker.OrbwalkingMode.Combo)
            {
                Combo();
            }

            if (Orbwalk.ActiveMode == Orbwalker.OrbwalkingMode.Harass)
            {
                Harass();
            }

            if (Orbwalk.ActiveMode == Orbwalker.OrbwalkingMode.LaneClear)
            {
                Clear();
            }

            if (Orbwalk.ActiveMode == Orbwalker.OrbwalkingMode.JungleClear)
            {
                Jungle();
            }

            if (Orbwalk.ActiveMode == Orbwalker.OrbwalkingMode.LastHit)
            {
                Lasthit();
            }

            if (Orbwalk.ActiveMode == Orbwalker.OrbwalkingMode.Flee)
            {
                Flee();
            }

            Killsteal();
        }

        void CastItems(AIHeroClient target)
        {
            foreach (var item in ItemList.Where(i => i.IsReady() && target.IsValidTarget(i.Range)))
            {
                item.Cast(target);
                return;
            }
        }

        void Combo()
        {
            var target = TargetSelector.GetTarget(Q.Range, TargetSelector.DamageType.Magical);

            if (target == null || !target.IsValidTarget()) return;

            if (UseItems)
            {
                CastItems(target);
            }

            if (ComboQ && Q.IsReady())
            {
                Q.Cast(target);
            }

            if (ComboR && R.IsReady() && !Player.IsInRange(target, E.Range))
            {
                R.Cast(target);
            }

            if (ComboW && W.IsReady() && Player.CountEnemiesInRange(Q.Range) >= 2)
            {
                W.Cast();
            }
        }

        void Harass()
        {
            var target = TargetSelector.GetTarget(Q.Range, TargetSelector.DamageType.Magical);

            if (target == null || !target.IsValidTarget(Q.Range)) return;

            if (HarassQ && Q.IsReady())
            {
                Q.Cast(target);
            }
        }

        void Clear()
        {
            if (Player.ManaPercent < ClearMana) return;

            var minion = Helper.Minions(MinionTeam.Enemy, Q.Range, Player.ServerPosition).FirstOrDefault();
            if (minion == null || !minion.IsValidTarget(Q.Range)) return;

            if (ClearQ && Q.IsReady())
            {
                Q.Cast(minion);
            }
            if (ClearE && E.IsReady() && minion.IsValidTarget(E.Range))
            {
                E.Cast();
            }
        }

        void Jungle()
        {
            var monster = Helper.Monsters(Q.Range, Player.ServerPosition).FirstOrDefault();

            if (monster == null || !monster.IsValidTarget()) return;

            if (JungleQ && Q.IsReady())
            {
                Q.Cast(monster);
            }
        }

        void Lasthit()
        {
            var qminion = Helper.Minions(MinionTeam.Enemy, Q.Range, Player.ServerPosition).FirstOrDefault();

            var eminion = Helper.Minions(MinionTeam.Enemy, E.Range, Player.ServerPosition).FirstOrDefault();

            var qiskillable = Helper.IsKillableMinion(qminion, Q.Range, SpellSlot.Q);
            var eiskillable = Helper.IsKillableMinion(eminion, E.Range, SpellSlot.E);

            if (qminion != null && !Player.IsInAutoAttackRange(qminion))
            {
                if (LastQ && Q.IsReady() && qiskillable)
                {
                    Q.Cast(qminion);
                }
            }

            if (eminion != null)
            {
                if (LastE && E.IsReady() && eiskillable)
                {
                    E.Cast();
                }
            }
        }

        void Flee()
        {
            if (FleeW && W.IsReady())
            {
                W.Cast();
            }
        }

        void Killsteal()
        {
            if (KsQ && Q.IsReady())
            {
                var target = Helper.CloseEnemies(Q.Range, Player.ServerPosition).FirstOrDefault();
                if (Helper.IsKillableTarget(target, Q.Range, SpellSlot.Q))
                {
                    Q.Cast(target);
                }
            }
            if (KsR && R.IsReady())
            {
                var target = Helper.CloseEnemies(R.Range, Player.ServerPosition).FirstOrDefault();
                if (Helper.IsKillableTarget(target, R.Range, SpellSlot.R))
                {
                    R.Cast(target);
                }
            }
        }
    }
}