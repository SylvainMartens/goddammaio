﻿using System;
using System.Linq;
using HesaEngine.SDK.GameObjects;
using GodDammAIO.Evade.Spells;
using HesaEngine.SDK;
using GodDammAIO.Evade.Utils;
using GodDammAIO.Evade.Helpers;

namespace GodDammAIO.Evade.SpecialSpells
{
    class Lux : ChampionPlugin
    {
        static Lux()
        {

        }

        public void LoadSpecialSpell(SpellData spellData)
        {
            if (spellData.spellName == "LuxMaliceCannon")
            {
                var hero = ObjectManager.Heroes.All.FirstOrDefault(h => h.ChampionName == "Lux");
                if (hero != null && hero.CheckTeam())
                {
                    ObjectTracker.HuiTrackerForceLoad();
                    GameObject.OnCreate += (obj, args) => OnCreateObj_LuxMaliceCannon(obj, args, hero, spellData);
                }
            }
        }

        private static void OnCreateObj_LuxMaliceCannon(GameObject obj, EventArgs args, AIHeroClient hero, SpellData spellData)
        {
            if (obj.Name.Contains("Lux") && obj.Name.Contains("R_mis_beam_middle"))
            {
                if (hero.IsVisible) return;

                var objList = ObjectTracker.objTracker.Values.Where(o => o.Name == "hiu");
                if (objList.Count() >= 2)
                {
                    var dir = ObjectTracker.GetLastHiuOrientation();
                    var pos1 = obj.Position.To2D() - dir * 1750;
                    var pos2 = obj.Position.To2D() + dir * 1750;

                    SpellDetector.CreateSpellData(hero, pos1.To3D(), pos2.To3D(), spellData, null, 0);

                    foreach (ObjectTrackerInfo gameObj in objList)
                    {
                        DelayAction.Add(1, () => ObjectTracker.objTracker.Remove(gameObj.obj.NetworkId));
                    }
                }
            }
        }
    }
}