﻿using System;
using System.Collections.Generic;
using System.Linq;
using SharpDX;
using HesaEngine.SDK.GameObjects;
using HesaEngine.SDK;
using HesaEngine.SDK.Enums;
using HesaEngine.SDK.Args;
using GodDammAIO.Evade.Helpers;
using GodDammAIO.Evade.Utils;
using GodDammAIO.Evade.Spells;
using GodDammAIO.Evade.EvadeSpells;
using GodDammAIO.Evade.Tests;

namespace GodDammAIO.Evade.Core
{
    internal class Evade
    {
        public static AIHeroClient myHero { get { return ObjectManager.Player; } }

        public static SpellDetector spellDetector;
        private static SpellDrawer spellDrawer;
        private static EvadeTester evadeTester;
        private static PingTester pingTester;
        private static EvadeSpell evadeSpell;
        private static SpellTester spellTester;
        private static AutoSetPing autoSetPing;

        public static SpellSlot lastSpellCast;
        public static float lastSpellCastTime = 0;

        public static float lastWindupTime = 0;

        public static float lastTickCount = 0;
        public static float lastStopEvadeTime = 0;

        public static Vector3 lastMovementBlockPos = Vector3.Zero;
        public static float lastMovementBlockTime = 0;

        public static float lastEvadeOrderTime = 0;
        public static float lastIssueOrderGameTime = 0;
        public static float lastIssueOrderTime = 0;
        public static GameObjectIssueOrderEventArgs lastIssueOrderArgs = null;

        public static Vector2 lastMoveToPosition = Vector2.Zero;
        public static Vector2 lastMoveToServerPos = Vector2.Zero;
        public static Vector2 lastStopPosition = Vector2.Zero;

        public static DateTime assemblyLoadTime = DateTime.Now;

        public static bool isDodging = false;
        public static bool dodgeOnlyDangerous = false;

        public static bool devModeOn = false;
        public static bool hasGameEnded = false;
        public static bool isChanneling = false;
        public static Vector2 channelPosition = Vector2.Zero;

        public static PositionInfo lastPosInfo;

        public static EvadeCommand lastEvadeCommand = new EvadeCommand { isProcessed = true, timestamp = EvadeUtils.TickCount };

        public static EvadeCommand lastBlockedUserMoveTo = new EvadeCommand { isProcessed = true, timestamp = EvadeUtils.TickCount };
        public static float lastDodgingEndTime = 0;

        public static Menu Menu_;

        public static float sumCalculationTime = 0;
        public static float numCalculationTime = 0;
        public static float avgCalculationTime = 0;

        public Evade(Menu menu)
        {
            try
            {
                devModeOn = false;

                Chat.Print(devModeOn ? "<b>Evade: Developer Mode On</b>" : "<b>Evade: Loaded!</b>");
                Menu_ = menu;

                Menu mainMenu = Menu_.AddSubMenu("Main");

                mainMenu.Add(new MenuKeybind("DodgeSkillShots", "Dodge SkillShots", SharpDX.DirectInput.Key.K, MenuKeybindType.Toggle).SetValue(EvadeConfigs.DodgeSkillShots)).OnValueChanged += (sender, value) => { EvadeConfigs.DodgeSkillShots = value; };
                mainMenu.Add(new MenuKeybind("ActivateEvadeSpells", "Use Evade Spells", SharpDX.DirectInput.Key.K, MenuKeybindType.Toggle).SetValue(EvadeConfigs.ActivateEvadeSpells)).OnValueChanged += (sender, value) => { EvadeConfigs.ActivateEvadeSpells = value; };

                mainMenu.Add(new MenuCheckbox("DodgeDangerous", "Dodge Only Dangerous", EvadeConfigs.DodgeDangerous)).OnValueChanged += (sender, value) => { EvadeConfigs.DodgeDangerous = value; };
                mainMenu.Add(new MenuCheckbox("DodgeFOWSpells", "Dodge FOW SkillShots", EvadeConfigs.DodgeFOWSpells)).OnValueChanged += (sender, value) => { EvadeConfigs.DodgeFOWSpells = value; };
                mainMenu.Add(new MenuCheckbox("DodgeCircularSpells", "Dodge Circular SkillShots", EvadeConfigs.DodgeCircularSpells)).OnValueChanged += (sender, value) => { EvadeConfigs.DodgeCircularSpells = value; };

                spellDetector = new SpellDetector(Menu_);
                evadeSpell = new EvadeSpell(Menu_);

                Menu keyMenu = Menu_.AddSubMenu("Key Settings");
                keyMenu.Add(new MenuCheckbox("DodgeDangerousKeyEnabled", "Enable Dodge Only Dangerous Keys", false));
                keyMenu.Add(new MenuKeybind("DodgeDangerousKey", "Dodge Only Dangerous Key", SharpDX.DirectInput.Key.Space, MenuKeybindType.Hold));
                keyMenu.Add(new MenuKeybind("DodgeDangerousKey2", "Dodge Only Dangerous Key 2", SharpDX.DirectInput.Key.V, MenuKeybindType.Hold));
                keyMenu.Add(new MenuCheckbox("DodgeOnlyOnComboKeyEnabled", "Enable Dodge Only On Combo Key", false));
                keyMenu.Add(new MenuKeybind("DodgeComboKey", "Dodge Only Combo Key", SharpDX.DirectInput.Key.Space, MenuKeybindType.Hold));
                keyMenu.Add(new MenuCheckbox("DontDodgeKeyEnabled", "Enable Don't Dodge Key", false));
                keyMenu.Add(new MenuKeybind("DontDodgeKey", "Don't Dodge Key", SharpDX.DirectInput.Key.Z, MenuKeybindType.Hold));

                Menu miscMenu = Menu_.AddSubMenu("Misc Settings");
                miscMenu.Add(new MenuCheckbox("HigherPrecision", "Enhanced Dodge Precision", true));
                miscMenu.Add(new MenuCheckbox("RecalculatePosition", "Recalculate Path", true));
                miscMenu.Add(new MenuCheckbox("ContinueMovement", "Continue Last Movement", true));
                miscMenu.Add(new MenuCheckbox("CalculateWindupDelay", "Calculate Windup Delay", true));
                miscMenu.Add(new MenuCheckbox("CheckSpellCollision", "Check Spell Collision", true));
                miscMenu.Add(new MenuCheckbox("PreventDodgingUnderTower", "Prevent Dodging Under Tower", false));
                miscMenu.Add(new MenuCheckbox("PreventDodgingNearEnemy", "Prevent Dodging Near Enemies", true));
                miscMenu.Add(new MenuCheckbox("AdvancedSpellDetection", "Advanced Spell Detection", true));
                miscMenu.Add(new MenuCheckbox("ClickRemove", "Allow Left Click Removal", true));

                //miscMenu.AddItem(new MenuItem("AllowCrossing", "Allow Crossing").SetValue(false));
                //miscMenu.AddItem(new MenuItem("CalculateHeroPos", "Calculate Hero Position").SetValue(false));
                miscMenu.Add(new MenuCombo("EvadeMode", "Evade Profile", new string[] { "Smooth", "Very Smooth", "Fastest", "Hawk", "Kurisu", "GuessWho" }, 2));
                miscMenu.Item("EvadeMode").ValueChanged += OnEvadeModeChange;

                //miscMenu.Add(new MenuCheckbox("ResetConfig", "Reset Evade Config", false));

                Menu limiterMenu = miscMenu.AddSubMenu("Humanizer");
                miscMenu.Add(new MenuCheckbox("ClickOnlyOnce", "Click Only Once", true));
                miscMenu.Add(new MenuCheckbox("EnableEvadeDistance", "Extended Evade", true));
                miscMenu.Add(new MenuSlider("TickLimiter", "Tick Limiter", new Slider(0, 500, 100)));
                miscMenu.Add(new MenuSlider("SpellDetectionTime", "Spell Detection Time", new Slider(0, 1000, 0)));
                miscMenu.Add(new MenuSlider("ReactionTime", "Reaction Time", new Slider(0, 500, 0)));
                miscMenu.Add(new MenuSlider("DodgeInterval", "Dodge Interval", new Slider(0, 2000, 0)));

                Menu fastEvadeMenu = miscMenu.AddSubMenu("Fast Evade");
                fastEvadeMenu.Add(new MenuCheckbox("FastMovementBlock", "Fast Movement Block", false));
                fastEvadeMenu.Add(new MenuSlider("FastEvadeActivationTime", "FastEvade Activation Time", new Slider(0, 500, 65)));
                fastEvadeMenu.Add(new MenuSlider("SpellActivationTime", "Spell Activation Time", new Slider(0, 1000, 400)));
                fastEvadeMenu.Add(new MenuSlider("RejectMinDistance", "Collision Distance Buffer", new Slider(0, 100, 10)));

                /*Menu evadeSpellSettingsMenu = new Menu("Evade Spell", "EvadeSpellMisc");
                evadeSpellSettingsMenu.AddItem(new MenuItem("EvadeSpellActivationTime", "Evade Spell Activation Time").SetValue(new Slider(150, 0, 500)));
                miscMenu.AddSubMenu(evadeSpellSettingsMenu);*/

                Menu bufferMenu = miscMenu.AddSubMenu("Extra Buffers");
                bufferMenu.Add(new MenuSlider("ExtraPingBuffer", "Extra Ping Buffer", new Slider(0, 200, 65)));
                bufferMenu.Add(new MenuSlider("ExtraCPADistance", "Extra Collision Distance", new Slider(0, 150, 10)));
                bufferMenu.Add(new MenuSlider("ExtraSpellRadius", "Extra Spell Radius", new Slider(0, 100, 0)));
                bufferMenu.Add(new MenuSlider("ExtraEvadeDistance", "Extra Evade Distance", new Slider(0, 300, 100)));
                bufferMenu.Add(new MenuSlider("ExtraAvoidDistance", "Extra Avoid Distance", new Slider(0, 300, 50)));
                bufferMenu.Add(new MenuSlider("MinComfortZone", "Min Distance to Champion", new Slider(0, 1000, 550)));
                /*
                Menu loadTestMenu = miscMenu.AddSubMenu("Tests");
                loadTestMenu.Add(new MenuCheckbox("LoadPingTester", "Load Ping Tester", false));
                loadTestMenu.Add(new MenuCheckbox("LoadSpellTester", "Load Spell Tester", false));
                loadTestMenu.Item("LoadPingTester").ValueChanged += OnLoadPingTesterChange;
                loadTestMenu.Item("LoadSpellTester").ValueChanged += OnLoadSpellTesterChange;
                */
                spellDrawer = new SpellDrawer(Menu_);

                //autoSetPing = new AutoSetPing(menu);

                ObjectCache.menuCache = new MenuCache(Menu_);
                var initCache = ObjectCache.myHeroCache;
                /*
                if (devModeOn)
                {
                    var o = new EvadeTester(Menu.AddMenu("Evade: Test"));
                    //Utility.DelayAction.Add(100, () => loadTestMenu.Item("LoadSpellTester").SetValue(true));
                }
                */
                //Console.WriteLine("Evade Loaded");

                Obj_AI_Base.OnIssueOrder += Game_OnIssueOrder;
                SpellBook.OnCastSpell += Game_OnCastSpell;
                Game.OnUpdate += Game_OnGameUpdate;

                Obj_AI_Base.OnProcessSpellCast += Game_OnProcessSpell;

                Game.OnEnd += Game_OnGameEnd;
                SpellDetector.OnProcessDetectedSpells += SpellDetector_OnProcessDetectedSpells;
                Orbwalker.BeforeAttack += Orbwalking_BeforeAttack;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        
        public static void ResetConfig(bool kappa = true)
        {
            EvadeConfigs.DodgeSkillShots = true;
            EvadeConfigs.ActivateEvadeSpells = true;
            EvadeConfigs.DodgeDangerous = true;
            EvadeConfigs.DodgeFOWSpells = false;
            EvadeConfigs.DodgeCircularSpells = true;

            Menu_.Item("HigherPrecision").SetValue(false);
            Menu_.Item("RecalculatePosition").SetValue(true);
            Menu_.Item("ContinueMovement").SetValue(true);
            Menu_.Item("CalculateWindupDelay").SetValue(true);
            Menu_.Item("CheckSpellCollision").SetValue(false);
            Menu_.Item("PreventDodgingUnderTower").SetValue(false);
            Menu_.Item("PreventDodgingNearEnemy").SetValue(true);
            Menu_.Item("AdvancedSpellDetection").SetValue(false);
            Menu_.Item("LoadPingTester").SetValue(true);

            Menu_.Item("ClickOnlyOnce").SetValue(true);
            Menu_.Item("EnableEvadeDistance").SetValue(false);
            Menu_.Item("TickLimiter").SetValue(new Slider(100, 0, 500));
            Menu_.Item("SpellDetectionTime").SetValue(new Slider(0, 0, 1000));
            Menu_.Item("ReactionTime").SetValue(new Slider(0, 0, 500));
            Menu_.Item("DodgeInterval").SetValue(new Slider(0, 0, 2000));

            Menu_.Item("FastMovementBlock").SetValue(false);
            Menu_.Item("FastEvadeActivationTime").SetValue(new Slider(65, 0, 500));
            Menu_.Item("SpellActivationTime").SetValue(new Slider(400, 0, 1000));
            Menu_.Item("RejectMinDistance").SetValue(new Slider(10, 0, 100));

            Menu_.Item("ExtraPingBuffer").SetValue(new Slider(65, 0, 200));
            Menu_.Item("ExtraCPADistance").SetValue(new Slider(10, 0, 150));
            Menu_.Item("ExtraSpellRadius").SetValue(new Slider(0, 0, 100));
            Menu_.Item("ExtraEvadeDistance").SetValue(new Slider(200, 0, 300));
            Menu_.Item("ExtraAvoidDistance").SetValue(new Slider(50, 0, 300));
            Menu_.Item("MinComfortZone").SetValue(new Slider(550, 0, 1000));

            // drawings
            Menu_.Item("DrawSkillShots").SetValue(true);
            Menu_.Item("ShowStatus").SetValue(true);
            Menu_.Item("DrawSpellPos").SetValue(false);
            Menu_.Item("DrawEvadePosition").SetValue(false);

            if (kappa)
            {
                // profiles
                Menu_.Item("EvadeMode").SetValue(new StringList(new[] { "Smooth", "Very Smooth", "Fastest", "Hawk", "Kurisu", "GuessWho" }, 0));

                // keys
                Menu_.Item("DodgeDangerousKeyEnabled").SetValue(false);
                Menu_.Item("DodgeDangerousKey").SetValue(new KeyBind(SharpDX.DirectInput.Key.Space, MenuKeybindType.Hold));
                Menu_.Item("DodgeDangerousKey2").SetValue(new KeyBind(SharpDX.DirectInput.Key.V, MenuKeybindType.Hold));
                Menu_.Item("DodgeOnlyOnComboKeyEnabled").SetValue(false);
                Menu_.Item("DodgeComboKey").SetValue(new KeyBind(SharpDX.DirectInput.Key.Space, MenuKeybindType.Hold));
                Menu_.Item("DontDodgeKeyEnabled").SetValue(false);
                Menu_.Item("DontDodgeKey").SetValue(new KeyBind(SharpDX.DirectInput.Key.Z, MenuKeybindType.Hold));
            }
        }

        private void OnEvadeModeChange(object sender, OnValueChangeEventArgs e)
        {
            var mode = e.GetNewValue<int>();

            if (mode == 0)
            {
                ResetConfig(false);
                Menu_.Item("FastMovementBlock").SetValue(true);
                Menu_.Item("FastEvadeActivationTime").SetValue(new Slider(120, 0, 500));
                Menu_.Item("RejectMinDistance").SetValue(new Slider(25, 0, 100));
                Menu_.Item("ExtraCPADistance").SetValue(new Slider(25, 0, 150));
                Menu_.Item("ExtraPingBuffer").SetValue(new Slider(80, 0, 200));
            }
            else if (mode == 1)
            {
                ResetConfig(false);
                Menu_.Item("FastEvadeActivationTime").SetValue(new Slider(0, 0, 500));
                Menu_.Item("RejectMinDistance").SetValue(new Slider(0, 0, 100));
                Menu_.Item("ExtraCPADistance").SetValue(new Slider(0, 0, 150));
                Menu_.Item("ExtraPingBuffer").SetValue(new Slider(40, 0, 200));
            }
            else if (mode == 2)
            {
                ResetConfig(false);
                Menu_.Item("FastMovementBlock").SetValue(true);
                Menu_.Item("FastEvadeActivationTime").SetValue(new Slider(65, 0, 500));
                Menu_.Item("RejectMinDistance").SetValue(new Slider(10, 0, 100));
                Menu_.Item("ExtraCPADistance").SetValue(new Slider(10, 0, 150));
                Menu_.Item("ExtraPingBuffer").SetValue(new Slider(65, 0, 200));
            }
            else if (mode == 3)
            {
                ResetConfig(false);
                Menu_.Item("DodgeDangerous").SetValue(false);
                Menu_.Item("DodgeFOWSpells").SetValue(false);
                Menu_.Item("DodgeCircularSpells").SetValue(false);
                Menu_.Item("DodgeDangerousKeyEnabled").SetValue(true);
                Menu_.Item("HigherPrecision").SetValue(true);
                Menu_.Item("RecalculatePosition").SetValue(true);
                Menu_.Item("ContinueMovement").SetValue(true);
                Menu_.Item("CalculateWindupDelay").SetValue(true);
                Menu_.Item("CheckSpellCollision").SetValue(true);
                Menu_.Item("PreventDodgingUnderTower").SetValue(false);
                Menu_.Item("PreventDodgingNearEnemy").SetValue(true);
                Menu_.Item("AdvancedSpellDetection").SetValue(true);
                Menu_.Item("ClickOnlyOnce").SetValue(true);
                Menu_.Item("EnableEvadeDistance").SetValue(true);
                Menu_.Item("TickLimiter").SetValue(new Slider(200, 0, 500));
                Menu_.Item("SpellDetectionTime").SetValue(new Slider(375, 0, 1000));
                Menu_.Item("ReactionTime").SetValue(new Slider(285, 0, 500));
                Menu_.Item("DodgeInterval").SetValue(new Slider(235, 0, 2000));
                Menu_.Item("FastEvadeActivationTime").SetValue(new Slider(0, 0, 500));
                Menu_.Item("SpellActivationTime").SetValue(new Slider(200, 0, 1000));
                Menu_.Item("RejectMinDistance").SetValue(new Slider(0, 0, 100));
                Menu_.Item("ExtraPingBuffer").SetValue(new Slider(65, 0, 200));
                Menu_.Item("ExtraCPADistance").SetValue(new Slider(0, 0, 150));
                Menu_.Item("ExtraSpellRadius").SetValue(new Slider(0, 0, 100));
                Menu_.Item("ExtraEvadeDistance").SetValue(new Slider(200, 0, 300));
                Menu_.Item("ExtraAvoidDistance").SetValue(new Slider(200, 0, 300));
                Menu_.Item("MinComfortZone").SetValue(new Slider(550, 0, 1000));
            }
            else if (mode == 4)
            {
                ResetConfig(false);
                Menu_.Item("DodgeFOWSpells").SetValue(false);
                Menu_.Item("DodgeDangerousKeyEnabled").SetValue(true);
                Menu_.Item("RecalculatePosition").SetValue(true);
                Menu_.Item("ContinueMovement").SetValue(true);
                Menu_.Item("CalculateWindupDelay").SetValue(true);
                Menu_.Item("PreventDodgingUnderTower").SetValue(true);
                Menu_.Item("PreventDodgingNearEnemy").SetValue(true);
                Menu_.Item("MinComfortZone").SetValue(new Slider(850, 0, 1000));
            }
            else if (mode == 5)
            {
                ResetConfig(false);
                Menu_.Item("DodgeDangerousKeyEnabled").SetValue(true);
                Menu_.Item("DodgeDangerousKey2").SetValue(new KeyBind(SharpDX.DirectInput.Key.Subtract, MenuKeybindType.Hold));
                Menu_.Item("HigherPrecision").SetValue(true);
                Menu_.Item("CheckSpellCollision").SetValue(true);
                Menu_.Item("PreventDodgingUnderTower").SetValue(true);
                Menu_.Item("ShowStatus").SetValue(false);
                Menu_.Item("DrawSpellPos").SetValue(true);
            }
        }

        private void OnLoadPingTesterChange(object sender, OnValueChangeEventArgs e)
        {
            e.Process = false;

            if (pingTester == null)
            {
                pingTester = new PingTester();
            }
        }

        private void OnLoadSpellTesterChange(object sender, OnValueChangeEventArgs e)
        {
            e.Process = false;

            if (spellTester == null)
            {
                spellTester = new SpellTester();
            }
        }

        private void Game_OnGameEnd()
        {
            hasGameEnded = true;
        }

        private void Game_OnCastSpell(SpellBook spellbook, SpellbookCastSpellEventArgs args)
        {
            if (!spellbook.Owner.IsMe)
                return;

            var sData = spellbook.GetSpell(args.Slot);
            string name;

            if (SpellDetector.channeledSpells.TryGetValue(sData.SpellData.Name, out name))
            {
                //Evade.isChanneling = true;
                //Evade.channelPosition = ObjectCache.myHeroCache.serverPos2D;
                lastStopEvadeTime = EvadeUtils.TickCount + ObjectCache.gamePing + 100;
            }

            //block spell commmands if evade spell just used
            if (EvadeSpell.lastSpellEvadeCommand != null &&
                EvadeSpell.lastSpellEvadeCommand.timestamp + ObjectCache.gamePing + 150 > EvadeUtils.TickCount)
            {
                args.Process = false;
            }

            lastSpellCast = args.Slot;
            lastSpellCastTime = EvadeUtils.TickCount;

            //moved from processPacket

            /*if (args.Slot == SpellSlot.Recall)
            {
                lastStopPosition = myHero.ServerPosition.To2D();
            }*/

            if (Situation.ShouldDodge())
            {
                if (isDodging && SpellDetector.spells.Any())
                {
                    foreach (KeyValuePair<String, SpellData> entry in SpellDetector.windupSpells)
                    {
                        SpellData spellData = entry.Value;

                        if (spellData.spellKey == args.Slot) //check if it's a spell that we should block
                        {
                            args.Process = false;
                            return;
                        }
                    }
                }
            }

            foreach (var evadeSpell in EvadeSpell.evadeSpells)
            {
                if (evadeSpell.isItem == false && evadeSpell.spellKey == args.Slot &&
                    evadeSpell.untargetable == false)
                {
                    if (evadeSpell.evadeType == EvadeType.Blink)
                    {
                        var blinkPos = args.StartPosition.To2D();

                        var posInfo = EvadeHelper.CanHeroWalkToPos(blinkPos, evadeSpell.speed, ObjectCache.gamePing, 0);
                        if (posInfo != null && posInfo.posDangerLevel == 0)
                        {
                            EvadeCommand.MoveTo(posInfo.position);
                            lastStopEvadeTime = EvadeUtils.TickCount + ObjectCache.gamePing + evadeSpell.spellDelay;
                        }
                    }

                    if (evadeSpell.evadeType == EvadeType.Dash)
                    {
                        var dashPos = args.StartPosition.To2D();

                        if (args.Target != null)
                        {
                            dashPos = args.Target.Position.To2D();
                        }

                        if (evadeSpell.fixedRange || dashPos.Distance(myHero.ServerPosition.To2D()) > evadeSpell.range)
                        {
                            var dir = (dashPos - myHero.ServerPosition.To2D()).Normalized();
                            dashPos = myHero.ServerPosition.To2D() + dir * evadeSpell.range;
                        }

                        var posInfo = EvadeHelper.CanHeroWalkToPos(dashPos, evadeSpell.speed, ObjectCache.gamePing, 0);
                        if (posInfo != null && posInfo.posDangerLevel > 0)
                        {
                            args.Process = false;
                            return;
                        }

                        if (isDodging || EvadeUtils.TickCount < lastDodgingEndTime + 500)
                        {
                            EvadeCommand.MoveTo(Game.CursorPosition.To2D());
                            lastStopEvadeTime = EvadeUtils.TickCount + ObjectCache.gamePing + 100;
                        }
                    }
                    return;
                }
            }
        }

        private void Game_OnIssueOrder(Obj_AI_Base hero, GameObjectIssueOrderEventArgs args)
        {
            if (!hero.IsMe)
                return;

            if (!Situation.ShouldDodge())
                return;

            if (args.Order == GameObjectOrder.MoveTo)
            {
                if (isDodging && SpellDetector.spells.Any())
                {
                    CheckHeroInDanger();

                    lastBlockedUserMoveTo = new EvadeCommand
                    {
                        order = EvadeOrderCommand.MoveTo,
                        targetPosition = args.TargetPosition.To2D(),
                        timestamp = EvadeUtils.TickCount,
                        isProcessed = false,
                    };

                    args.Process = false;
                }
                else
                {
                    var movePos = args.TargetPosition.To2D();
                    var extraDelay = ObjectCache.menuCache.cache["ExtraPingBuffer"].GetValue<int>();

                    if (EvadeHelper.CheckMovePath(movePos, ObjectCache.gamePing + extraDelay))
                    {
                        /*if (ObjectCache.menuCache.cache["AllowCrossing"].GetValue<bool>())
                        {
                            var extraDelayBuffer = ObjectCache.menuCache.cache["ExtraPingBuffer"]
                                .GetValue<int>() + 30;
                            var extraDist = ObjectCache.menuCache.cache["ExtraCPADistance"]
                                .GetValue<int>() + 10;
                            var tPosInfo = EvadeHelper.CanHeroWalkToPos(movePos, ObjectCache.myHeroCache.moveSpeed, extraDelayBuffer + ObjectCache.gamePing, extraDist);
                            if (tPosInfo.posDangerLevel == 0)
                            {
                                lastPosInfo = tPosInfo;
                                return;
                            }
                        }*/

                        lastBlockedUserMoveTo = new EvadeCommand
                        {
                            order = EvadeOrderCommand.MoveTo,
                            targetPosition = args.TargetPosition.To2D(),
                            timestamp = EvadeUtils.TickCount,
                            isProcessed = false,
                        };

                        args.Process = false; //Block the command

                        if (EvadeUtils.TickCount - lastMovementBlockTime < 500 && lastMovementBlockPos.Distance(args.TargetPosition) < 100)
                        {
                            return;
                        }

                        lastMovementBlockPos = args.TargetPosition;
                        lastMovementBlockTime = EvadeUtils.TickCount;

                        var posInfo = EvadeHelper.GetBestPositionMovementBlock(movePos);
                        if (posInfo != null)
                        {
                            EvadeCommand.MoveTo(posInfo.position);
                        }
                        return;
                    }
                    else
                    {
                        lastBlockedUserMoveTo.isProcessed = true;
                    }
                }
            }
            else //need more logic
            {
                if (isDodging)
                {
                    args.Process = false; //Block the command
                }
                else
                {
                    if (args.Order == GameObjectOrder.AttackUnit)
                    {
                        var target = args.Target;
                        if (target != null && target.IsValid<Obj_AI_Base>())
                        {
                            var baseTarget = target as Obj_AI_Base;
                            if (ObjectCache.myHeroCache.serverPos2D.Distance(baseTarget.ServerPosition.To2D()) >
                                myHero.AttackRange + ObjectCache.myHeroCache.boundingRadius + baseTarget.BoundingRadius)
                            {
                                var movePos = args.TargetPosition.To2D();
                                var extraDelay = ObjectCache.menuCache.cache["ExtraPingBuffer"].GetValue<int>();
                                if (EvadeHelper.CheckMovePath(movePos, ObjectCache.gamePing + extraDelay))
                                {
                                    args.Process = false; //Block the command
                                    return;
                                }
                            }
                        }
                    }
                }
            }

            if (args.Process == true)
            {
                lastIssueOrderGameTime = Game.Time * 1000;
                lastIssueOrderTime = EvadeUtils.TickCount;
                lastIssueOrderArgs = args;

                if (args.Order == GameObjectOrder.MoveTo)
                {
                    lastMoveToPosition = args.TargetPosition.To2D();
                    lastMoveToServerPos = myHero.ServerPosition.To2D();
                }

                if (args.Order == GameObjectOrder.HoldPosition)
                {
                    lastStopPosition = myHero.ServerPosition.To2D();
                }
            }
        }

        private void Orbwalking_BeforeAttack(BeforeAttackEventArgs args)
        {
            if (isDodging)
            {
                args.Process = false; //Block orbwalking
            }
        }

        private void Game_OnProcessSpell(Obj_AI_Base hero, GameObjectProcessSpellCastEventArgs args)
        {
            if (!hero.IsMe)
            {
                return;
            }

            string name;
            if (SpellDetector.channeledSpells.TryGetValue(args.SData.Name.ToLower(), out name))
            {
                isChanneling = true;
                channelPosition = myHero.ServerPosition.To2D();
            }

            if (ObjectCache.menuCache.cache["CalculateWindupDelay"].GetValue<bool>())
            {
                var castTime = (hero.Spellbook.CastTime - Game.Time) * 1000;

                if (castTime > 0 && !Orbwalker.IsAutoAttack(args.SData.Name)
                    && Math.Abs(castTime - myHero.AttackCastDelay * 1000) > 1)
                {
                    lastWindupTime = EvadeUtils.TickCount + castTime - Game.Ping / 2;

                    if (isDodging)
                    {
                        SpellDetector_OnProcessDetectedSpells(); //reprocess
                    }
                }
            }


        }

        private void Game_OnGameUpdate()
        {
            try
            {
                ObjectCache.myHeroCache.UpdateInfo();
                CheckHeroInDanger();

                if (isChanneling && channelPosition.Distance(ObjectCache.myHeroCache.serverPos2D) > 50
                    && !myHero.IsChannelingImportantSpell())
                {
                    isChanneling = false;
                }
                /*
                if (ObjectCache.menuCache.cache["ResetConfig"].GetValue<bool>())
                {
                    ResetConfig();
                    menu.Item("ResetConfig").SetValue(false);
                }
                */
                var limitDelay = (ObjectCache.menuCache.cache["TickLimiter"] as MenuSlider).CurrentValue; //Tick limiter                
                if (EvadeHelper.fastEvadeMode || EvadeUtils.TickCount - lastTickCount > limitDelay)
                {
                    if (EvadeUtils.TickCount > lastStopEvadeTime)
                    {
                        DodgeSkillShots(); //walking           
                        ContinueLastBlockedCommand();
                    }

                    lastTickCount = EvadeUtils.TickCount;
                }

                EvadeSpell.UseEvadeSpell(); //using spells
                CheckDodgeOnlyDangerous();
                RecalculatePath();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private void RecalculatePath()
        {
            if (ObjectCache.menuCache.cache["RecalculatePosition"].GetValue<bool>() && isDodging)//recheck path
            {
                if (lastPosInfo != null && !lastPosInfo.recalculatedPath)
                {
                    var path = myHero.Path;
                    if (path.Length > 0)
                    {
                        var movePos = path.Last().To2D();

                        if (movePos.Distance(lastPosInfo.position) < 5) //more strict checking
                        {
                            var posInfo = EvadeHelper.CanHeroWalkToPos(movePos, ObjectCache.myHeroCache.moveSpeed, 0, 0, false);
                            if (posInfo.posDangerCount > lastPosInfo.posDangerCount)
                            {
                                lastPosInfo.recalculatedPath = true;

                                if (EvadeSpell.PreferEvadeSpell())
                                {
                                    lastPosInfo = PositionInfo.SetAllUndodgeable();
                                }
                                else
                                {
                                    var newPosInfo = EvadeHelper.GetBestPosition();
                                    if (newPosInfo.posDangerCount < posInfo.posDangerCount)
                                    {
                                        lastPosInfo = newPosInfo;
                                        CheckHeroInDanger();
                                        DodgeSkillShots();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void ContinueLastBlockedCommand()
        {
            if (ObjectCache.menuCache.cache["ContinueMovement"].GetValue<bool>()
                && Situation.ShouldDodge())
            {
                var movePos = lastBlockedUserMoveTo.targetPosition;
                var extraDelay = ObjectCache.menuCache.cache["ExtraPingBuffer"].GetValue<int>();

                if (isDodging == false && lastBlockedUserMoveTo.isProcessed == false
                    && EvadeUtils.TickCount - lastEvadeCommand.timestamp > ObjectCache.gamePing + extraDelay
                    && EvadeUtils.TickCount - lastBlockedUserMoveTo.timestamp < 1500)
                {
                    movePos = movePos + (movePos - ObjectCache.myHeroCache.serverPos2D).Normalized() * EvadeUtils.random.NextFloat(1, 65);

                    if (!EvadeHelper.CheckMovePath(movePos, ObjectCache.gamePing + extraDelay))
                    {
                        //Console.WriteLine("Continue Movement");
                        //myHero.IssueOrder(GameObjectOrder.MoveTo, movePos.To3D());
                        EvadeCommand.MoveTo(movePos);
                        lastBlockedUserMoveTo.isProcessed = true;
                    }
                }
            }
        }

        private void CheckHeroInDanger()
        {
            bool playerInDanger = false;

            foreach (KeyValuePair<int, Spells.Spell> entry in SpellDetector.spells)
            {
                Spells.Spell spell = entry.Value;

                if (lastPosInfo != null && lastPosInfo.dodgeableSpells.Contains(spell.spellID))
                {
                    if (myHero.ServerPosition.To2D().InSkillShot(spell, ObjectCache.myHeroCache.boundingRadius))
                    {
                        playerInDanger = true;
                        break;
                    }

                    if (ObjectCache.menuCache.cache["EnableEvadeDistance"].GetValue<bool>() &&
                        EvadeUtils.TickCount < lastPosInfo.endTime)
                    {
                        playerInDanger = true;
                        break;
                    }
                }
            }

            if (isDodging && !playerInDanger)
            {
                lastDodgingEndTime = EvadeUtils.TickCount;
            }

            if (isDodging == false && !Situation.ShouldDodge())
                return;

            isDodging = playerInDanger;
        }

        private void DodgeSkillShots()
        {
            if (!Situation.ShouldDodge())
            {
                isDodging = false;
                return;
            }

            /*
            if (isDodging && playerInDanger == false) //serverpos test
            {
                myHero.IssueOrder(GameObjectOrder.HoldPosition, myHero, false);
            }*/

            if (isDodging)
            {
                if (lastPosInfo != null)
                {
                    /*foreach (KeyValuePair<int, Spell> entry in SpellDetector.spells)
                    {
                        Spell spell = entry.Value;
                        Console.WriteLine("" + (int)(TickCount-spell.startTime));
                    }*/


                    Vector2 lastBestPosition = lastPosInfo.position;

                    if (ObjectCache.menuCache.cache["ClickOnlyOnce"].GetValue<bool>() == false
                        || !(myHero.Path.Count() > 0 && lastPosInfo.position.Distance(myHero.Path.Last().To2D()) < 5))
                    //|| lastPosInfo.timestamp > lastEvadeOrderTime)
                    {
                        EvadeCommand.MoveTo(lastBestPosition);
                        lastEvadeOrderTime = EvadeUtils.TickCount;
                    }
                }
            }
            else //if not dodging
            {
                //Check if hero will walk into a skillshot
                var path = myHero.Path;
                if (path.Length > 1)
                {
                    var movePos = path.Last().To2D();

                    if (EvadeHelper.CheckMovePath(movePos))
                    {
                        /*if (ObjectCache.menuCache.cache["AllowCrossing"].GetValue<bool>())
                        {
                            var extraDelayBuffer = ObjectCache.menuCache.cache["ExtraPingBuffer"]
                                .GetValue<int>() + 30;
                            var extraDist = ObjectCache.menuCache.cache["ExtraCPADistance"]
                                .GetValue<int>() + 10;
                            var tPosInfo = EvadeHelper.CanHeroWalkToPos(movePos, ObjectCache.myHeroCache.moveSpeed, extraDelayBuffer + ObjectCache.gamePing, extraDist);
                            if (tPosInfo.posDangerLevel == 0)
                            {
                                lastPosInfo = tPosInfo;
                                return;
                            }
                        }*/

                        var posInfo = EvadeHelper.GetBestPositionMovementBlock(movePos);
                        if (posInfo != null)
                        {
                            EvadeCommand.MoveTo(posInfo.position);
                        }
                        return;
                    }
                }
            }
        }

        public void CheckLastMoveTo()
        {
            if (EvadeHelper.fastEvadeMode || ObjectCache.menuCache.cache["FastMovementBlock"].GetValue<bool>())
            {
                if (isDodging == false)
                {
                    if (lastIssueOrderArgs != null && lastIssueOrderArgs.Order == GameObjectOrder.MoveTo)
                    {
                        if (Game.Time * 1000 - lastIssueOrderGameTime < 500)
                        {
                            Game_OnIssueOrder(myHero, lastIssueOrderArgs);
                            lastIssueOrderArgs = null;
                        }
                    }
                }
            }
        }

        public static bool isDodgeDangerousEnabled()
        {
            if (EvadeConfigs.DodgeDangerous == true)
            {
                return true;
            }

            if (ObjectCache.menuCache.cache["DodgeDangerousKeyEnabled"].GetValue<bool>() == true)
            {
                if (ObjectCache.menuCache.cache["DodgeDangerousKey"].GetValue<bool>() == true
                || ObjectCache.menuCache.cache["DodgeDangerousKey2"].GetValue<bool>() == true)
                    return true;
            }

            return false;
        }

        public static void CheckDodgeOnlyDangerous() //Dodge only dangerous event
        {
            bool bDodgeOnlyDangerous = isDodgeDangerousEnabled();

            if (dodgeOnlyDangerous == false && bDodgeOnlyDangerous)
            {
                spellDetector.RemoveNonDangerousSpells();
                dodgeOnlyDangerous = true;
            }
            else
            {
                dodgeOnlyDangerous = bDodgeOnlyDangerous;
            }
        }

        public static void SetAllUndodgeable()
        {
            lastPosInfo = PositionInfo.SetAllUndodgeable();
        }

        private void SpellDetector_OnProcessDetectedSpells()
        {
            ObjectCache.myHeroCache.UpdateInfo();

            if (EvadeConfigs.DodgeSkillShots == false)
            {
                lastPosInfo = PositionInfo.SetAllUndodgeable();
                EvadeSpell.UseEvadeSpell();
                return;
            }

            if (ObjectCache.myHeroCache.serverPos2D.CheckDangerousPos(0)
                || ObjectCache.myHeroCache.serverPos2DExtra.CheckDangerousPos(0))
            {
                if (EvadeSpell.PreferEvadeSpell())
                {
                    lastPosInfo = PositionInfo.SetAllUndodgeable();
                }
                else
                {

                    var posInfo = EvadeHelper.GetBestPosition();
                    var calculationTimer = EvadeUtils.TickCount;
                    var caculationTime = EvadeUtils.TickCount - calculationTimer;

                    //computing time
                    /*if (numCalculationTime > 0)
                    {
                        sumCalculationTime += caculationTime;
                        avgCalculationTime = sumCalculationTime / numCalculationTime;
                    }
                    numCalculationTime += 1;*/

                    //Console.WriteLine("CalculationTime: " + caculationTime);

                    /*if (EvadeHelper.GetHighestDetectedSpellID() > EvadeHelper.GetHighestSpellID(posInfo))
                    {
                        return;
                    }*/
                    if (posInfo != null)
                    {
                        lastPosInfo = posInfo.CompareLastMovePos();

                        var travelTime = ObjectCache.myHeroCache.serverPos2DPing.Distance(lastPosInfo.position) / myHero.MovementSpeed;

                        lastPosInfo.endTime = EvadeUtils.TickCount + travelTime * 1000 - 100;
                    }

                    CheckHeroInDanger();

                    if (EvadeUtils.TickCount > lastStopEvadeTime)
                    {
                        DodgeSkillShots(); //walking           
                    }

                    CheckLastMoveTo();
                    EvadeSpell.UseEvadeSpell(); //using spells
                }
            }
            else
            {
                lastPosInfo = PositionInfo.SetAllDodgeable();
                CheckLastMoveTo();
            }


            //Console.WriteLine("SkillsDodged: " + lastPosInfo.dodgeableSpells.Count + " DangerLevel: " + lastPosInfo.undodgeableSpells.Count);            
        }
    }
}