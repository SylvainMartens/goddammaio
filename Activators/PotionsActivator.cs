﻿using HesaEngine.SDK;
using System.Linq;
using static GodDammAIO.Configs;

namespace GodDammAIO.Activators
{
    internal class PotionsActivator
    {
        private readonly Item HealthPotion = new Item(2003);
        private readonly Item TotalBiscuitofRejuvenation = new Item(2010);
        private readonly Item RefillablePotion = new Item(2031);
        private readonly Item HuntersPotion = new Item(2032);
        private readonly Item CorruptingPotion = new Item(2033);

        private readonly string[] buffName = { "RegenerationPotion", "ItemMiniRegenPotion", "ItemCrystalFlask", "ItemCrystalFlaskJungle", "ItemDarkCrystalFlask" };

        private bool Enabled = true;
        private int HealthPercent = 35;
        private Menu potionsMenu;

        public PotionsActivator(Menu mainMenu)
        {
            potionsMenu = mainMenu.AddSubMenu("Potions");
            potionsMenu.Add(new MenuCheckbox("Enabled", "Enabled", true)).OnValueChanged += (MenuCheckbox checkbox, bool newValue) => { Enabled = newValue; };
            potionsMenu.Add(new MenuSlider("HealthPercent", "When Player HealthPercent <= x%", new Slider(0, 100, 35))).OnValueChanged += (MenuSlider slider, int newValue) => { HealthPercent = newValue; };

            Game.OnUpdate += OnUpdate;
        }

        private void OnUpdate()
        {
            if (!Enabled || Player.IsDead ||
                Player.InFountain() || Player.IsRecalling() ||
                Player.Buffs.Any(b => b.Name.ToLower().Contains("recall") || b.Name.ToLower().Contains("teleport")) ||
                Player.Buffs.Any(x => buffName.Contains(x.Name))
            )
                return;

            if (Player.HealthPercent <= HealthPercent)
            {
                if (HealthPotion.IsReady())
                {
                    HealthPotion.Cast();
                }
                else if (TotalBiscuitofRejuvenation.IsReady())
                {
                    TotalBiscuitofRejuvenation.Cast();
                }
                else if (RefillablePotion.IsReady())
                {
                    RefillablePotion.Cast();
                }
                else if (HuntersPotion.IsReady())
                {
                    HuntersPotion.Cast();
                }
                else if (CorruptingPotion.IsReady())
                {
                    CorruptingPotion.Cast();
                }
            }
        }
    }
}