﻿using HesaEngine.SDK;
using HesaEngine.SDK.Enums;
using HesaEngine.SDK.GameObjects;
using SharpDX;
using System.Linq;
using static GodDammAIO.Configs;

namespace GodDammAIO.Activators
{
    internal class SummonerSmiteActivator
    {
        bool IsSummonersRift => Game.MapId == GameMapId.SummonersRift;
        bool IsTwistedTreeline => Game.MapId == GameMapId.TwistedTreeline;

        Menu SmiteMenu;
        Obj_AI_Minion _minion;
        Spell SmiteSpell;
        readonly string[] SmiteMobs =
        {
            "SRU_Red", "SRU_Blue", "SRU_Dragon_Water", "SRU_Dragon_Fire", "SRU_Dragon_Earth", "SRU_Dragon_Air",
            "SRU_Dragon_Elder", "SRU_Baron", "SRU_Gromp", "SRU_Murkwolf", "SRU_Razorbeak", "SRU_RiftHerald", "SRU_Krug",
            "Sru_Crab", "TT_Spiderboss", "TT_NGolem", "TT_NWolf", "TT_NWraith"
        };

        bool Activated = true;
        bool Range = true;

        bool SRU_Gromp = false;
        bool SRU_Razorbeak = false;
        bool Sru_Crab = false;
        bool SRU_Krug = false;
        bool SRU_Murkwolf = false;

        bool SRU_Baron = true;
        bool SRU_RiftHerald = true;
        bool SRU_Dragon = true;
        bool SRU_Blue = true;
        bool SRU_Red = true;

        bool TT_Spiderboss = true;
        bool TT_NGolem = true;
        bool TT_NWolf = true;
        bool TT_NWraith = true;

        bool KS = true;

        public SummonerSmiteActivator(Menu menu)
        {
            if (SmiteSlot == SpellSlot.Unknown) return;

            SmiteMenu = menu.AddSubMenu("Smite");
            SmiteMenu.Add(new MenuCheckbox("Activated", "Activated", Activated)).OnValueChanged += (sender, value) => { Activated = value; };
            SmiteMenu.Add(new MenuCheckbox("Range", "Smite Range", Range)).OnValueChanged += (sender, value) => { Range = value; };

            //Maps
            if (IsSummonersRift)
            {
                var small = SmiteMenu.AddSubMenu("Small Mobs");
                small.Add(new MenuCheckbox("SRU_Gromp", "Gromp", SRU_Gromp)).OnValueChanged += (sender, value) => { SRU_Gromp = value; };
                small.Add(new MenuCheckbox("SRU_Razorbeak", "Raptors", SRU_Razorbeak)).OnValueChanged += (sender, value) => { SRU_Razorbeak = value; };
                small.Add(new MenuCheckbox("Sru_Crab", "Crab", Sru_Crab)).OnValueChanged += (sender, value) => { Sru_Crab = value; };
                small.Add(new MenuCheckbox("SRU_Krug", "Krug", SRU_Krug)).OnValueChanged += (sender, value) => { SRU_Krug = value; };
                small.Add(new MenuCheckbox("SRU_Murkwolf", "Wolves", SRU_Murkwolf)).OnValueChanged += (sender, value) => { SRU_Murkwolf = value; };

                var big = SmiteMenu.AddSubMenu("Big Mobs");
                big.Add(new MenuCheckbox("SRU_Baron", "Baron Nashor", SRU_Baron)).OnValueChanged += (sender, value) => { SRU_Baron = value; };
                big.Add(new MenuCheckbox("SRU_RiftHerald", "Rift Herald", SRU_RiftHerald)).OnValueChanged += (sender, value) => { SRU_RiftHerald = value; };
                big.Add(new MenuCheckbox("SRU_Dragon", "Dragons", SRU_Dragon)).OnValueChanged += (sender, value) => { SRU_Dragon = value; };
                big.Add(new MenuCheckbox("SRU_Blue", "Blue Buff", SRU_Blue)).OnValueChanged += (sender, value) => { SRU_Blue = value; };
                big.Add(new MenuCheckbox("SRU_Red", "Red Buff", SRU_Red)).OnValueChanged += (sender, value) => { SRU_Red = value; };
            }
            else if (IsTwistedTreeline)
            {
                var mobs = SmiteMenu.AddSubMenu("Mobs");
                mobs.Add(new MenuCheckbox("TT_Spiderboss", "Spiderman", TT_Spiderboss)).OnValueChanged += (sender, value) => { TT_Spiderboss = value; };
                mobs.Add(new MenuCheckbox("TT_NGolem", "Golem Enabled", TT_NGolem)).OnValueChanged += (sender, value) => { TT_NGolem = value; };
                mobs.Add(new MenuCheckbox("TT_NWolf", "Wolf Enabled", TT_NWolf)).OnValueChanged += (sender, value) => { TT_NWolf = value; };
                mobs.Add(new MenuCheckbox("TT_NWraith", "Wraith Enabled", TT_NWraith)).OnValueChanged += (sender, value) => { TT_NWraith = value; };
            }

            SmiteMenu.Add(new MenuCheckbox("KS", "Killsteal with Smite", KS)).OnValueChanged += (sender, value) => { KS = value; };

            SmiteSpell = new Spell(SmiteSlot, 570f, TargetSelector.DamageType.True);
            Drawing.OnDraw += Drawing_OnDraw;
            Game.OnUpdate += Game_OnUpdate;
        }

        private void Game_OnUpdate()
        {
            if (ObjectManager.Player.IsDead || !Activated)
                return;
            SmiteKill();
            // MOBS EXCEPT DRAGON
            _minion = MinionManager.GetMinions(ObjectManager.Player.ServerPosition, 570f, MinionTypes.All, MinionTeam.Neutral).FirstOrDefault(buff => buff.Name.StartsWith(buff.CharData.BaseSkinName) && SmiteMobs.Contains(buff.CharData.BaseSkinName) && !buff.Name.Contains("Mini") && !buff.Name.Contains("Spawn"));

            if (_minion != null)
            {
                if (SmiteMenu.Get<MenuCheckbox>(_minion.CharData.BaseSkinName).Checked)
                {
                    if (SmiteSpell.IsReady())
                    {
                        if (Vector3.Distance(ObjectManager.Player.ServerPosition, _minion.ServerPosition) <= 570)
                        {
                            if (ObjectManager.Player.GetSummonerSpellDamage(_minion, Damage.SummonerSpell.Smite) >= _minion.Health && SmiteSpell.CanCast(_minion))
                            {
                                ObjectManager.Player.Spellbook.CastSpell(SmiteSpell.Slot, _minion);
                                Logger.Log("Smited!");
                            }

                        }
                    }
                }
            }
            else if (SRU_Dragon)
            {
                if (SmiteSpell.IsReady())
                {
                    if (ObjectManager.Dragon != null && ObjectManager.Dragon.IsInRange(ObjectManager.Player, 570))
                    {
                        if (ObjectManager.Player.GetSummonerSpellDamage(ObjectManager.Dragon, Damage.SummonerSpell.Smite) >= ObjectManager.Dragon.Health && SmiteSpell.CanCast(ObjectManager.Dragon))
                        {
                            ObjectManager.Player.Spellbook.CastSpell(SmiteSpell.Slot, ObjectManager.Dragon);
                        }
                    }
                }
            }
            else
            {
                SmiteKill();
            }
        }

        private void Drawing_OnDraw(System.EventArgs args)
        {
            if (Range && Activated)
            {
                Drawing.DrawCircle(ObjectManager.Player.Position, SmiteSpell.Range, Color.Violet);
            }
        }

        private void SmiteKill()
        {
            if (!KS)
            {
                return;
            }
            var smiteableEnemy = TargetSelector.GetTarget(570, TargetSelector.DamageType.True);
            if (smiteableEnemy == null)
            {
                return;
            }
            else if (20 + 8 * ObjectManager.Me.Level >= smiteableEnemy.Health)
            {
                ObjectManager.Player.Spellbook.CastSpell(SmiteSlot, smiteableEnemy);
                Logger.Log("Smited!");
            }
        }
    }
}