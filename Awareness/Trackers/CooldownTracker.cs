﻿using GodDammAIO.Properties;
using HesaEngine.SDK;
using HesaEngine.SDK.Enums;
using HesaEngine.SDK.GameObjects;
using SharpDX;
using SharpDX.Direct3D9;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GodDammAIO.Awareness.Trackers
{
    public class CooldownTracker
    {
        #region Fields
        private readonly SpellSlot[] SpellsSlots = { SpellSlot.Q, SpellSlot.W, SpellSlot.E, SpellSlot.R };
        private readonly Dictionary<string, Texture> SummonerSpells = new Dictionary<string, Texture>();
        private readonly SpellSlot[] SummonersSlots = { SpellSlot.Summoner1, SpellSlot.Summoner2 };

        private Menu Menu;
        #endregion

        #region Properties
        private int hpX = -85;
        private int hpY = 0;

        private bool TrackAllies = true;
        private bool TrackEnemies = true;
        private bool TrackSelf = true;

        #endregion

        #region Methods
        bool drawEndScene = false;
        public CooldownTracker()
        {
            Menu = Awareness.Menu.AddSubMenu("Cooldowns");
            Menu.Add(new MenuSlider("hpX", "HP X Offset", -300, 300, -85)).OnValueChanged += (sender, value) => { hpX = value; };
            Menu.Add(new MenuSlider("hpY", "HP Y Offset", -300, 300, 0)).OnValueChanged += (sender, value) => { hpY = value; };

            Menu.Add(new MenuCheckbox("allies", "Track Allies", true)).OnValueChanged += (sender, value) => { TrackAllies = value; };
            Menu.Add(new MenuCheckbox("enemies", "Track Enemies", true)).OnValueChanged += (sender, value) => { TrackEnemies = value; };
            Menu.Add(new MenuCheckbox("self", "Track Self", true)).OnValueChanged += (sender, value) => { TrackSelf = value; };

            CreateSpellsTextures();
            Drawing.OnEndScene += Drawing_OnEndScene;
        }

        private void CreateSpellsTextures()
        {
            drawEndScene = false;
            Drawing.LoadTexture("summonerheal", Resources.summonerheal);
            Drawing.LoadTexture("summonerhaste", Resources.summonerhaste);
            Drawing.LoadTexture("summonerflash", Resources.summonerflash);
            Drawing.LoadTexture("summonerteleport", Resources.summonerteleport);
            Drawing.LoadTexture("summonerexhaust", Resources.summonerexhaust);
            Drawing.LoadTexture("summonerdot", Resources.summonerdot);
            Drawing.LoadTexture("summonerboost", Resources.summonerboost);
            Drawing.LoadTexture("summonerbarrier", Resources.summonerbarrier);
            Drawing.LoadTexture("summonersmite", Resources.summonersmite);
            Drawing.LoadTexture("s5_summonersmiteduel", Resources.s5_summonersmiteduel);
            Drawing.LoadTexture("s5_summonersmiteplayerganker", Resources.s5_summonersmiteplayerganker);

            drawEndScene = true;
        }
        
        private float AdditionalXOffset(AIHeroClient hero)
        {
            var champName = hero.ChampionName.ToLower();
            switch (champName)
            {
                case "darius":
                    return -4;
            }
            return 0;
        }

        private void Drawing_OnEndScene(EventArgs args)
        {
            if (!drawEndScene) return;
            if (!TrackAllies && !TrackEnemies && !TrackSelf) return;

            foreach (var hero in ObjectManager.Heroes.All.Where(o => !o.IsDead && o.IsHPBarRendered && o.IsVisibleOnScreen))
            {
                /*//Testing only:
                if(hero.IsMe)
                {
                    try
                    {
                        Texture texture = Drawing.GetTexture("summonerheal");
                        if (texture != null && !texture.IsDisposed)
                        {
                            Drawing.DrawTexture(texture, TacticalMap.WorldToMinimap(hero.Position), Color.White);
                        }
                    }
                    catch (Exception) { }
                }
                */
                if (hero.IsMe && !TrackSelf)
                {
                    continue;
                }

                if (hero.IsEnemy && !TrackEnemies)
                {
                    continue;
                }

                if (hero.IsAlly && !hero.IsMe && !TrackAllies)
                {
                    continue;
                }

                foreach (var slot in SpellsSlots)
                {
                    DrawSpell(hero, slot);
                }

                foreach (var slot in SummonersSlots)
                {
                    DrawSummoner(hero, slot);
                }
            }
        }

        private void DrawSpell(AIHeroClient hero, SpellSlot slot)
        {
            if (!drawEndScene) return;
            var spell = hero.Spellbook.GetSpell(slot);
            var color = Color.Cyan;
            var cooldown = spell.CooldownExpires - Game.Time;
            var location = GetSpellLocation(hero, slot);

            var str = slot.ToString();

            if (!spell.IsLearned)
            {
                str = "?";
                color = Color.White;
            }
            else if (spell.Mana != 0 && hero.Mana < spell.Mana)
            {
                str = "M";
                color = Color.Red;
            }
            else if (cooldown > 0)
            {
                str = ((int)Math.Ceiling(cooldown)).ToString();
                color = Color.Orange;
            }

            Drawing.DrawText(string.Format("{0,3}", str), new Vector2(location.X, location.Y), color);
        }

        private void DrawSummoner(AIHeroClient hero, SpellSlot slot)
        {
            if (!drawEndScene) return;
            var spell = hero.Spellbook.GetSpell(slot);
            if (spell.SpellData == null) return;
            var cooldown = spell.CooldownExpires - Game.Time;
            var location = GetSummonerLocation(hero, spell.Slot);
            var color = cooldown > 0 ? Color.Red : Color.Green;

            try
            {
                Texture texture = Drawing.GetTexture(spell.SpellData.Name.ToLower());
                if (texture != null && !texture.IsDisposed)
                {
                    if (Drawing.OnScreen(location))
                    {
                        if (cooldown > 0)
                        {
                            Drawing.DrawTexture(texture, location, Color.Red);
                        }
                        else
                        {
                            Drawing.DrawTexture(texture, location, Color.White);
                        }
                    }
                }
            }
            catch (Exception) { }
            /*
            var lineX = location.X + (hero.IsMe ? 17 : -3);
            Drawing.DrawLine(new Vector2(lineX, location.Y), new Vector2(lineX, location.Y - 16), color, 3f);
            */
            if (cooldown > 0)
            {
                var offset = hero.IsMe ? 22 : -27;
                Drawing.DrawText(string.Format("{0,3}", (int)Math.Ceiling(cooldown)), new Vector2(location.X + offset, location.Y), Color.White);
            }
        }

        private Vector2 GetSpellLocation(AIHeroClient hero, SpellSlot slot)
        {
            var gap = 27;
            var x = hero.HPBarPosition.X + hpX + (hero.IsMe ? 34 : 0) + AdditionalXOffset(hero);
            var y = hero.HPBarPosition.Y + hpY + 23;

            switch (slot)
            {
                case SpellSlot.Q:
                    return new Vector2(x, y);
                case SpellSlot.W:
                    return new Vector2(x + gap, y);
                case SpellSlot.E:
                    return new Vector2(x + 2 * gap, y);
                case SpellSlot.R:
                    return new Vector2(x + 3 * gap, y);
            }

            return Vector2.Zero;
        }

        private Vector2 GetSummonerLocation(AIHeroClient hero, SpellSlot slot)
        {
            var x = hero.HPBarPosition.X + hpX + (hero.IsMe ? 133 : -18) + AdditionalXOffset(hero);
            var y = hero.HPBarPosition.Y + hpY + (hero.IsMe ? -9 : -4);

            var location = new Vector2(x, y);

            if (slot == SpellSlot.Summoner2)
            {
                location.Y += 16;
            }

            return location;
        }

        #endregion
    }
}