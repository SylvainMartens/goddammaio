﻿using HesaEngine.SDK;
using HesaEngine.SDK.Enums;
using SharpDX;
using System;
using static GodDammAIO.Configs;

namespace GodDammAIO.Champions
{
    internal class Alistar : ChampionScript
    {
        public Alistar()
        {
            ChampionMenu = MainMenu.AddSubMenu("Alistar");

            ComboMenu = ChampionMenu.AddSubMenu("Combo");
            ComboMenu.Add(new MenuCheckbox("ComboQ", "Use Q", true));
            ComboMenu.Add(new MenuCheckbox("ComboW", "Use WQ", true));
            ComboMenu.Add(new MenuCheckbox("ComboE", "Use E", true));

            HarassMenu = ChampionMenu.AddSubMenu("Harass");
            HarassMenu.Add(new MenuCheckbox("HarassQ", "Use Q", true));
            HarassMenu.Add(new MenuCheckbox("HarassW", "Use W", true));

            MiscMenu = ChampionMenu.AddSubMenu("Misc");
            MiscMenu.Add(new MenuCheckbox("InterruptQ", "Interrupt with Q", true));
            MiscMenu.Add(new MenuCheckbox("InterruptW", "Interrupt with W", true));
            MiscMenu.Add(new MenuCheckbox("GapCloser", "Anti Gap Closer", true));

            DrawingsMenu = ChampionMenu.AddSubMenu("Drawings");
            DrawingsMenu.Add(new MenuCheckbox("DrawQ", "Draw Q Range", true));
            DrawingsMenu.Add(new MenuCheckbox("DrawW", "Draw W Range", true));

            Q = new Spell(SpellSlot.Q, 365);
            W = new Spell(SpellSlot.W, 650);
            E = new Spell(SpellSlot.E, 575);
            R = new Spell(SpellSlot.R, 0);
            
            W.SetTargetted(0.5f, float.MaxValue);

            Game.OnUpdate += Game_OnUpdate;
            Drawing.OnDraw += Drawing_OnDraw;
            AntiGapcloser.OnEnemyGapcloser += AntiGapcloser_OnEnemyGapcloser;
            Interrupter.OnInterruptableTarget += Interrupter_OnInterruptableTarget;
        }

        private void Interrupter_OnInterruptableTarget(HesaEngine.SDK.GameObjects.AIHeroClient sender, Interrupter.InterruptableTargetEventArgs args)
        {
            if ((int)args.DangerLevel < (int)InterruptableDangerLevel.High || sender.IsAlly)
            {
                return;
            }
            
            if (MiscMenu["InterruptQ"].Cast<MenuCheckbox>().Checked && Q.IsReady() && sender.Distance(Player) < Q.Range)
            {
                Q.Cast();
            }else if (MiscMenu["InterruptW"].Cast<MenuCheckbox>().Checked && W.IsReady() && sender.Distance(Player) < W.Range)
            {
                W.CastOnUnit(sender);
            }
        }

        private void AntiGapcloser_OnEnemyGapcloser(ActiveGapcloser gapcloser)
        {
            if (gapcloser.Sender == null || !gapcloser.Sender.IsEnemy) return;
            
            if (MiscMenu["GapCloser"].Cast<MenuCheckbox>().Checked && W.IsReady() && gapcloser.Sender.Distance(Player) < W.Range)
            {
                W.CastOnUnit(gapcloser.Sender);
            }
        }

        private void Drawing_OnDraw(EventArgs args)
        {
            if(DrawingsMenu["DrawQ"].Cast<MenuCheckbox>().Checked && Q.IsReady())
            {
                Drawing.DrawCircle(Player.Position, Q.Range, Color.SpringGreen);
            }
            if (DrawingsMenu["DrawW"].Cast<MenuCheckbox>().Checked && W.IsReady())
            {
                Drawing.DrawCircle(Player.Position, W.Range, Color.Crimson);
            }
        }

        private void Game_OnUpdate()
        {
            if (Orbwalk.ActiveMode == Orbwalker.OrbwalkingMode.Combo)
            {
                var target = TargetSelector.GetTarget(W.Range, TargetSelector.DamageType.Magical);
                if (target == null || !target.IsValidTarget()) return;

                if (Q.IsReady() && ComboMenu["ComboW"].Cast<MenuCheckbox>().Checked && W.IsReady())
                {
                    W.CastOnUnit(target);
                    var jumpTime = Math.Max(0, Player.Distance(target) - 500) * 10 / 25 + 25;
                    Utility.DelayAction((int)jumpTime, () => Q.Cast());
                }

                if (ComboMenu["ComboQ"].Cast<MenuCheckbox>().Checked && Q.IsReady() && target.Distance(Player) < Q.Range)
                {
                    Q.Cast();
                }
                
                if (ComboMenu["ComboE"].Cast<MenuCheckbox>().Checked && E.IsReady() && target.Distance(Player) < E.Range)
                {
                    E.Cast();
                }
            }

            if (Orbwalk.ActiveMode == Orbwalker.OrbwalkingMode.Harass)
            {
                var target = TargetSelector.GetTarget(W.Range, TargetSelector.DamageType.Magical);
                if (target == null || !target.IsValidTarget()) return;

                if (ComboMenu["HarassQ"].Cast<MenuCheckbox>().Checked && Q.IsReady() && target.Distance(Player) < Q.Range)
                {
                    Q.Cast();
                }

                if (ComboMenu["HarassW"].Cast<MenuCheckbox>().Checked && W.IsReady())
                {
                    W.Cast(target);
                }
            }
        }
    }
}