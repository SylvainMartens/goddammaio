﻿using HesaEngine.SDK;
using SharpDX;
using SharpDX.DirectInput;
using System.Collections.Generic;
using static GodDammAIO.Configs;

namespace GodDammAIO.Menus
{
    internal static class DrawOption
    {
        static DrawOption()
        {
            DrawingsMenu = ChampionMenu.AddSubMenu("Drawings");
        }

        internal static Dictionary<string, bool> CachedBoolValues = new Dictionary<string, bool>();
        internal static Dictionary<string, int> CachedIntValues = new Dictionary<string, int>();

        internal static void CacheBoolValue(MenuCheckbox checkbox, bool addEvent = true)
        {
            if (checkbox == null) return;
            if (!CachedBoolValues.ContainsKey(checkbox.Name))
                CachedBoolValues.Add(checkbox.Name, checkbox.Checked);
            else
                CachedBoolValues[checkbox.Name] = checkbox.Checked;
            if (addEvent)
                checkbox.OnValueChanged += (sender, value) => { CacheBoolValue(sender, false); };
        }

        internal static void CacheBoolValue(MenuKeybind keybind, bool addEvent = true)
        {
            if (keybind == null) return;
            if (!CachedBoolValues.ContainsKey(keybind.Name))
                CachedBoolValues.Add(keybind.Name, keybind.Active);
            else
                CachedBoolValues[keybind.Name] = keybind.Active;
            if (addEvent)
                keybind.OnValueChanged += (sender, value) => { CacheBoolValue(sender, false); };
        }

        internal static void CacheIntValue(MenuSlider slider, bool addEvent = true)
        {
            if (slider == null) return;
            if (!CachedIntValues.ContainsKey(slider.Name))
                CachedIntValues.Add(slider.Name, slider.CurrentValue);
            else
                CachedIntValues[slider.Name] = slider.CurrentValue;
            if (addEvent)
                slider.OnValueChanged += (sender, value) => { CacheIntValue(sender, false); };
        }

        internal static void CacheIntValue(MenuCombo combo, bool addEvent = true)
        {
            if (combo == null) return;
            if (!CachedIntValues.ContainsKey(combo.Name))
                CachedIntValues.Add(combo.Name, combo.CurrentValue);
            else
                CachedIntValues[combo.Name] = combo.CurrentValue;
            if (addEvent)
                combo.OnValueChanged += (sender, value) => { CacheIntValue(sender, false); };
        }

        internal static void AddQ(bool enabled = true)
        {
            AddBool("DrawQ", "Use Q", enabled);
        }

        internal static void AddW(bool enabled = true)
        {
            AddBool("DrawW", "Use W", enabled);
        }

        internal static void AddE(bool enabled = true)
        {
            AddBool("DrawE", "Use E", enabled);
        }

        internal static void AddR(bool enabled = true)
        {
            AddBool("DrawR", "Use R", enabled);
        }
        
        internal static void AddFarm()
        {
            MyManaManager.AddDrawFarm(DrawingsMenu);
        }

        internal static void AddEvent()
        {
            Drawing.OnDraw += delegate
            {
                if (ObjectManager.Player.IsDead || Shop.IsShopOpen)// || Chat.IsChatOpen)
                    return;

                if (GetBool("DrawQ") && Q.IsReady())
                    Drawing.DrawCircle(ObjectManager.Player.Position, Q.Range, Color.SpringGreen, 1);

                if (GetBool("DrawW") && W.IsReady())
                    Drawing.DrawCircle(ObjectManager.Player.Position, W.Range, Color.Crimson, 1);

                if (GetBool("DrawE") && E.IsReady())
                    Drawing.DrawCircle(ObjectManager.Player.Position, E.Range, Color.Lavender, 1);

                if (GetBool("DrawR") && R.IsReady())
                    Drawing.DrawCircle(ObjectManager.Player.Position, R.Range, Color.DodgerBlue, 1);
            };

            //Drawing.OnEndScene += delegate
            //{
            //    if (ObjectManager.Player.IsDead || Shop.IsShopOpen || Chat.IsChatOpen)
            //        return;
            //};
        }

        ///
        internal static void AddBool(string name, string text, bool enabled = true)
        {
            CacheBoolValue(DrawingsMenu.Add(new MenuCheckbox(name, text, enabled)));
        }

        internal static void AddSlider(string name, string defaultName, int defaultValue, int minValue, int maxValue)
        {
            CacheIntValue(DrawingsMenu.Add(new MenuSlider(name, defaultName, new Slider(defaultValue, minValue, maxValue))));
        }

        internal static void AddKey(string name, string text, Key key, MenuKeybindType type = MenuKeybindType.Hold, bool enabled = false)
        {
            CacheBoolValue(DrawingsMenu.Add(new MenuKeybind(name, text, new KeyBind(key, type, enabled))));
        }

        internal static void AddList(string name, string defaultName, string[] values, int defaultValue = 0)
        {
            CacheIntValue(DrawingsMenu.Add(new MenuCombo(name, defaultName, new StringList(values, defaultValue))));
        }

        internal static bool GetBool(string name)
        {
            if (CachedBoolValues.ContainsKey(name)) return CachedBoolValues[name];
            return false;
        }

        internal static int GetSlider(string name)
        {
            if (CachedIntValues.ContainsKey(name)) return CachedIntValues[name];
            return int.MaxValue;
        }

        internal static bool GetKey(string name)
        {
            if (CachedBoolValues.ContainsKey(name)) return CachedBoolValues[name];
            return false;
        }

        internal static int GetList(string name)
        {
            if (CachedIntValues.ContainsKey(name)) return CachedIntValues[name];
            return int.MaxValue;
        }

        internal static bool UseQ => GetBool("DrawQ");
        internal static bool UseW => GetBool("DrawW");
        internal static bool UseE => GetBool("DrawE");
        internal static bool UseR => GetBool("DrawR");
    }
}