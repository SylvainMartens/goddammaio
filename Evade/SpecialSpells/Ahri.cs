﻿using GodDammAIO.Evade.Helpers;
using GodDammAIO.Evade.Spells;
using HesaEngine.SDK;
using HesaEngine.SDK.GameObjects;
using System.Linq;

namespace GodDammAIO.Evade.SpecialSpells
{
    class Ahri : ChampionPlugin
    {
        public void LoadSpecialSpell(SpellData spellData)
        {
            if (spellData.spellName == "AhriOrbofDeception2")
            {
                var hero = ObjectManager.Heroes.All.FirstOrDefault(x => x.ChampionName == "Ahri");
                if (hero != null && hero.CheckTeam())
                {
                    Game.OnUpdate += () => Game_OnUpdate(hero);
                }
            }
        }

        private void Game_OnUpdate(AIHeroClient hero)
        {
            foreach (
                var spell in
                    SpellDetector.detectedSpells.Where(
                        s =>
                            s.Value.heroID == hero.NetworkId &&
                            s.Value.info.spellName.ToLower() == "ahriorbofdeception2"))
            {
                spell.Value.endPos = hero.ServerPosition.To2D();
            }
        }
    }
}