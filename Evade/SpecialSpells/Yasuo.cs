﻿using System.Linq;
using GodDammAIO.Evade.Spells;
using HesaEngine.SDK.Args;
using HesaEngine.SDK;
using HesaEngine.SDK.GameObjects;
using GodDammAIO.Evade.Helpers;

namespace GodDammAIO.Evade.SpecialSpells
{
    class Yasuo : ChampionPlugin
    {
        static Yasuo()
        {

        }

        public void LoadSpecialSpell(SpellData spellData)
        {
            if (spellData.spellName == "YasuoQW" || spellData.spellName == "YasuoQ3W")
            {
                var hero = ObjectManager.Heroes.All.FirstOrDefault(h => h.ChampionName == "Yasuo");
                if (hero != null && hero.CheckTeam())
                {
                    Obj_AI_Base.OnProcessSpellCast += (sender, args) => ProcessSpell_YasuoQW(sender, args, spellData);
                }
            }
        }

        private static void ProcessSpell_YasuoQW(Obj_AI_Base hero, GameObjectProcessSpellCastEventArgs args, SpellData spellData)
        {
            if (hero.IsEnemy && args.SData.Name == "YasuoQ")
            {
                var castTime = (hero.Spellbook.CastTime - Game.Time) * 1000;

                if (castTime > 0)
                {
                    spellData.spellDelay = castTime;
                }
            }
        }
    }
}