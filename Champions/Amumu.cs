﻿using GodDammAIO.Menus;
using HesaEngine.SDK;
using HesaEngine.SDK.Enums;
using HesaEngine.SDK.GameObjects;
using System;
using System.Linq;
using static GodDammAIO.Configs;

namespace GodDammAIO.Champions
{
    internal class Amumu : ChampionScript
    {
        Item RDO;
        Item DFG;
        Item YOY;
        Item BOTK;
        Item HYD;
        Item CUT;
        Item TYM;

        public Amumu()
        {
            Q = new Spell(SpellSlot.Q, 1000);
            W = new Spell(SpellSlot.W, 300);
            E = new Spell(SpellSlot.E, 350);
            R = new Spell(SpellSlot.R, 525);
            Q.SetSkillshot(0.250f, 80, 2000, true, SkillshotType.SkillshotLine);
            
            RDO = new Item(ItemId.Randuins_Omen, 490f);
            HYD = new Item(ItemId.Ravenous_Hydra_Melee_Only, 175f);
            DFG = new Item(ItemId.Deathfire_Grasp, 750f);
            YOY = new Item(ItemId.Youmuus_Ghostblade, 185f);
            BOTK = new Item(ItemId.Blade_of_the_Ruined_King, 450f);
            CUT = new Item(ItemId.Bilgewater_Cutlass, 450f);
            TYM = new Item(ItemId.Tiamat_Melee_Only, 175f);

            //Menu
            CreateChampionMenu();
            ComboOption.AddBool("UseItems", "Use Items", true);
            ComboOption.AddQ();
            ComboOption.AddW();
            ComboOption.AddE();
            ComboOption.AddR();
            ComboOption.AddSlider("CountR", "Num of Enemies in Range to Ult", 1, 5, 2);

            LaneClearOption.AddQ();
            LaneClearOption.AddW();
            LaneClearOption.AddE();

            JungleClearOption.AddQ();
            JungleClearOption.AddW();
            JungleClearOption.AddE();

            MiscOption.AddInterrupt();
            MiscOption.AddBool("InteruptQ", "Use Q to Interupt", true);
            MiscOption.AddBool("InteruptR", "Use R to Interupt", true);
            MiscOption.AddTargetList();

            MiscOption.AddGapcloser();
            MiscOption.AddBool("GapCloserR", "Use R on GapCloser", false);
            MiscOption.AddGapcloserTargetList();

            DrawOption.AddQ();
            DrawOption.AddW();
            DrawOption.AddE();
            DrawOption.AddR();
            DrawOption.AddEvent();

            Game.OnUpdate += Game_OnUpdate;
            AntiGapcloser.OnEnemyGapcloser += AntiGapcloser_OnEnemyGapcloser;
            Interrupter.OnInterruptableTarget += Interrupter_OnInterruptableTarget;
        }

        private void Interrupter_OnInterruptableTarget(AIHeroClient sender, Interrupter.InterruptableTargetEventArgs args)
        {
            if(sender != null && (int)args.DangerLevel >= (int) DangerLevel.Medium && MiscOption.GetMiscTarget(sender.ChampionName))
            {
                if(MiscOption.GetBool("InteruptQ") && Q.IsReady() && sender.Distance(Player) < Q.Range)
                {
                    Q.PredCast(sender);
                }else if(MiscOption.GetBool("InteruptR") && R.IsReady() && sender.Distance(Player) < R.Range)
                {
                    R.PredCast(sender);
                }
            }
        }

        private void AntiGapcloser_OnEnemyGapcloser(ActiveGapcloser gapcloser)
        {
            if(gapcloser.Sender != null && MiscOption.GetGapcloserTarget(gapcloser.Sender.ChampionName))
            {
                if (MiscOption.GetBool("GapCloserR") && R.IsReady() && gapcloser.Sender.Distance(Player) < R.Range)
                {
                    R.PredCast(gapcloser.Sender);
                }
            }
        }

        private void Game_OnUpdate()
        {
            Orbwalk.SetAttack(true);
            if (IsComboMode)
            {
                Combo();
            }else if (IsJungleClearMode)
            {
                JungleClear();
            }else if (IsLaneClearMode)
            {
                WaveClear();
            }
        }

        private void WaveClear()
        {
            var Minions = MinionManager.GetMinions(Player.ServerPosition, Q.Range);

            var useQ = LaneClearOption.UseQ;
            var useW = LaneClearOption.UseW;
            var useE = LaneClearOption.UseE;

            var minions = MinionManager.GetMinions(Player.ServerPosition, Q.Range);

            if (minions.Count > 0)
            {
                if (useQ && Q.IsReady() && minions[0].IsValidTarget() && Player.Distance(minions[0]) <= Q.Range)
                {
                    Q.Cast(minions[0].Position);
                }

                if (useW && W.IsReady() && minions[0].IsValidTarget())
                {
                    if (Player.Distance(minions[0]) <= W.Range && (Player.Spellbook.GetSpell(SpellSlot.W).ToggleState == 1))
                    {
                        W.Cast();
                    }
                    else if (Player.Distance(minions[0]) > W.Range && (Player.Spellbook.GetSpell(SpellSlot.W).ToggleState == 2))
                    {
                        W.Cast();
                    }

                }

                if (useE && E.IsReady() && minions[0].IsValidTarget() && Player.Distance(minions[0]) <= E.Range)
                {
                    E.Cast();
                }
            }
        }

        private void JungleClear()
        {
            var useQ = JungleClearOption.UseQ;
            var useW = JungleClearOption.UseW;
            var useE = JungleClearOption.UseE;

            var allminions = MinionManager.GetMinions(Player.ServerPosition, Q.Range, MinionTypes.All, MinionTeam.Neutral, MinionOrderTypes.MaxHealth);

            if (allminions.Count > 0)
            {
                if (useQ && Q.IsReady() && allminions[0].IsValidTarget() && Player.Distance(allminions[0]) <= Q.Range)
                {
                    Q.Cast(allminions[0].Position);
                }

                if (useW && W.IsReady() && allminions[0].IsValidTarget())
                {
                    if (Player.Distance(allminions[0]) <= W.Range && (Player.Spellbook.GetSpell(SpellSlot.W).ToggleState == 1))
                    {
                        W.Cast();
                    }
                    else if (Player.Distance(allminions[0]) > W.Range && (Player.Spellbook.GetSpell(SpellSlot.W).ToggleState == 2))
                    {
                        W.Cast();
                    }

                }

                if (useE && E.IsReady() && allminions[0].IsValidTarget() && Player.Distance(allminions[0]) <= E.Range)
                {
                    E.Cast();
                }
            }
        }

        private void Combo()
        {
            var target = TargetSelector.GetTarget(Q.Range, TargetSelector.DamageType.Magical);
            if (target == null) return;

            //Combo
            if (ComboOption.UseQ && Q.IsReady() && Player.Distance(target) <= Q.Range)
            {
                Q.PredCast(target);
            }

            if (ComboOption.UseW && W.IsReady())
            {
                if ((Player.Spellbook.GetSpell(SpellSlot.W).ToggleState == 1) && Player.ServerPosition.Distance(target.Position) < W.Range)
                    W.Cast();
                if ((Player.Spellbook.GetSpell(SpellSlot.W).ToggleState == 2) && Player.ServerPosition.Distance(target.Position) > W.Range)
                    W.Cast();
            }

            if (ComboOption.UseE && E.IsReady() && Player.Distance(target) <= E.Range)
                E.Cast();

            if (ComboOption.GetBool("UseItems"))
            {
                if (Player.Distance(target) <= RDO.Range)
                {
                    RDO.Cast(target);
                }
                if (Player.Distance(target) <= HYD.Range)
                {
                    HYD.Cast(target);
                }
                if (Player.Distance(target) <= DFG.Range)
                {
                    DFG.Cast(target);
                }
                if (Player.Distance(target) <= BOTK.Range)
                {
                    BOTK.Cast(target);
                }
                if (Player.Distance(target) <= CUT.Range)
                {
                    CUT.Cast(target);
                }
                if (Player.Distance(target) <= 125f)
                {
                    YOY.Cast();
                }
                if (Player.Distance(target) <= TYM.Range)
                {
                    TYM.Cast(target);
                }
            }

            if (ComboOption.UseR && R.IsReady())
            {
                if (GetNumberHitByR(target) >= ComboOption.GetSlider("CountR"))
                {
                    R.PredCast(target);
                }
            }
        }

        private int GetNumberHitByR(Obj_AI_Base target)
        {
            int totalHit = 0;
            foreach (AIHeroClient current in ObjectManager.Heroes.Enemies)
            {
                if (current.IsValidTarget(R.Range, true))
                {
                    totalHit = totalHit + 1;
                }
            }
            return totalHit;
        }
    }
}