﻿using GodDammAIO.Evade.Spells;

namespace GodDammAIO.Evade.SpecialSpells
{
    interface ChampionPlugin
    {
        void LoadSpecialSpell(SpellData spellData);
    }
}