﻿using HesaEngine.SDK;
using HesaEngine.SDK.Enums;
using HesaEngine.SDK.GameObjects;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GodDammAIO.Activators
{
    internal class ActivatorItem
    {
        public String Name { get; set; }
        public int Id { get; set; }
        public float Range { get; set; }
        public ItemClass Class { get; set; }
        public ItemMode Mode { get; set; }
        public PredictionInput CustomInput { get; set; }
    }

    enum ItemMode
    {
        Targeted, Skillshot, NoTarget
    }

    enum ItemClass
    {
        Offensive, Defensive
    }

    internal class OffensivesItemActivator
    {
        private Menu offensiveMenu;
        private float _lastCheckTick;
        private int ActivatorDelay = 80;
        private bool EnabledAlways = false;
        private bool EnabledCombo = false;
        private int Hitchance = 2;

        private Dictionary<string, bool> ItemValueBool = new Dictionary<string, bool>();
        private Dictionary<string, int> ItemValueSlider = new Dictionary<string, int>();

        private readonly List<ActivatorItem> ItemList = new List<ActivatorItem>
        {
            new ActivatorItem
            {
                Id = 3144,
                Name = "Bilgewater Cutlass",
                Range = 600f,
                Class = ItemClass.Offensive,
                Mode = ItemMode.Targeted
            },
            new ActivatorItem
            {
                Id = 3153,
                Name = "Blade of the Ruined King",
                Range = 600f,
                Class = ItemClass.Offensive,
                Mode = ItemMode.Targeted
            },
            new ActivatorItem
            {
                Id = 3142,
                Name = "Youmuu",
                Range = float.MaxValue,
                Class = ItemClass.Offensive,
                Mode = ItemMode.NoTarget
            }
        };

        public OffensivesItemActivator(Menu mainMenu)
        {
            offensiveMenu = mainMenu.AddSubMenu("Items Offensive");

            var offensiveItems = ItemList.FindAll(item => item.Class == ItemClass.Offensive);
            foreach (var item in offensiveItems)
            {
                var itemMenu = offensiveMenu.AddSubMenu(item.Name);
                AddToItemValueBool(itemMenu.Add(new MenuCheckbox("activator." + item.Id + ".always", "Always", true)));
                AddToItemValueSlider(itemMenu.Add(new MenuSlider("activator." + item.Id + ".onmyhp", "On my HP < then %", new Slider(0, 100, 30))));
                AddToItemValueSlider(itemMenu.Add(new MenuSlider("activator." + item.Id + ".ontghpgreater", "On Target HP > then %", new Slider(0, 100, 40))));
                AddToItemValueSlider(itemMenu.Add(new MenuSlider("activator." + item.Id + ".ontghplesser", "On Target HP < then %", new Slider(0, 100, 40))));
                AddToItemValueBool(itemMenu.Add(new MenuCheckbox("activator." + item.Id + ".ontgkill", "On Target Killable", true)));
                AddToItemValueBool(itemMenu.Add(new MenuCheckbox("activator." + item.Id + ".displaydmg", "Display Damage", true)));
            }
            offensiveMenu.Add(new MenuSlider("offensive.activatordelay", "Global Activator Delay", new Slider(0, 300, 80))).OnValueChanged += (slider, newValue) => { ActivatorDelay = newValue; };
            offensiveMenu.Add(new MenuCheckbox("offensive.enabledalways", "Enabled Always", false)).OnValueChanged += (checkbox, newValue) => { EnabledAlways = newValue; };            
            offensiveMenu.Add(new MenuKeybind("offensive.enabledcombo", "Enabled On Press", SharpDX.DirectInput.Key.Space, MenuKeybindType.Hold)).OnValueChanged += (keybind, newValue) => { EnabledCombo = newValue; };

            offensiveMenu.Add(new MenuCombo("activator.customhitchance", "Hitchance", new string[] { "Low", "Medium", "High", "Very High" }, 2)).OnValueChanged += (slider, newValue) => { Hitchance = newValue; };

            Game.OnUpdate += Game_OnUpdate;
        }

        internal void AddToItemValueBool(MenuCheckbox checkbox)
        {
            ItemValueBool.Add(checkbox.Name, checkbox.Checked);
            checkbox.OnValueChanged += (MenuCheckbox checkbox1, bool newValue) => { ItemValueBool[checkbox.Name] = newValue; };
        }

        internal void AddToItemValueSlider(MenuSlider slider)
        {
            ItemValueSlider.Add(slider.Name, slider.CurrentValue);
            slider.OnValueChanged += (MenuSlider slider1, int newValue) => { ItemValueSlider[slider.Name] = newValue; };
        }

        private void Game_OnUpdate()
        {
            if (!EnabledAlways && !EnabledCombo)
            {
                return;
            }

            if (Environment.TickCount - _lastCheckTick < ActivatorDelay)
            {
                return;
            }

            _lastCheckTick = Environment.TickCount;
            UseOffensive();
        }

        void UseOffensive()
        {
            var offensiveItems = ItemList.FindAll(item => item.Class == ItemClass.Offensive);
            foreach (var item in offensiveItems)
            {
                var selectedTarget = TargetSelector.GetSelectedTarget() != null ? TargetSelector.GetSelectedTarget() : TargetSelector.GetTarget(item.Range, TargetSelector.DamageType.True);
                if (!selectedTarget.IsValidTarget(item.Range) && item.Mode != ItemMode.NoTarget)
                {
                    return;
                }
                if (ItemValueBool["activator." + item.Id + ".always"])
                {
                    UseItem(selectedTarget, item);
                }
                if (ObjectManager.Player.HealthPercent < ItemValueSlider["activator." + item.Id + ".onmyhp"])
                {
                    UseItem(selectedTarget, item);
                }
                if (selectedTarget.HealthPercent < ItemValueSlider["activator." + item.Id + ".ontghplesser"] && !ItemValueBool["activator." + item.Id + ".ontgkill"])
                {
                    UseItem(selectedTarget, item);
                }
                if (selectedTarget.HealthPercent > ItemValueSlider["activator." + item.Id + ".ontghpgreater"])
                {
                    UseItem(selectedTarget, item);
                }
                if (selectedTarget.Health < ObjectManager.Player.GetSpellDamage(selectedTarget, GetItemSpellSlot(item)) && ItemValueBool["activator." + item.Id + ".ontgkill"])
                {
                    UseItem(selectedTarget, item);
                }
            }
        }

        void UseItem(Obj_AI_Base target, ActivatorItem item)
        {
            if (!Shop.HasItem(item.Id) || !Shop.CanUseItem(item.Id))
            {
                return;
            }

            switch (item.Mode)
            {
                case ItemMode.Targeted:
                    Shop.UseItem(item.Id, target);
                break;
                case ItemMode.NoTarget:
                    Shop.UseItem(item.Id, ObjectManager.Player);
                break;
                case ItemMode.Skillshot:
                    if (item.CustomInput == null)
                    {
                        return;
                    }
                    var customPred = Core.Prediction.GetPrediction(item.CustomInput);
                    if (customPred.Hitchance >= GetHitchance())
                    {
                        Shop.UseItem(item.Id, customPred.CastPosition);
                    }
                break;
            }
        }

        SpellSlot GetItemSpellSlot(ActivatorItem item)
        {
            foreach (var it in ObjectManager.Player.InventoryItems.Where(it => (int)it.Id == item.Id))
            {
                return it.SpellSlot != SpellSlot.Unknown ? it.SpellSlot : SpellSlot.Unknown;
            }
            return SpellSlot.Unknown;
        }

        HitChance GetHitchance()
        {
            switch (Hitchance)
            {
                case 0: return HitChance.Low;
                case 1: return HitChance.Medium;
                case 2: return HitChance.High;
                case 3: return HitChance.VeryHigh;
                default: return HitChance.Medium;
            }
        }
    }
}