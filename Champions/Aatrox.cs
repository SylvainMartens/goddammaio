﻿using HesaEngine.SDK;
using HesaEngine.SDK.Enums;
using HesaEngine.SDK.GameObjects;
using SharpDX;
using System.Linq;
using static GodDammAIO.Configs;

namespace GodDammAIO.Champions
{
    internal class Aatrox : ChampionScript
    {
        bool WHealing;
        Item HDR, BKR, TMT, BWC, YOU;

        #region Combo Settings
        bool UseItems = true;
        bool UseQCombo = true;
        bool NoQNear = true;
        bool UseWCombo = true;
        bool UseECombo = true;
        bool EbeforeQ = false;
        bool UseRCombo = true;
        int SwitchLife = 40;
        int SwitchPower = 40;
        #endregion

        #region Harass Settings
        bool UseEHarass = true;
        #endregion

        #region Misc Settings
        bool InterruptSpells = true;
        bool KillstealE = true;
        bool KillstealR = false;
        #endregion

        #region Drawings Settings
        bool QRange = true;
        bool ERange = true;
        bool RRange = true;
        #endregion

        public Aatrox()
        {
            ChampionMenu = MainMenu.AddSubMenu("Aatrox");

            ComboMenu = ChampionMenu.AddSubMenu("Combo");
            ComboMenu.Add(new MenuCheckbox("UseItems", "Use Items", UseItems)).OnValueChanged += (sender, value) => { UseItems = value; };
            ComboMenu.Add(new MenuCheckbox("UseQCombo", "Use Q", UseQCombo)).OnValueChanged += (sender, value) => { UseQCombo = value; };
            ComboMenu.Add(new MenuCheckbox("NoQNear", "Don't use Q near", NoQNear)).OnValueChanged += (sender, value) => { NoQNear = value; };
            ComboMenu.Add(new MenuCheckbox("UseWCombo", "Use W", UseWCombo)).OnValueChanged += (sender, value) => { UseWCombo = value; };
            ComboMenu.Add(new MenuCheckbox("UseECombo", "Use E", UseECombo)).OnValueChanged += (sender, value) => { UseECombo = value; };
            ComboMenu.Add(new MenuCheckbox("EbeforeQ", "Use E before Q", EbeforeQ)).OnValueChanged += (sender, value) => { EbeforeQ = value; };
            ComboMenu.Add(new MenuCheckbox("UseRCombo", "Use R", UseRCombo)).OnValueChanged += (sender, value) => { UseRCombo = value; };
            ComboMenu.Add(new MenuSlider("SwitchLife", "Change to Life", 1, 100, SwitchLife)).OnValueChanged += (sender, value) => { SwitchLife = value; };
            ComboMenu.Add(new MenuSlider("SwitchPower", "Change to Power", 1, 100, SwitchPower)).OnValueChanged += (sender, value) => { SwitchPower = value; };

            HarassMenu = ChampionMenu.AddSubMenu("Harass");
            HarassMenu.Add(new MenuCheckbox("UseEHarass", "Use E", UseEHarass)).OnValueChanged += (sender, value) => { UseEHarass = value; };

            MiscMenu = ChampionMenu.AddSubMenu("Misc");
            MiscMenu.Add(new MenuCheckbox("InterruptSpells", "Interrupt spells with Q", InterruptSpells)).OnValueChanged += (sender, value) => { InterruptSpells = value; };
            MiscMenu.Add(new MenuCheckbox("KillstealE", "Killsteal with E", KillstealE)).OnValueChanged += (sender, value) => { KillstealE = value; };
            MiscMenu.Add(new MenuCheckbox("KillstealR", "Killsteal with R", KillstealR)).OnValueChanged += (sender, value) => { KillstealR = value; };

            DrawingsMenu = ChampionMenu.AddSubMenu("Drawings");
            DrawingsMenu.Add(new MenuCheckbox("QRange", "Show Q Range", QRange)).OnValueChanged += (sender, value) => { QRange = value; };
            DrawingsMenu.Add(new MenuCheckbox("ERange", "Show E Range", ERange)).OnValueChanged += (sender, value) => { ERange = value; };
            DrawingsMenu.Add(new MenuCheckbox("RRange", "Show R Range", RRange)).OnValueChanged += (sender, value) => { RRange = value; };

            //Create the spells
            Q = new Spell(SpellSlot.Q, 650);
            W = new Spell(SpellSlot.W, Player.AttackRange + 25);
            E = new Spell(SpellSlot.E, 950); //1000?
            R = new Spell(SpellSlot.R, 550); //300?

            Q.SetSkillshot(0.5f, 180f, 1800f, false, SkillshotType.SkillshotCircle); //width tuned
            E.SetSkillshot(0.5f, 150f, 1200f, false, SkillshotType.SkillshotCone);
            
            //Create the items
            HDR = new Item(3074, 175f);
            TMT = new Item(3077, 175f);
            BKR = new Item(3153, 450f);
            BWC = new Item(3144, 450f);
            YOU = new Item(3142, 185f);

            //Create the event handlers
            Game.OnUpdate += Game_OnUpdate;
            Drawing.OnDraw += Drawing_OnDraw;
            Interrupter.OnInterruptableTarget += Interrupter_OnInterruptableTarget;
        }

        private void Interrupter_OnInterruptableTarget(AIHeroClient sender, Interrupter.InterruptableTargetEventArgs args)
        {
            if (!InterruptSpells) return;
            if (Player.Distance(sender) < Q.Range && Q.IsReady())
            {
                Q.Cast(sender, false, true);
            }
        }
        
        private void Drawing_OnDraw(System.EventArgs args)
        {
            if (Q.IsReady() && QRange)
                Drawing.DrawCircle(Player.Position, Q.Range, Color.DodgerBlue, 2);

            if (E.IsReady() && ERange)
                Drawing.DrawCircle(Player.Position, E.Range, Color.CornflowerBlue, 2);

            if (R.IsReady() && RRange)
                Drawing.DrawCircle(Player.Position, R.Range, Color.BlueViolet, 2);
        }

        private void Game_OnUpdate()
        {
            //foreach (var buff in ObjectManager.Player.Buffs) { Game.PrintChat(buff.DisplayName); }
            CheckWHealing();
            if (Player.IsDead) return;

            Orbwalker.Attack = true;
            Orbwalker.Move = true;

            var useEKS = KillstealE && E.IsReady();
            var useRKS = KillstealR && R.IsReady();

            if (Orbwalk.ActiveMode == Orbwalker.OrbwalkingMode.Combo)
                Combo();
            else if (Orbwalk.ActiveMode == Orbwalker.OrbwalkingMode.Harass)
                Harass();
            
            if (useRKS && Orbwalk.ActiveMode != Orbwalker.OrbwalkingMode.Combo)
                DoKillstealR();
            if (useEKS)
                DoKillstealE();
        }

        private static float GetComboDamage(Obj_AI_Base qTarget)
        {
            var ComboDamage = 0d;

            if (Q.IsReady())
                ComboDamage += Player.GetSpellDamage(qTarget, SpellSlot.Q);

            if (R.IsReady())
                ComboDamage += Player.GetSpellDamage(qTarget, SpellSlot.R);

            if (E.IsReady())
                ComboDamage += Player.GetSpellDamage(qTarget, SpellSlot.E);

            if (IgniteSlot != SpellSlot.Unknown && Player.Spellbook.GetSpellState(IgniteSlot) == SpellState.Ready)
                ComboDamage += Player.GetSummonerSpellDamage(qTarget, Damage.SummonerSpell.Ignite);

            return (float)ComboDamage;
        }

        private void Combo()
        {
            Orbwalker.Attack = true;

            var qTarget = TargetSelector.GetTarget(Q.Range, TargetSelector.DamageType.Magical);
            var eTarget = TargetSelector.GetTarget(E.Range, TargetSelector.DamageType.Magical);
            var rTarget = TargetSelector.GetTarget(R.Range, TargetSelector.DamageType.Magical);
            
            if (UseItems)
            {
                BKR.Cast(qTarget);
                YOU.Cast();
                BWC.Cast(qTarget);
                if (Player.Distance(qTarget) <= HDR.Range)
                {
                    HDR.Cast(qTarget);
                }
                if (Player.Distance(qTarget) <= TMT.Range)
                {
                    TMT.Cast(qTarget);
                }
            }
            if (Q.IsReady() && E.IsReady() && R.IsReady() && GetComboDamage(qTarget) >= qTarget.Health)
            {

                if (qTarget != null && UseQCombo && Q.IsReady())
                    if ((NoQNear && Player.Distance(qTarget) < Player.AttackRange + 50)
                        || (EbeforeQ && E.IsReady()))
                        return;
                Q.Cast(qTarget, true, true);

                if (rTarget != null && UseRCombo && R.IsReady())
                    R.Cast(rTarget, true);

                if (UseWCombo && W.IsReady())
                {
                    if (Player.Health < (Player.MaxHealth * (SwitchLife) * 0.01) && !WHealing)
                        W.Cast();
                    else
                        if (Player.Health > (Player.MaxHealth * (SwitchPower) * 0.01) && WHealing)
                        W.Cast();
                }

                if (eTarget != null && UseECombo && E.IsReady())
                    E.Cast(eTarget, true);

            }
            else
            {
                if (qTarget != null && UseQCombo && Q.IsReady())
                    if ((NoQNear && Player.Distance(qTarget) < Player.AttackRange + 50) || (EbeforeQ && E.IsReady()))
                        return;
                Q.Cast(qTarget, true, true);

                if (UseWCombo && W.IsReady())
                {
                    if (Player.Health < (Player.MaxHealth * 0.4) && !WHealing)
                        W.Cast();
                    else
                        if (Player.Health > (Player.MaxHealth * 0.55) && WHealing)
                        W.Cast();
                }

                if (eTarget != null && UseECombo && E.IsReady())
                    E.Cast(eTarget, true);
            }
        }

        private void Harass()
        {
            if (!UseEHarass) return;

            var eTarget = TargetSelector.GetTarget(E.Range, TargetSelector.DamageType.Magical);
            
            if (eTarget != null && E.IsReady())
                E.Cast(eTarget, true);
        }

        private void CheckWHealing()
        {
            foreach (var buff in ObjectManager.Player.Buffs)
            {
                if (buff.DisplayName == "AatroxWLife")
                    WHealing = true;
                else if (buff.DisplayName == "AatroxWPower")
                    WHealing = false;
            }
        }

        private void DoKillstealE()
        {
            foreach (var hero in ObjectManager.Heroes.Enemies.Where(hero => hero.IsValidTarget(E.Range)))
            {
                if (E.IsReady() && hero.Distance(ObjectManager.Player) <= E.Range &&
                    Damage.GetSpellDamage(Player, hero, SpellSlot.E) >= hero.Health + 20)
                    E.CastIfHitchanceEquals(hero, HitChance.High, true);
            }
        }

        private void DoKillstealR()
        {
            foreach (var hero in ObjectManager.Heroes.Enemies.Where(hero => hero.IsValidTarget(E.Range)))
            {
                if (R.IsReady() && hero.Distance(ObjectManager.Player) <= R.Range &&
                    Damage.GetSpellDamage(Player, hero, SpellSlot.R) >= hero.Health + 20)
                    R.Cast();
            }
        }

    }
}