﻿namespace GodDammAIO.Evade.Core
{
    internal static class EvadeConfigs
    {
        internal static bool DodgeSkillShots = false;
        internal static bool ActivateEvadeSpells = false;
        internal static bool DodgeDangerous = false;
        internal static bool DodgeFOWSpells = false;
        internal static bool DodgeCircularSpells = false;
    }
}