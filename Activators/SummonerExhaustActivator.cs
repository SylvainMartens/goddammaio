﻿using HesaEngine.SDK;
using HesaEngine.SDK.GameObjects;
using static GodDammAIO.Configs;

namespace GodDammAIO.Activators
{
    internal class SummonerExhaustActivator
    {
        Menu ExhaustMenu;
        Menu AlliesMenu;
        Menu EnemiesMenu;

        bool Enabled = true;

        Spell ExhaustSpell;

        public SummonerExhaustActivator(Menu menu)
        {
            if (ExhaustSlot == HesaEngine.SDK.Enums.SpellSlot.Unknown) return;

            ExhaustMenu = menu.AddSubMenu("Exhaust");
            ExhaustMenu.Add(new MenuCheckbox("Enabled", "Enabled", Enabled)).OnValueChanged += (sender, value) => { Enabled = value; };

            AlliesMenu = ExhaustMenu.AddSubMenu("Allies to use for");
            foreach(var ally in ObjectManager.Heroes.Allies)
            {
                AlliesMenu.Add(new MenuCheckbox("Exhaust" + ally.Name.ToLower(), "Use for " + ally.Name, true));
                AlliesMenu.Add(new MenuSlider("Exhausthp" + ally.Name.ToLower(), "Use for " + ally.Name + " On HP %", 1, 100, (int)(TargetSelector.GetPriority(ally) * 10)));
            }

            EnemiesMenu = ExhaustMenu.AddSubMenu("Enemies to use on");
            foreach (var enemy in ObjectManager.Heroes.Enemies)
            {
                EnemiesMenu.Add(new MenuCheckbox("Exhaust" + enemy.Name.ToLower(), "Use on " + enemy.Name, true));
                EnemiesMenu.Add(new MenuSlider("Exhausthp" + enemy.Name.ToLower(), "Use on " + enemy.Name + " On HP %", 1, 100, (int)(TargetSelector.GetPriority(enemy) * 10)));
            }

            ExhaustSpell = new Spell(ExhaustSlot, 750);

            AttackableUnit.OnDamage += AttackableUnit_OnDamage;
            Game.OnUpdate += Game_OnUpdate;
        }

        private void Game_OnUpdate()
        {
            
        }

        private void AttackableUnit_OnDamage(AttackableUnit sender, HesaEngine.SDK.Args.AttackableUnitDamageEventArgs args)
        {
            if (args.Target is AIHeroClient target && sender is AIHeroClient && target.IsAlly)
            {
                if (!Enabled || !AlliesMenu.Get<MenuCheckbox>("Exhaust" + target.Name.ToLower()).Checked || !ExhaustSpell.IsReady() || sender.Distance(Player) > 750 || target.IsUnkillable)
                    return;

                if (EnemiesMenu.Get<MenuCheckbox>("Exhaust" + sender.Name.ToLower()).Checked && 
                    (EnemiesMenu.Get<MenuSlider>("Exhausthp" + sender.Name.ToLower()).CurrentValue >= sender.HealthPercent)
                    || AlliesMenu.Get<MenuSlider>("Exhausthp" + target.Name.ToLower()).CurrentValue >= (int)(((target.TotalShieldHealth - args.Damage) / target.MaxHealth) * 100)
                    )
                {
                    Player.Spellbook.CastSpell(ExhaustSlot, sender);
                }
            }
        }
    }
}