﻿using HesaEngine.SDK;
using HesaEngine.SDK.Enums;
using HesaEngine.SDK.GameObjects;
using static HesaEngine.SDK.Orbwalker;

namespace GodDammAIO
{
    internal static class Configs
    {
        internal static Menu MainMenu;
        internal static OrbwalkerInstance Orbwalk => Core.Orbwalker;
        internal static AIHeroClient Player => ObjectManager.Me;
        internal static Spell Q, W, E, R;
        internal static SpellSlot HealSlot, FlashSlot, BarrierSlot, IgniteSlot, ExhaustSlot, SmiteSlot = SpellSlot.Unknown;
        internal static Menu ChampionMenu, ComboMenu, HarassMenu, LaneClearMenu, JungleClearMenu, LasthitMenu, ItemsMenu, KillStealMenu, MiscMenu, DrawingsMenu, FleeMenu;

        internal static bool IsComboMode => Orbwalk.ActiveMode == OrbwalkingMode.Combo;
        internal static bool IsHarassMode => Orbwalk.ActiveMode == OrbwalkingMode.Harass;
        internal static bool IsLaneClearMode => Orbwalk.ActiveMode == OrbwalkingMode.LaneClear;
        internal static bool IsLastHitMode => Orbwalk.ActiveMode == OrbwalkingMode.LastHit;
        internal static bool IsJungleClearMode => Orbwalk.ActiveMode == OrbwalkingMode.JungleClear;
        internal static bool IsFarmMode => Orbwalk.ActiveMode == OrbwalkingMode.LaneClear || Orbwalk.ActiveMode == OrbwalkingMode.JungleClear;
        internal static bool IsFleeMode => Orbwalk.ActiveMode == OrbwalkingMode.Flee;
        internal static bool IsNoneMode => Orbwalk.ActiveMode == OrbwalkingMode.None;

        internal static int LastQTime = 0;
        internal static int LastWTime = 0;
        internal static int LastETime = 0;
        internal static int LastRTime = 0;
        internal static int LastCastTime = 0;
        internal static int LastCatchTime = 0;

        internal static void CreateChampionMenu()
        {
            ChampionMenu = MainMenu.AddSubMenu(Player.ChampionName);
        }
    }
}