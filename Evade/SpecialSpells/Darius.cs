﻿using GodDammAIO.Evade.Helpers;
using GodDammAIO.Evade.Spells;
using HesaEngine.SDK;
using HesaEngine.SDK.GameObjects;
using System.Linq;

namespace GodDammAIO.Evade.SpecialSpells
{
    class Darius : ChampionPlugin
    {
        static Darius()
        {
            // todo: fix for multiple darius' on same team (one for all)
        }

        public void LoadSpecialSpell(SpellData spellData)
        {
            if (spellData.spellName == "DariusCleave")
            {
                var hero = ObjectManager.Heroes.All.FirstOrDefault(x => x.ChampionName == "Darius");
                if (hero != null && hero.CheckTeam())
                {
                    Game.OnUpdate += () => Game_OnUpdate(hero);
                }
            }
        }

        private void Game_OnUpdate(AIHeroClient hero)
        {
            foreach (var spell in SpellDetector.detectedSpells.Where(x => x.Value.heroID == hero.NetworkId))
            {
                if (spell.Value.info.spellName == "DariusCleave")
                {
                    spell.Value.startPos = hero.ServerPosition.To2D();
                    spell.Value.endPos = hero.ServerPosition.To2D() + spell.Value.direction * spell.Value.info.range;
                }
            }
        }
    }
}