﻿using HesaEngine.SDK;
using HesaEngine.SDK.GameObjects;
using static GodDammAIO.Configs;

namespace GodDammAIO.Activators
{
    internal class SummonerBarrierActivator
    {
        Menu BarrierMenu;
        bool Enabled = true;
        int MinimumHP = 5;
        Spell BarrierSpell;

        public SummonerBarrierActivator(Menu menu)
        {
            if (BarrierSlot == HesaEngine.SDK.Enums.SpellSlot.Unknown) return;

            BarrierMenu = menu.AddSubMenu("Barrier");
            BarrierMenu.Add(new MenuCheckbox("Enabled", "Enabled", Enabled)).OnValueChanged += (sender, value) => { Enabled = value; };
            MinimumHP = ((int)TargetSelector.GetPriority(Player)) * 5;
            BarrierMenu.Add(new MenuSlider("MinimumHP", "Minimum HP %", 1, 100, MinimumHP)).OnValueChanged += (sender, value) => { MinimumHP = value; };
            BarrierSpell = new Spell(BarrierSlot, 1);

            AttackableUnit.OnDamage += AttackableUnit_OnDamage;
        }

        private void AttackableUnit_OnDamage(AttackableUnit sender, HesaEngine.SDK.Args.AttackableUnitDamageEventArgs args)
        {
            if (!Enabled || !args.Target.IsMe || !BarrierSpell.IsReady() || Player.IsUnkillable)// || !ObjectManager.Heroes.Enemies.Any(e => e.IsValid() && !e.IsDead && e.IsInRange(args.Target, 1250)) && args.Target.Health > args.Damage)
                return;
            
            if (MinimumHP >= args.Target.HealthPercent || args.Damage >= Player.TotalShieldHealth)
            {
                Player.Spellbook.CastSpell(BarrierSlot);
            }
        }
    }
}