﻿using HesaEngine.SDK;
using HesaEngine.SDK.Enums;
using HesaEngine.SDK.GameObjects;
using SharpDX;
using System.Linq;
using static GodDammAIO.Configs;

namespace GodDammAIO.Champions
{
    internal class Ahri : ChampionScript
    {
        #region Combo Settings
        bool RCombo = false;
        int UseR = 1;
        bool RKill = true;
        #endregion

        #region Harass Settings
        bool HarQ = true;
        bool HarW = true;
        bool HarE = true;
        int HarMana = 50;
        #endregion

        #region LaneClear Settings
        bool UseQLane = true;
        bool UseWLane = true;
        #endregion

        #region JungleClear Settings
        bool UseQJungle = true;
        bool UseWJungle = true;
        #endregion

        #region Killsteal Settings
        bool SmartKS = true;
        bool AutoIgnite = true;
        #endregion

        #region Misc Settings
        bool TowerE = true;
        bool IntE = true;
        bool IntMed = false;
        #endregion

        #region Drawings Settings
        bool Qdraw = true;
        bool Wdraw = true;
        bool Edraw = true;
        bool Rdraw = true;
        bool LagFree = true;
        int CircleThickness = 1;
        #endregion

        public Ahri()
        {
            Q = new Spell(SpellSlot.Q, 840);
            W = new Spell(SpellSlot.W, 800);
            E = new Spell(SpellSlot.E, 975);
            R = new Spell(SpellSlot.R, 550);

            Q.SetSkillshot(Q.Instance.SpellData.SpellDataInfos.SpellCastTime, 90f, Q.Instance.SpellData.SpellDataInfos.MissileSpeed, false, SkillshotType.SkillshotLine);
            E.SetSkillshot(E.Instance.SpellData.SpellDataInfos.SpellCastTime, 100f, E.Instance.SpellData.SpellDataInfos.MissileSpeed, true, SkillshotType.SkillshotLine);

            ChampionMenu = MainMenu.AddSubMenu("Ahri");
            ComboMenu = ChampionMenu.AddSubMenu("Combo");
            ComboMenu.Add(new MenuKeybind("RCombo", "Use R Combo", new KeyBind(SharpDX.DirectInput.Key.T, MenuKeybindType.Toggle, RCombo))).OnValueChanged += (sender, value) => { RCombo = value; };
            ComboMenu.Add(new MenuCombo("UseR", "Usage Of R", new StringList(new[] { "To Mouse", "To Enemy", "Don't Use" }, UseR))).OnValueChanged += (sender, value) => { UseR = value; };
            ComboMenu.Add(new MenuCheckbox("RKill", "Only Use R When Killable", RKill)).OnValueChanged += (sender, value) => { RKill = value; };

            HarassMenu = ChampionMenu.AddSubMenu("Harass");
            HarassMenu.Add(new MenuCheckbox("HarQ", "Use Q In Harass", HarQ)).OnValueChanged += (sender, value) => { HarQ = value; };
            HarassMenu.Add(new MenuCheckbox("HarW", "Use W In Harass", HarW)).OnValueChanged += (sender, value) => { HarW = value; };
            HarassMenu.Add(new MenuCheckbox("HarE", "Use E In Harass", HarE)).OnValueChanged += (sender, value) => { HarE = value; };
            HarassMenu.Add(new MenuSlider("HarMana", "Min Mana %", 0, 100, HarMana)).OnValueChanged += (sender, value) => { HarMana = value; };

            LaneClearMenu = ChampionMenu.AddSubMenu("Lane Clear");
            LaneClearMenu.Add(new MenuCheckbox("UseQLane", "Use Q", UseQLane)).OnValueChanged += (sender, value) => { UseQLane = value; };
            LaneClearMenu.Add(new MenuCheckbox("UseWLane", "Use W", UseWLane)).OnValueChanged += (sender, value) => { UseWLane = value; };

            JungleClearMenu = ChampionMenu.AddSubMenu("Jungle Clear");
            JungleClearMenu.Add(new MenuCheckbox("UseQJungle", "Use Q", UseQJungle)).OnValueChanged += (sender, value) => { UseQJungle = value; };
            JungleClearMenu.Add(new MenuCheckbox("UseWJungle", "Use W", UseWJungle)).OnValueChanged += (sender, value) => { UseWJungle = value; };

            KillStealMenu = ChampionMenu.AddSubMenu("Kill Steal");
            KillStealMenu.Add(new MenuCheckbox("SmartKS", "Smart KillSteal", SmartKS)).OnValueChanged += (sender, value) => { SmartKS = value; };
            KillStealMenu.Add(new MenuCheckbox("AutoIgnite", "Auto Ignite", AutoIgnite)).OnValueChanged += (sender, value) => { AutoIgnite = value; };

            MiscMenu = ChampionMenu.AddSubMenu("Misc");
            MiscMenu.Add(new MenuCheckbox("TowerE", "Auto E Under Turret", TowerE)).OnValueChanged += (sender, value) => { TowerE = value; };
            MiscMenu.Add(new MenuCheckbox("IntE", "Auto Interrupt with E", IntE)).OnValueChanged += (sender, value) => { IntE = value; };
            MiscMenu.Add(new MenuCheckbox("IntMed", "Interrupt Medium Danger Spells", IntMed)).OnValueChanged += (sender, value) => { IntMed = value; };

            DrawingsMenu = ChampionMenu.AddSubMenu("Drawings");
            DrawingsMenu.Add(new MenuCheckbox("Qdraw", "Draw Q Range", Qdraw)).OnValueChanged += (sender, value) => { Qdraw = value; };
            DrawingsMenu.Add(new MenuCheckbox("Wdraw", "Draw W Range", Wdraw)).OnValueChanged += (sender, value) => { Wdraw = value; };
            DrawingsMenu.Add(new MenuCheckbox("Edraw", "Draw E Range", Edraw)).OnValueChanged += (sender, value) => { Edraw = value; };
            DrawingsMenu.Add(new MenuCheckbox("Rdraw", "Draw R Range", Rdraw)).OnValueChanged += (sender, value) => { Rdraw = value; };
            DrawingsMenu.Add(new MenuCheckbox("LagFree", "Lag Free Cirlces", LagFree)).OnValueChanged += (sender, value) => { LagFree = value; };
            DrawingsMenu.Add(new MenuSlider("CircleThickness", "Circles Thickness", 1, 10, CircleThickness)).OnValueChanged += (sender, value) => { CircleThickness = value; };
            
            Game.OnUpdate += Game_OnUpdate;
            Drawing.OnDraw += Drawing_OnDraw;
            Interrupter.OnInterruptableTarget += Interrupter_OnInterruptableTarget;
        }

        private void Interrupter_OnInterruptableTarget(HesaEngine.SDK.GameObjects.AIHeroClient sender, Interrupter.InterruptableTargetEventArgs args)
        {
            if (!IntE || !E.IsReady() || !sender.IsValidTarget(Q.Range))
            {
                return;
            }
            if (args.DangerLevel == Interrupter.DangerLevel.High || args.DangerLevel == Interrupter.DangerLevel.Medium && IntMed)
            {
                E.Cast(sender);
            }
        }

        private void Drawing_OnDraw(System.EventArgs args)
        {
            if (!LagFree)
            {
                if (Qdraw)
                {
                    Drawing.DrawCircle(Player.Position, Q.Range, Color.Cyan, CircleThickness);
                }

                if (Wdraw)
                {
                    Drawing.DrawCircle(Player.Position, W.Range, Color.Chartreuse, CircleThickness);
                }

                if (Edraw)
                {
                    Drawing.DrawCircle(Player.Position, E.Range, Color.BlueViolet, CircleThickness);
                }

                if (Rdraw)
                {
                    Drawing.DrawCircle(Player.Position, R.Range, Color.Crimson, CircleThickness);
                }
            }
            else
            {
                if (Qdraw)
                {
                    Drawing.DrawCircle(Player.Position, Q.Range, Color.Cyan);
                }

                if (Wdraw)
                {
                    Drawing.DrawCircle(Player.Position, W.Range, Color.Chartreuse);
                }

                if (Edraw)
                {
                    Drawing.DrawCircle(Player.Position, E.Range, Color.BlueViolet);
                }

                if (Rdraw)
                {
                    Drawing.DrawCircle(Player.Position, R.Range, Color.Crimson);
                }
            }
        }

        private void Game_OnUpdate()
        {
            if (Player.IsDead)
            {
                return;
            }

            if (Orbwalk.ActiveMode == Orbwalker.OrbwalkingMode.Combo)
            {
                Combo();
            }
            if (Orbwalk.ActiveMode == Orbwalker.OrbwalkingMode.Harass)
            {
                if (Player.ManaPercent >= HarMana)
                {
                    Harass();
                }
            }
            if (Orbwalk.ActiveMode == Orbwalker.OrbwalkingMode.JungleClear)
            {
                Jungleclear();
            }
            if (Orbwalk.ActiveMode == Orbwalker.OrbwalkingMode.LaneClear)
            {
                Laneclear();
            }
            if (SmartKS)
            {
                Smartks();
            }
            if (Orbwalk.ActiveMode == Orbwalker.OrbwalkingMode.Flee)
            {
                Flee();
            }
            if (TowerE)
            {
                DoTowerE();
            }
        }

        private void DoTowerE()
        {
            var allyturret = ObjectManager.Get<Obj_AI_Turret>().First(obj => obj.IsAlly && obj.Distance(Player) <= 775f);
            var minUnderTur = MinionManager.GetMinions(allyturret.ServerPosition, 775, MinionTypes.All, MinionTeam.Enemy);

            foreach (var target in ObjectManager.Get<AIHeroClient>().Where(target => target.IsValidTarget(E.Range)))
            {
                if (allyturret != null && minUnderTur == null && target.IsValidTarget())
                {
                    E.Cast(target);
                }
            }
        }

        private void Flee()
        {
            //Player.IssueOrder(GameObjectOrder.MoveTo, Game.CursorPosition);
            if (R.IsReady())
            {
                R.Cast(Game.CursorPosition);
            }
            var t = TargetSelector.GetTarget(Q.Range, TargetSelector.DamageType.Magical);
            if (E.IsReady() && t.IsValidTarget(E.Range))
            {
                E.Cast(t);
            }
        }

        private void Smartks()
        {
            foreach (var t in ObjectManager.Get<AIHeroClient>().Where(t => t.IsEnemy).Where(t => t.IsValidTarget(Q.Range)))
            {
                if (t.Health <= Q.GetDamage(t) && Q.IsReady())
                {
                    Q.Cast(t);
                }
                else if (t.Health <= (Q.GetDamage(t) + E.GetDamage(t)) && Q.IsReady() && E.IsReady())
                {
                    E.Cast(t);
                    Q.Cast(t);
                }
                else if (t.Health <= (E.GetDamage(t)) && E.IsReady())
                {
                    E.Cast(t);
                }
                if (IgniteSlot.IsReady() && IgniteSlot != SpellSlot.Unknown && t.Distance(Player) <= 600 && t.Health <= Player.GetSummonerSpellDamage(t, Damage.SummonerSpell.Ignite))
                {
                    Player.Spellbook.CastSpell(IgniteSlot, t);
                }
            }
        }

        private void Laneclear()
        {
            var minion = MinionManager.GetMinions(Player.ServerPosition, Q.Range);

            if (minion.Count < 3)
                return;
            if (UseQLane && Q.IsReady())
            {
                Q.Cast(minion[0].ServerPosition);
            }
            if (UseWLane && W.IsReady())
            {
                W.Cast();
            }
        }

        private void Jungleclear()
        {
            var junglemonster = MinionManager.GetMinions(Player.ServerPosition, Q.Range, MinionTypes.All, MinionTeam.Neutral, MinionOrderTypes.MaxHealth);
            if (junglemonster.Count == 0) return;

            if (UseQJungle && Q.IsReady())
            {
                Q.Cast(junglemonster[0].ServerPosition);
            }
            if (UseWJungle && W.IsReady())
            {
                W.Cast();
            }
        }

        private void Harass()
        {
            var t = TargetSelector.GetTarget(E.Range, TargetSelector.DamageType.Magical);

            if (Player.Distance(t) <= E.Range && E.IsReady() && E.MinHitChance >= HitChance.Medium)
            {
                E.Cast(t);
            }

            if (Player.Distance(t) <= Q.Range && Q.IsReady() && Q.MinHitChance >= HitChance.Medium)
            {
                Q.Cast(t);
            }

            if (Player.Distance(t) <= W.Range && W.IsReady())
            {
                W.Cast();
            }
        }

        private void Combo()
        {
            var t = TargetSelector.GetTarget(E.Range, TargetSelector.DamageType.Magical);
            
            if (t.IsValidTarget() && t != null)
            {
                if (RCombo)//REQW
                {
                    if (!RKill)
                    {
                        if (UseR == 0)
                        {
                            R.Cast(Game.CursorPosition);
                        }

                        if (UseR == 1)

                        {
                            R.Cast(t.ServerPosition);
                        }
                    }

                    if (RKill && (((R.GetDamage(t) * 3) + Q.GetDamage(t) + W.GetDamage(t) + E.GetDamage(t)) >= t.Health) && Q.IsReady() && W.IsReady() && E.IsReady())
                    {
                        if (UseR == 0)
                        {
                            R.Cast(Game.CursorPosition);
                        }

                        if (UseR == 1)
                        {
                            R.Cast(t.ServerPosition);
                        }
                    }

                    if (Player.Distance(t) <= E.Range && E.IsReady() && E.MinHitChance >= HitChance.Medium)
                    {
                        E.Cast(t);
                    }

                    if (Player.Distance(t) <= Q.Range && Q.IsReady() && Q.MinHitChance >= HitChance.Medium)
                    {
                        Q.Cast(t);
                    }

                    if (Player.Distance(t) <= W.Range && W.IsReady())
                    {
                        W.Cast();
                    }

                }
                else
                {
                    if (Player.Distance(t) <= E.Range && E.IsReady() && E.MinHitChance >= HitChance.Medium)
                    {
                        E.Cast(t);
                    }

                    if (Player.Distance(t) <= Q.Range && Q.IsReady() && Q.MinHitChance >= HitChance.Medium)
                    {
                        Q.Cast(t);
                    }

                    if (Player.Distance(t) <= W.Range && W.IsReady())
                    {
                        W.Cast();
                    }
                }
            }
        }
    }
}