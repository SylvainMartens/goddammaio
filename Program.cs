﻿using HesaEngine.SDK;
using System;

namespace GodDammAIO
{
    class Program : IScript
    {
        public string Name => "God Damm AIO";
        public string Version => "1.0.0";
        public string Author => "Hesa";

        static void Main(string[] args)
        {
            //Game.OnGameLoaded += Game_OnGameLoaded;
        }

        private static void Game_OnGameLoaded()
        {
            var bootTime = new Random().Next(3, 5);
            Chat.Print(string.Format("<b>God Damm AIO</b> loaded, initializing in {0} seconds.", bootTime));
            Core.DelayAction(() => { new AIO(); }, bootTime * 1000);
        }

        public void OnInitialize()
        {
            Game.OnGameLoaded += Game_OnGameLoaded;
        }
    }
}