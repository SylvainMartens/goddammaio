﻿using HesaEngine.SDK;
using HesaEngine.SDK.GameObjects;
using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GodDammAIO.Awareness.Trackers
{
    public class TowersTracker
    {
        static List<Obj_AI_Turret> TurretsAttackingMe = new List<Obj_AI_Turret>();

        internal static bool Enabled = true;
        bool ShowFriendlyTower = true;
        bool ShowEnemyTower = true;

        public TowersTracker()
        {
            var towersMenu = Awareness.Menu.AddSubMenu("Tower Range");

            towersMenu.Add(new MenuCheckbox("enabled", "Enabled", true)).OnValueChanged += (sender, args) => { Enabled = args; };
            towersMenu.Add(new MenuCheckbox("ShowFriendlyTower", "Show Friendly Towers Range", true)).OnValueChanged += (sender, args) => { ShowFriendlyTower = args; };
            towersMenu.Add(new MenuCheckbox("ShowEnemyTower", "Show Enemy Towers Range", true)).OnValueChanged += (sender, args) => { ShowEnemyTower = args; };
            Drawing.OnDraw += Drawing_OnDraw;
        }

        private void Obj_AI_Turret_OnProcessSpellCast(Obj_AI_Base sender, HesaEngine.SDK.Args.GameObjectProcessSpellCastEventArgs args)
        {
            if (sender == null || args == null || args.Target == null) return;
            try
            {
                if (sender is Obj_AI_Turret tower)
                {
                    if (tower != null && args.Target.IsMe && !TowerAttackingMe(tower))
                    {
                        lock (TurretsAttackingMe)
                        {
                            TurretsAttackingMe.Add(tower);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex, ConsoleColor.Red);
            }
        }

        private bool TowerAttackingMe(Obj_AI_Turret tower)
        {
            return TurretsAttackingMe.Count(x => x.NetworkId == tower.NetworkId) >= 1;
        }

        private void SetTowerNotAttackingMe(Obj_AI_Turret tower)
        {
            try
            {
                lock (TurretsAttackingMe)
                {
                    var existing = TurretsAttackingMe.FirstOrDefault(x => x.NetworkId == tower.NetworkId);
                    if (existing != null)
                        TurretsAttackingMe.Remove(existing);
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex, ConsoleColor.Red);
            }
        }

        private int GetAlliesUnitInTowerRange(Obj_AI_Turret tower, float range)
        {
            try
            {
                return ObjectManager.Get<Obj_AI_Base>().Count(x => x.IsAlly && !x.IsMe && x.IsValidTarget(range, false, tower.ServerPosition));
            }
            catch (Exception ex)
            {
                Logger.Log(ex, ConsoleColor.Red);
            }
            return 0;
        }

        internal void Drawing_OnDraw(EventArgs args)
        {
            if (!Enabled) return;
            try
            {
                if (ShowEnemyTower)
                {
                    foreach (var turret in ObjectManager.Turrets.Enemy.Where(x => !x.IsDead && (x.IsVisibleOnScreen || x.Distance(ObjectManager.Me) < 2000)))
                    {
                        if (turret.Distance(ObjectManager.Me) <= turret.Range)
                        {
                            if (TowerAttackingMe(turret) || GetAlliesUnitInTowerRange(turret, turret.Range) <= 1)
                                Drawing.DrawCircle(turret.IsFountain ? turret.Position.To2D().To3D3() : turret.Position, turret.Range, Color.Red);
                            else
                                Drawing.DrawCircle(turret.IsFountain ? turret.Position.To2D().To3D3() : turret.Position, turret.Range, Color.Yellow);
                        }
                        else
                        {
                            Drawing.DrawCircle(turret.IsFountain ? turret.Position.To2D().To3D3() : turret.Position, turret.Range, Color.Lime);
                            SetTowerNotAttackingMe(turret);
                        }
                    }
                }

                if (ShowFriendlyTower)
                {
                    foreach (var turret in ObjectManager.Turrets.Ally.Where(x => !x.IsDead && (x.IsVisibleOnScreen || x.Distance(ObjectManager.Me) < 2000)))
                            Drawing.DrawCircle(turret.IsFountain ? turret.Position.To2D().To3D3() : turret.Position, turret.Range, Color.Lime);
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex, ConsoleColor.Red);
            }
        }
    }
}