﻿using GodDammAIO.Champions;
using GodDammAIO.Menus;
using HesaEngine.SDK;
using HesaEngine.SDK.Enums;
using static GodDammAIO.Configs;

namespace GodDammAIO
{
    internal class AIO
    {
        Activators.Activator activator;
        Awareness.Awareness awareness;
        Evade.Core.Evade evade;
        ChampionScript ChampionScript;

        public AIO()
        {
            MainMenu = Menu.AddMenu("God Damm AIO");

            HealSlot = Player.GetSpellSlot("SummonerHeal");
            FlashSlot = Player.GetSpellSlot("SummonerFlash");
            BarrierSlot = Player.GetSpellSlot("SummonerBarrier");
            IgniteSlot = Player.GetSpellSlot("SummonerDot");
            ExhaustSlot = Player.GetSpellSlot("SummonerExhaust");
            SmiteSlot = Player.GetSpellSlot("SummonerSmite");
            
            activator = new Activators.Activator(MainMenu.AddSubMenu("Activators"));
            awareness = new Awareness.Awareness(MainMenu.AddSubMenu("Awareness"));
            evade = new Evade.Core.Evade(MainMenu.AddSubMenu("Evade"));

            switch (Player.Hero)
            {
                case Champion.Aatrox: ChampionScript = new Aatrox(); break;
                case Champion.Ahri: ChampionScript = new Ahri(); break;
                case Champion.Akali: ChampionScript = new Akali(); break;
                case Champion.Alistar: ChampionScript = new Alistar(); break;
                case Champion.Amumu: ChampionScript = new Amumu(); break;

                case Champion.Ashe: ChampionScript = new Ashe(); break;

                case Champion.Diana: ChampionScript = new Diana(); break;
                
                case Champion.Kalista: ChampionScript = new Kalista(); break;
                case Champion.Katarina: ChampionScript = new Katarina(); break;
                case Champion.Vayne: ChampionScript = new Vayne(); break;
            }

            if(ChampionMenu != null)
            {
                MyManaManager.AddSpellFarm(ChampionMenu);
            }
        }
    }
}