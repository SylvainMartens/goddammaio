﻿using HesaEngine.SDK;
using SharpDX.DirectInput;
using System.Collections.Generic;
using static GodDammAIO.Configs;

namespace GodDammAIO.Menus
{
    internal static class MiscOption
    {
        static MiscOption()
        {
            MiscMenu = ChampionMenu.AddSubMenu("Misc");
        }

        internal static Dictionary<string, bool> CachedBoolValues = new Dictionary<string, bool>();
        internal static Dictionary<string, int> CachedIntValues = new Dictionary<string, int>();

        internal static void CacheBoolValue(MenuCheckbox checkbox, bool addEvent = true)
        {
            if (checkbox == null) return;
            if (!CachedBoolValues.ContainsKey(checkbox.Name))
                CachedBoolValues.Add(checkbox.Name, checkbox.Checked);
            else
                CachedBoolValues[checkbox.Name] = checkbox.Checked;
            if (addEvent)
                checkbox.OnValueChanged += (sender, value) => { CacheBoolValue(sender, false); };
        }

        internal static void CacheBoolValue(MenuKeybind keybind, bool addEvent = true)
        {
            if (keybind == null) return;
            if (!CachedBoolValues.ContainsKey(keybind.Name))
                CachedBoolValues.Add(keybind.Name, keybind.Active);
            else
                CachedBoolValues[keybind.Name] = keybind.Active;
            if (addEvent)
                keybind.OnValueChanged += (sender, value) => { CacheBoolValue(sender, false); };
        }

        internal static void CacheIntValue(MenuSlider slider, bool addEvent = true)
        {
            if (slider == null) return;
            if (!CachedIntValues.ContainsKey(slider.Name))
                CachedIntValues.Add(slider.Name, slider.CurrentValue);
            else
                CachedIntValues[slider.Name] = slider.CurrentValue;
            if (addEvent)
                slider.OnValueChanged += (sender, value) => { CacheIntValue(sender, false); };
        }

        internal static void CacheIntValue(MenuCombo combo, bool addEvent = true)
        {
            if (combo == null) return;
            if (!CachedIntValues.ContainsKey(combo.Name))
                CachedIntValues.Add(combo.Name, combo.CurrentValue);
            else
                CachedIntValues[combo.Name] = combo.CurrentValue;
            if (addEvent)
                combo.OnValueChanged += (sender, value) => { CacheIntValue(sender, false); };
        }

        internal static void AddSetting(string name)
        {
            MiscMenu.AddSeparator(name + " Settings");
        }

        internal static void AddQ()
        {
            MiscMenu.AddSeparator("Q Settings");
        }

        internal static void AddW()
        {
            MiscMenu.AddSeparator("W Settings");
        }

        internal static void AddE()
        {
            MiscMenu.AddSeparator("E Settings");
        }

        internal static void AddR()
        {
            MiscMenu.AddSeparator("R Settings");
        }

        internal static void AddGapcloser()
        {
            MiscMenu.AddSeparator("Anti Gapcloser Settings");
        }

        internal static void AddInterrupt()
        {
            MiscMenu.AddSeparator("Interrupt Spell Settings");
        }

        internal static void AddMelee()
        {
            MiscMenu.AddSeparator("Anti Melee Settings");
        }

        internal static void AddGapcloserTargetList()
        {
            MiscMenu.AddSeparator("AntiGapcloser List Settings");
            foreach (var target in ObjectManager.Heroes.Enemies)
            {
                if (target != null)
                {
                    AddBool("AntiGapcloserList" + target.ChampionName.ToLower(), target.ChampionName, true);
                }
            }
        }

        internal static bool GetGapcloserTarget(string name)
        {
            return GetBool("AntiGapcloserList" + name.ToLower());
        }


        internal static void AddTargetList()
        {
            MiscMenu.AddSeparator("Misc Target Settings");
            foreach (var target in ObjectManager.Heroes.Enemies)
            {
                if (target != null)
                {
                    AddBool("MiscList" + target.ChampionName.ToLower(), target.ChampionName, true);
                }
            }
        }

        internal static bool GetMiscTarget(string name)
        {
            return GetBool("MiscList" + name.ToLower());
        }
        
        ///
        internal static void AddBool(string name, string text, bool enabled = true)
        {
            CacheBoolValue(MiscMenu.Add(new MenuCheckbox(name, text, enabled)));
        }

        internal static void AddSlider(string name, string defaultName, int defaultValue, int minValue = 0, int maxValue = 100)
        {
            CacheIntValue(MiscMenu.Add(new MenuSlider(name, defaultName, new Slider(defaultValue, minValue, maxValue))));
        }

        internal static void AddKey(string name, string text, Key key, MenuKeybindType type = MenuKeybindType.Hold, bool enabled = false)
        {
            CacheBoolValue(MiscMenu.Add(new MenuKeybind(name, text, new KeyBind(key, type, enabled))));
        }

        internal static void AddList(string name, string defaultName, string[] values, int defaultValue = 0)
        {
            CacheIntValue(MiscMenu.Add(new MenuCombo(name, defaultName, new StringList(values, defaultValue))));
        }

        internal static bool GetBool(string name)
        {
            if (CachedBoolValues.ContainsKey(name)) return CachedBoolValues[name];
            return false;
        }

        internal static int GetSlider(string name)
        {
            if (CachedIntValues.ContainsKey(name)) return CachedIntValues[name];
            return int.MaxValue;
        }

        internal static bool GetKey(string name)
        {
            if (CachedBoolValues.ContainsKey(name)) return CachedBoolValues[name];
            return false;
        }

        internal static int GetList(string name)
        {
            if (CachedIntValues.ContainsKey(name)) return CachedIntValues[name];
            return int.MaxValue;
        }

        internal static bool UseQ => GetBool("MiscQ");
        internal static bool UseW => GetBool("MiscW");
        internal static bool UseE => GetBool("MiscE");
        internal static bool UseR => GetBool("MiscR");
    }
}