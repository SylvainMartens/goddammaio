﻿using HesaEngine.SDK;
using HesaEngine.SDK.GameObjects;
using System.Linq;
using static GodDammAIO.Configs;

namespace GodDammAIO.Activators
{
    internal class SummonerHealActivator
    {
        Menu HealMenu;
        Menu AlliesMenu;

        bool Heal = true;
        bool Execute = true;
        bool HealAllies = true;

        Spell HealSpell;

        public SummonerHealActivator(Menu menu)
        {
            if (HealSlot == HesaEngine.SDK.Enums.SpellSlot.Unknown) return;
            HealMenu = menu.AddSubMenu("Heal");
            HealMenu.Add(new MenuCheckbox("Heal", "Use Heal On Self", Heal)).OnValueChanged += (sender, value) => { Heal = value; };
            HealMenu.Add(new MenuCheckbox("Execute", "Use Heal Before Dying", Execute)).OnValueChanged += (sender, value) => { Execute = value; };

            AlliesMenu = HealMenu.AddSubMenu("Allies");
            foreach (var ally in ObjectManager.Heroes.Allies)
            {
                AlliesMenu.Add(new MenuCheckbox("Heal" + ally.ChampionName.ToLower(), "Heal " + ally.ChampionName, TargetSelector.GetPriority(ally) > 1));
                AlliesMenu.Add(new MenuSlider("hp" + ally.ChampionName.ToLower(), "Heal " + ally.ChampionName + " Under % HP", 1, 100, (int)(TargetSelector.GetPriority(ally) * 10)));
            }
            HealMenu.Add(new MenuCheckbox("HealAllies", "Use Heal On Allies", HealAllies)).OnValueChanged += (sender, value) => { HealAllies = value; };

            HealSpell = new Spell(HealSlot, 800);

            AttackableUnit.OnDamage += AttackableUnit_OnDamage;
        }

        private void AttackableUnit_OnDamage(AttackableUnit sender, HesaEngine.SDK.Args.AttackableUnitDamageEventArgs args)
        {
            if (args.Target is AIHeroClient target && target != null && target.IsAlly)
            {
                if (!HealSpell.IsReady() /*|| !target.IsKillable(800)*/ || !ObjectManager.Heroes.Enemies.Any(e => e.IsValid() && !e.IsDead && e.IsInRange(target, 1250)) && target.Health > args.Damage)
                    return;

                if (!target.IsMe && !AlliesMenu.Get<MenuCheckbox>("Heal" + target.ChampionName.ToLower()).Checked)
                    return;

                if (Execute)
                {
                    if (target.IsMe || HealAllies)
                    {
                        if (args.Damage >= target.Health && target.IsInRange(Player, 800))
                            Player.Spellbook.CastSpell(HealSlot);
                    }
                    return;
                }

                if (((HealAllies && !target.IsMe && target.IsAlly) || Heal && target.IsMe) && (AlliesMenu.Get<MenuSlider>("hp" + target.ChampionName.ToLower()).CurrentValue >= target.HealthPercent || args.Damage >= target.TotalShieldHealth))
                {
                    Player.Spellbook.CastSpell(HealSlot);
                }
            }
        }
    }
}