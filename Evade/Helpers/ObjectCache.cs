﻿using System.Collections.Generic;
using SharpDX;
using HesaEngine.SDK.GameObjects;
using HesaEngine.SDK;
using GodDammAIO.Evade.Utils;
using System;

namespace GodDammAIO.Evade.Helpers
{
    public class HeroInfo
    {
        public AIHeroClient hero;
        public Vector2 serverPos2D;
        public Vector2 serverPos2DExtra;
        public Vector2 serverPos2DPing;
        public Vector2 currentPosition;
        public bool isMoving;
        public float boundingRadius;
        public float moveSpeed;

        public HeroInfo(AIHeroClient hero)
        {
            this.hero = hero;
            Game.OnUpdate += Game_OnGameUpdate;
        }

        private void Game_OnGameUpdate()
        {
            UpdateInfo();
        }

        public void UpdateInfo()
        {
            try
            {
                var extraDelayBuffer = ObjectCache.menuCache.cache["ExtraPingBuffer"].GetValue<int>();

                serverPos2D = hero.ServerPosition.To2D(); //CalculatedPosition.GetPosition(hero, Game.Ping);
                serverPos2DExtra = EvadeUtils.GetGamePosition(hero, Game.Ping + extraDelayBuffer);
                serverPos2DPing = EvadeUtils.GetGamePosition(hero, Game.Ping);
                //CalculatedPosition.GetPosition(hero, Game.Ping + extraDelayBuffer);            
                currentPosition = hero.Position.To2D(); //CalculatedPosition.GetPosition(hero, 0); 
                boundingRadius = hero.BoundingRadius;
                moveSpeed = hero.MovementSpeed;
                isMoving = hero.IsMoving;
            }
            catch (Exception) { }
        }
    }

    public class MenuCache
    {
        public Menu menu;
        public Dictionary<string, MenuElement> cache = new Dictionary<string, MenuElement>();

        public MenuCache(Menu menu)
        {
            this.menu = menu;
            AddMenuToCache(menu);
        }

        public void AddMenuToCache(Menu newMenu)
        {
            lock (cache)
            {
                foreach (var item in ReturnAllItems(newMenu))
                {
                    AddMenuItemToCache(item);
                }
            }   
        }

        public void AddMenuItemToCache(MenuElement item)
        {
            if (item != null && item.Name != null && !cache.ContainsKey(item.Name))
            {
                cache.Add(item.Name, item);
            }
        }

        public static List<MenuElement> ReturnAllItems(Menu menu)
        {
            List<MenuElement> menuList = new List<MenuElement>();

            menuList.AddRange(menu.Elements.Values);

            foreach (var submenu in menu.Elements)
            {
                if(submenu.Value is Menu subMenuElement)
                    menuList.AddRange(ReturnAllItems(subMenuElement));
            }

            return menuList;
        }
    }

    public static class ObjectCache
    {
        public static Dictionary<int, Obj_AI_Turret> turrets = new Dictionary<int, Obj_AI_Turret>();

        private static AIHeroClient myHero { get { return ObjectManager.Player; } }

        public static HeroInfo myHeroCache = new HeroInfo(myHero);
        public static MenuCache menuCache = new MenuCache(Core.Evade.Menu_);

        public static float gamePing = 0;

        static ObjectCache()
        {
            InitializeCache();
            Game.OnUpdate += Game_OnGameUpdate;
        }

        private static void Game_OnGameUpdate()
        {
            gamePing = Game.Ping;
        }

        private static void InitializeCache()
        {
            foreach (var obj in ObjectManager.Get<Obj_AI_Turret>())
            {
                if (!turrets.ContainsKey(obj.NetworkId))
                {
                    turrets.Add(obj.NetworkId, obj);
                }
            }
        }
    }
}