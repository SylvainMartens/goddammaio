﻿using System;
using System.Collections.Generic;
using SharpDX;
using HesaEngine.SDK.GameObjects;
using HesaEngine.SDK;
using GodDammAIO.Evade.Helpers;
using GodDammAIO.Evade.Core;

namespace GodDammAIO.Evade.Spells
{
    internal class SpellDrawer
    {
        public static Menu menu;

        private static AIHeroClient myHero { get { return ObjectManager.Player; } }
        
        public SpellDrawer(Menu mainMenu)
        {
            menu = mainMenu;
            Game_OnGameLoad();
            Drawing.OnDraw += Drawing_OnDraw;
        }

        private void Game_OnGameLoad()
        {
            //Console.WriteLine("SpellDrawer loaded");

            Menu drawMenu = menu.AddSubMenu("Draw");
            drawMenu.Add(new MenuCheckbox("DrawSkillShots", "Draw SkillShots").SetValue(true));
            drawMenu.Add(new MenuCheckbox("ShowStatus", "Show Evade Status").SetValue(true));
            drawMenu.Add(new MenuCheckbox("DrawSpellPos", "Draw Spell Position").SetValue(true));
            drawMenu.Add(new MenuCheckbox("DrawEvadePosition", "Draw Evade Position").SetValue(false));

            Menu dangerMenu = drawMenu.AddSubMenu("DangerLevel Drawings");
            
            Menu lowDangerMenu = dangerMenu.AddSubMenu("Low");
            lowDangerMenu.Add(new MenuSlider("LowWidth", "Line Width").SetValue(new Slider(1, 15, 3)));
            lowDangerMenu.Add(new MenuColor("LowColor", "Color").SetValue(new Circle(true, new ColorBGRA(255, 60, 255, 255))));

            Menu normalDangerMenu = dangerMenu.AddSubMenu("Normal");
            normalDangerMenu.Add(new MenuSlider("NormalWidth", "Line Width").SetValue(new Slider(1, 15, 3)));
            normalDangerMenu.Add(new MenuColor("NormalColor", "Color").SetValue(new Circle(true, new ColorBGRA(255, 140, 255, 255))));

            Menu highDangerMenu = dangerMenu.AddSubMenu("High");
            highDangerMenu.Add(new MenuSlider("HighWidth", "Line Width").SetValue(new Slider(1, 15, 3)));
            highDangerMenu.Add(new MenuColor("HighColor", "Color").SetValue(new Circle(true, new ColorBGRA(255, 255, 255, 255))));

            Menu extremeDangerMenu = dangerMenu.AddSubMenu("Extreme");
            extremeDangerMenu.Add(new MenuSlider("ExtremeWidth", "Line Width").SetValue(new Slider(1, 15, 4)));
            extremeDangerMenu.Add(new MenuColor("ExtremeColor", "Color").SetValue(new Circle(true, new ColorBGRA(255, 255, 255, 255))));

            /*
            Menu undodgeableDangerMenu = new Menu("Undodgeable", "Undodgeable");
            undodgeableDangerMenu.AddItem(new MenuItem("Width", "Line Width").SetValue(new Slider(6, 1, 15)));
            undodgeableDangerMenu.AddItem(new MenuItem("Color", "Color").SetValue(new Circle(true, Color.FromArgb(255, 255, 0, 0))));*/
        }

        private void DrawLineRectangle(Vector2 start, Vector2 end, int radius, int width, ColorBGRA color)
        {
            var dir = (end - start).Normalized();
            var pDir = dir.Perpendicular();

            var rightStartPos = start + pDir * radius;
            var leftStartPos = start - pDir * radius;
            var rightEndPos = end + pDir * radius;
            var leftEndPos = end - pDir * radius;

            var rStartPos = rightStartPos.To3D();// new Vector3(rightStartPos.X, rightStartPos.Y, myHero.Position.Z);
            var lStartPos = leftStartPos.To3D();// new Vector3(leftStartPos.X, leftStartPos.Y, myHero.Position.Z);
            var rEndPos = rightEndPos.To3D();// new Vector3(rightEndPos.X, rightEndPos.Y, myHero.Position.Z);
            var lEndPos = leftEndPos.To3D();// new Vector3(leftEndPos.X, leftEndPos.Y, myHero.Position.Z);

            Drawing.DrawLine(rStartPos, rEndPos, width, color);
            Drawing.DrawLine(lStartPos, lEndPos, width, color);
            Drawing.DrawLine(rStartPos, lStartPos, width, color);
            Drawing.DrawLine(lEndPos, rEndPos, width, color);
        }

        private void DrawLineTriangle(Vector2 start, Vector2 end, int radius, int width, ColorBGRA color)
        {
            var dir = (end - start).Normalized();
            var pDir = dir.Perpendicular();

            var initStartPos = start + dir;
            var rightEndPos = end + pDir * radius;
            var leftEndPos = end - pDir * radius;

            var iStartPos = initStartPos.To3D();// new Vector3(initStartPos.X, initStartPos.Y, myHero.Position.Z);
            var rEndPos = rightEndPos.To3D();// new Vector3(rightEndPos.X, rightEndPos.Y, myHero.Position.Z);
            var lEndPos = leftEndPos.To3D();// new Vector3(leftEndPos.X, leftEndPos.Y, myHero.Position.Z);

            Drawing.DrawLine(iStartPos, rEndPos, width, color);
            Drawing.DrawLine(iStartPos, lEndPos, width, color);
            Drawing.DrawLine(rEndPos, lEndPos, width, color);
        }

        private void DrawEvadeStatus()
        {
            if (ObjectCache.menuCache.cache["ShowStatus"].GetValue<bool>())
            {
                var heroPos = Drawing.WorldToScreen(ObjectManager.Player.Position);
                var dimension = Drawing.GetTextExtent("Evade: ON");

                if (EvadeConfigs.DodgeSkillShots)
                {
                    if (Core.Evade.isDodging)
                    {
                        Drawing.DrawText(heroPos.X - dimension.Width / 2, heroPos.Y, Color.Red, "Evade: ON");
                    }
                    else
                    {
                        if (ObjectCache.menuCache.cache["DodgeOnlyOnComboKeyEnabled"].GetValue<bool>() == true
                         && ObjectCache.menuCache.cache["DodgeComboKey"].GetValue<bool>() == false)
                        {
                            Drawing.DrawText(heroPos.X - dimension.Width / 2, heroPos.Y, Color.Gray, "Evade: OFF");
                        }
                        else
                        {
                            if (ObjectCache.menuCache.cache["DontDodgeKeyEnabled"].GetValue<bool>() == true
                         && ObjectCache.menuCache.cache["DontDodgeKey"].GetValue<bool>() == true)
                                Drawing.DrawText(heroPos.X - dimension.Width / 2, heroPos.Y, Color.Gray, "Evade: OFF");
                            else if (Core.Evade.isDodgeDangerousEnabled())
                                Drawing.DrawText(heroPos.X - dimension.Width / 2, heroPos.Y, Color.Yellow, "Evade: ON");
                            else
                                Drawing.DrawText(heroPos.X - dimension.Width / 2, heroPos.Y, Color.Lime, "Evade: ON");
                        }
                    }
                }
                else
                {
                    if (EvadeConfigs.ActivateEvadeSpells)
                    {
                        if (ObjectCache.menuCache.cache["DodgeOnlyOnComboKeyEnabled"].GetValue<bool>() == true
                         && ObjectCache.menuCache.cache["DodgeComboKey"].GetValue<bool>() == false)
                        {
                            Drawing.DrawText(heroPos.X - dimension.Width / 2, heroPos.Y, Color.Gray, "Evade: OFF");
                        }
                        else
                        {
                            if (Core.Evade.isDodgeDangerousEnabled())
                                Drawing.DrawText(heroPos.X - dimension.Width / 2, heroPos.Y, Color.Yellow, "Evade: Spell");
                            else
                                Drawing.DrawText(heroPos.X - dimension.Width / 2, heroPos.Y, Color.DeepSkyBlue, "Evade: Spell");
                        }
                    }
                    else
                    {
                        Drawing.DrawText(heroPos.X - dimension.Width / 2, heroPos.Y, Color.Gray, "Evade: OFF");
                    }
                }
            }
        }

        private void Drawing_OnDraw(EventArgs args)
        {
            try
            {
                if (ObjectCache.menuCache.cache["DrawEvadePosition"].GetValue<bool>())
                {
                    //Render.Circle.DrawCircle(myHero.Position.ExtendDir(dir, 500), 65, Color.Red, 10);

                    /*foreach (var point in myHero.Path)
                    {
                        Render.Circle.DrawCircle(point, 65, Color.Red, 10);
                    }*/

                    if (Core.Evade.lastPosInfo != null)
                    {
                        var pos = Core.Evade.lastPosInfo.position; //Evade.lastEvadeCommand.targetPosition;
                        Drawing.DrawCircle(pos.To3D(), 65, Color.Red, 10);
                    }
                }

                DrawEvadeStatus();

                if (ObjectCache.menuCache.cache["DrawSkillShots"].GetValue<bool>() == false)
                {
                    return;
                }

                foreach (KeyValuePair<int, Spell> entry in SpellDetector.drawSpells)
                {
                    Spell spell = entry.Value;

                    var dangerStr = spell.GetSpellDangerString();
                    var spellDrawingConfig = ObjectCache.menuCache.cache[dangerStr + "Color"].GetValue<Circle>();
                    var spellDrawingWidth = ObjectCache.menuCache.cache[dangerStr + "Width"].GetValue<int>();
                    var avoidRadius = ObjectCache.menuCache.cache["ExtraAvoidDistance"].GetValue<int>();

                    if (ObjectCache.menuCache.cache[spell.info.spellName + "DrawSpell"].GetValue<bool>() && spellDrawingConfig.Active)
                    {

                        bool canEvade = !(Core.Evade.lastPosInfo != null && Core.Evade.lastPosInfo.undodgeableSpells.Contains(spell.spellID)) || !Core.Evade.devModeOn;

                        if (spell.spellType == SpellType.Line)
                        {
                            Vector2 spellPos = spell.currentSpellPosition;
                            Vector2 spellEndPos = spell.GetSpellEndPosition();

                            DrawLineRectangle(spellPos, spellEndPos, (int)spell.radius,
                                spellDrawingWidth, !canEvade ? new ColorBGRA(Color.Yellow.ToBgra()) : spellDrawingConfig.Color);

                            if (ObjectCache.menuCache.cache["DrawSpellPos"].GetValue<bool>())// && spell.spellObject != null)
                            {
                                Drawing.DrawCircle(new Vector3(spellPos.X, ObjectManager.Player.Position.Y, spellPos.Y), (int)spell.radius, !canEvade ? new ColorBGRA(Color.Yellow.ToBgra()) : spellDrawingConfig.Color, spellDrawingWidth);
                            }
                        }
                        else if (spell.spellType == SpellType.Circular)
                        {
                            Drawing.DrawCircle(new Vector3(spell.endPos.X, ObjectManager.Player.Position.Y, spell.endPos.Y), (int)spell.radius, !canEvade ? new ColorBGRA(Color.Yellow.ToBgra()) : spellDrawingConfig.Color, spellDrawingWidth);

                            if (spell.info.spellName == "VeigarEventHorizon")
                            {
                                Drawing.DrawCircle(new Vector3(spell.endPos.X, ObjectManager.Player.Position.Y, spell.endPos.Y), (int)spell.radius - 125, !canEvade ? new ColorBGRA(Color.Yellow.ToBgra()) : spellDrawingConfig.Color, spellDrawingWidth);
                            }
                            else if (spell.info.spellName == "DariusCleave")
                            {
                                Drawing.DrawCircle(new Vector3(spell.endPos.X, ObjectManager.Player.Position.Y, spell.endPos.Y), (int)spell.radius - 220, !canEvade ? new ColorBGRA(Color.Yellow.ToBgra()) : spellDrawingConfig.Color, spellDrawingWidth);
                            }
                        }
                        else if (spell.spellType == SpellType.Arc)
                        {
                            /*var spellRange = spell.startPos.Distance(spell.endPos);
                            var midPoint = spell.startPos + spell.direction * (spellRange / 2);
                            Render.Circle.DrawCircle(new Vector3(midPoint.X, midPoint.Y, myHero.Position.Z), (int)spell.radius, spellDrawingConfig.Color, spellDrawingWidth);

                            Drawing.DrawLine(Drawing.WorldToScreen(spell.startPos.To3D()),
                                             Drawing.WorldToScreen(spell.endPos.To3D()), 
                                             spellDrawingWidth, spellDrawingConfig.Color);*/
                        }
                        else if (spell.spellType == SpellType.Cone)
                        {
                            DrawLineTriangle(spell.startPos, spell.endPos, (int)spell.radius, spellDrawingWidth, !canEvade ? new ColorBGRA(Color.Yellow.ToBgra()) : spellDrawingConfig.Color);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}