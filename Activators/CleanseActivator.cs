﻿using HesaEngine.SDK;
using HesaEngine.SDK.Enums;
using HesaEngine.SDK.GameObjects;
using System.Collections.Generic;
using static GodDammAIO.Configs;

namespace GodDammAIO.Activators
{
    internal class CleanseActivator
    {
        private int useCleanTime = 0;
        private Menu cleanMenu;

        private bool CleanEnable = true;
        private bool CleanOnlyKey = true;
        private bool Cleanblind = true;
        private bool Cleancharm = true;
        private bool Cleanfear = true;
        private bool Cleanflee = true;
        private bool Cleanstun = true;
        private bool Cleansnare = true;
        private bool Cleantaunt = true;
        private bool Cleansuppression = false;
        private bool Cleanpolymorph = false;
        private bool Cleansilence = true;
        private bool Cleanexhaust = true;
        private int CleanDelay = 100;
        private int CleanBuffTime = 800;

        public CleanseActivator(Menu mainMenu)
        {
            cleanMenu = mainMenu.AddSubMenu("Cleanse");
            cleanMenu.Add(new MenuCheckbox("CleanEnable", "Enabled", CleanEnable)).OnValueChanged += (MenuCheckbox checkbox, bool newValue) => { CleanEnable = newValue; };

            var Debuff = cleanMenu.AddSubMenu("Debuffs");
            {
                Debuff.Add(new MenuCheckbox("Cleanblind", "Blind", Cleanblind)).OnValueChanged += (MenuCheckbox checkbox, bool newValue) => { Cleanblind = newValue; };
                Debuff.Add(new MenuCheckbox("Cleancharm", "Charm", Cleancharm)).OnValueChanged += (MenuCheckbox checkbox, bool newValue) => { Cleancharm = newValue; };
                Debuff.Add(new MenuCheckbox("Cleanfear", "Fear", Cleanfear)).OnValueChanged += (MenuCheckbox checkbox, bool newValue) => { Cleanfear = newValue; };
                Debuff.Add(new MenuCheckbox("Cleanflee", "Flee", Cleanflee)).OnValueChanged += (MenuCheckbox checkbox, bool newValue) => { Cleanflee = newValue; };
                Debuff.Add(new MenuCheckbox("Cleanstun", "Stun", Cleanstun)).OnValueChanged += (MenuCheckbox checkbox, bool newValue) => { Cleanstun = newValue; };
                Debuff.Add(new MenuCheckbox("Cleansnare", "Snare", Cleansnare)).OnValueChanged += (MenuCheckbox checkbox, bool newValue) => { Cleansnare = newValue; };
                Debuff.Add(new MenuCheckbox("Cleantaunt", "Taunt", Cleantaunt)).OnValueChanged += (MenuCheckbox checkbox, bool newValue) => { Cleantaunt = newValue; };
                Debuff.Add(new MenuCheckbox("Cleansuppression", "Suppression", Cleansuppression)).OnValueChanged += (MenuCheckbox checkbox, bool newValue) => { Cleansuppression = newValue; };
                Debuff.Add(new MenuCheckbox("Cleanpolymorph", "Polymorph", Cleanpolymorph)).OnValueChanged += (MenuCheckbox checkbox, bool newValue) => { Cleanpolymorph = newValue; };
                Debuff.Add(new MenuCheckbox("Cleansilence", "Silence", Cleansilence)).OnValueChanged += (MenuCheckbox checkbox, bool newValue) => { Cleansilence = newValue; };
                Debuff.Add(new MenuCheckbox("Cleanexhaust", "Cleanexhaust", true)).OnValueChanged += (MenuCheckbox checkbox, bool newValue) => { Cleanexhaust = newValue; };
            }
            
            cleanMenu.Add(new MenuSlider("CleanDelay", "Clean Delay(ms)", new Slider(0, 2000, CleanDelay))).OnValueChanged += (MenuSlider slider, int newValue) => { CleanDelay = newValue; };
            cleanMenu.Add(new MenuSlider("CleanBuffTime", "Debuff Less End Times(ms)", new Slider(0, 1000, CleanBuffTime))).OnValueChanged += (MenuSlider slider, int newValue) => { CleanBuffTime = newValue; };
            cleanMenu.Add(new MenuCheckbox("CleanOnlyKey", "Only Combo Mode Active?", CleanOnlyKey)).OnValueChanged += (MenuCheckbox checkbox, bool newValue) => { CleanOnlyKey = newValue; };

            Game.OnUpdate += OnUpdate;
        }

        private void OnUpdate()
        {
            if (Player.IsDead || !CleanEnable || (CleanOnlyKey && Orbwalk.ActiveMode != Orbwalker.OrbwalkingMode.Combo))
            {
                return;
            }
            if (Utils.TickCount > useCleanTime && CanClean(Player))
            {
                if (Item.HasItem(ItemId.Quicksilver_Sash, Player) && Item.CanUseItem(ItemId.Quicksilver_Sash))
                {
                    Item.UseItem(ItemId.Quicksilver_Sash, Player);
                    useCleanTime = Utils.TickCount + 3000;
                }
                else if (Item.HasItem(ItemId.Mercurial_Scimitar, Player) && Item.CanUseItem(ItemId.Mercurial_Scimitar))
                {
                    Item.UseItem(ItemId.Mercurial_Scimitar, Player);
                    useCleanTime = Utils.TickCount + 3000;
                }
                else if (Item.HasItem(ItemId.Mikaels_Crucible, Player) && Item.CanUseItem(ItemId.Mikaels_Crucible))
                {
                    Item.UseItem(ItemId.Mikaels_Crucible, Player);
                    useCleanTime = Utils.TickCount + 3000;
                }
                else if (Item.HasItem(ItemId.Dervish_Blade, Player) && Item.CanUseItem(ItemId.Dervish_Blade))
                {
                    Item.UseItem(ItemId.Dervish_Blade, Player);
                    useCleanTime = Utils.TickCount + 3000;
                }
            }
        }

        private bool CanClean(AIHeroClient hero)
        {
            if (useCleanTime > Utils.TickCount) return false;

            List<BuffType> debuffTypes = new List<BuffType>();

            if (Cleanblind) debuffTypes.Add(BuffType.Blind);
            if (Cleancharm) debuffTypes.Add(BuffType.Charm);
            if (Cleanfear) debuffTypes.Add(BuffType.Fear);
            if (Cleanflee) debuffTypes.Add(BuffType.Flee);
            if (Cleanstun) debuffTypes.Add(BuffType.Stun);
            if (Cleansnare) debuffTypes.Add(BuffType.Snare);
            if (Cleantaunt) debuffTypes.Add(BuffType.Taunt);
            if (Cleansuppression)debuffTypes.Add(BuffType.Suppression);
            if (Cleanpolymorph) debuffTypes.Add(BuffType.Polymorph);
            if (Cleansilence) debuffTypes.Add(BuffType.Silence);
            
            if(debuffTypes.Count > 0)
                foreach (var buff in hero.Buffs)
                {
                    if (debuffTypes.Contains(buff.Type) && buff.IsActive && (buff.EndTime - Game.Time) * 1000 >= CleanBuffTime)
                    {
                        useCleanTime = Utils.TickCount + CleanDelay;
                        return true;
                    }
                }

            if (Cleanexhaust && hero.HasBuff("CleanSummonerExhaust"))
            {
                useCleanTime = Utils.TickCount + CleanDelay;
                return true;
            }
            
            return false;
        }
    }
}