﻿using GodDammAIO.Evade.Spells;
using HesaEngine.SDK.Args;
using HesaEngine.SDK.GameObjects;
using HesaEngine.SDK;
using GodDammAIO.Evade.Helpers;

namespace GodDammAIO.Evade.SpecialSpells
{
    class Ekko : ChampionPlugin
    {
        static Ekko()
        {

        }

        public void LoadSpecialSpell(SpellData spellData)
        {
            if (spellData.spellName == "EkkoR")
            {
                SpellDetector.OnProcessSpecialSpell += ProcessSpell_EkkoR;
            }
        }

        private static void ProcessSpell_EkkoR(Obj_AI_Base hero, GameObjectProcessSpellCastEventArgs args, SpellData spellData,
            SpecialSpellEventArgs specialSpellArgs)
        {
            if (spellData.spellName == "EkkoR")
            {
                foreach (var obj in ObjectManager.Get<GameObject>())
                {
                    if (obj != null && obj.IsValid() && !obj.IsDead && obj.Name == "Ekko" && obj.CheckTeam())
                    {
                        var blinkPos = obj.ServerPosition.To2D();

                        SpellDetector.CreateSpellData(hero, args.Start, blinkPos.To3D(), spellData);
                    }
                }

                specialSpellArgs.noProcess = true;
            }
        }
    }
}