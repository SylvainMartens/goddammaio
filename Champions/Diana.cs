﻿using HesaEngine.SDK;
using HesaEngine.SDK.Enums;
using HesaEngine.SDK.GameObjects;
using SharpDX;
using System;
using System.Linq;
using static GodDammAIO.Configs;

namespace GodDammAIO.Champions
{
    internal class Diana : ChampionScript
    {
        Item Zhonya;

        public Diana()
        {
            Q = new Spell(SpellSlot.Q, 820f);
            W = new Spell(SpellSlot.W, 200f);
            E = new Spell(SpellSlot.E, 420f);
            R = new Spell(SpellSlot.R, 825f);

            Q.SetSkillshot(0.35f, 200f, 1800, false, SkillshotType.SkillshotCircle);

            Zhonya = new Item(3157, 10);

            ChampionMenu = MainMenu.AddSubMenu("Diana");

            ComboMenu = ChampionMenu.AddSubMenu("Combo");
            ComboMenu.Add(new MenuCheckbox("qCombo", "Use Q", true));
            ComboMenu.Add(new MenuCheckbox("wCombo", "Use W", true));
            ComboMenu.Add(new MenuCheckbox("eCombo", "Use E", true));
            ComboMenu.Add(new MenuCheckbox("rCombo", "Use R", true));

            HarassMenu = ChampionMenu.AddSubMenu("Harass");
            HarassMenu.Add(new MenuCheckbox("qHarass", "Use Q", true));
            HarassMenu.Add(new MenuCheckbox("wHarass", "Use W", true));
            HarassMenu.Add(new MenuCheckbox("rHarass", "Use R", true));
            HarassMenu.Add(new MenuSlider("manaHarass", "Harass Mana Percent", 1, 100, 30));

            LaneClearMenu = ChampionMenu.AddSubMenu("Lane Clear");
            LaneClearMenu.Add(new MenuCheckbox("qClear", "Use Q").SetValue(true));
            LaneClearMenu.Add(new MenuCheckbox("wClear", "Use W").SetValue(true));
            LaneClearMenu.Add(new MenuSlider("qMinionHits", "Q Minion Hit Counts", 1, 5, 3));
            LaneClearMenu.Add(new MenuSlider("manaClear", "Clear Mana Percent", 1, 100, 30));

            JungleClearMenu = ChampionMenu.AddSubMenu("Jungle Clear");
            JungleClearMenu.Add(new MenuCheckbox("qJungle", "Use Q", true));
            JungleClearMenu.Add(new MenuCheckbox("wJungle", "Use W", true));
            JungleClearMenu.Add(new MenuSlider("jungleMana", "Jungle Mana Percent", 1, 100, 30));

            LasthitMenu = ChampionMenu.AddSubMenu("Last Hit");
            LasthitMenu.Add(new MenuCheckbox("qLast", "Use Q [Siege Minions]", true));
            LasthitMenu.Add(new MenuSlider("manaLast", "Last Hit Mana Percent", 1, 100, 30));

            ItemsMenu = ChampionMenu.AddSubMenu("Zhonya");
            ItemsMenu.Add(new MenuCheckbox("useZhonya", "Use Zhonya", true));
            ItemsMenu.Add(new MenuSlider("zhonyaMyHp", "If HP <= %", 1, 100, 10));

            KillStealMenu = ChampionMenu.AddSubMenu("Kill Steal");
            KillStealMenu.Add(new MenuCheckbox("ksQ", "KillSteal with Q", true));
            KillStealMenu.Add(new MenuCheckbox("ksR", "KillSteal with R", true));

            MiscMenu = ChampionMenu.AddSubMenu("Misc");
            MiscMenu.Add(new MenuCheckbox("agapcloser", "Anti-Gapcloser [W]!", true));
            MiscMenu.Add(new MenuCheckbox("ainterrupt", "Auto Interrupt [E]!", true));
            MiscMenu.Add(new MenuCheckbox("channelingBroker", "Channeling Spell Broker [E]!", true));
            MiscMenu.Add(new MenuCheckbox("immobileQ", "Auto Cast Immobile Target [Q]!", true));
            MiscMenu.Add(new MenuCheckbox("autoShield", "Auto Shield for Supported Skillshot [W]!", true));

            DrawingsMenu = ChampionMenu.AddSubMenu("Drawings");
            DrawingsMenu.Add(new MenuCheckbox("qDraw", "Q Range", true));
            DrawingsMenu.Add(new MenuCheckbox("wDraw", "W Range", true));
            DrawingsMenu.Add(new MenuCheckbox("eDraw", "E Range", true));
            DrawingsMenu.Add(new MenuCheckbox("rDraw", "R Range", true));
            DrawingsMenu.Add(new MenuCheckbox("hitChanceDraw", "Draw HitChance on Enemy", true));

            ChampionMenu.Add(new MenuCombo("cType", "Combo Method", new StringList(new[] { "Advantage", "Misaya" }, 0)));
            ChampionMenu.Add(new MenuCombo("hType", "Harass Method", new StringList(new[] { "Basic" }, 0)));
            ChampionMenu.Add(new MenuCheckbox("ignite", "Use Smart Ignite").SetValue(true));

            Interrupter.OnInterruptableTarget += Interrupter_OnInterruptableTarget;
            AntiGapcloser.OnEnemyGapcloser += AntiGapcloser_OnEnemyGapcloser;
            Obj_AI_Base.OnProcessSpellCast += AIHeroClient_OnProcessSpellCast;
            Game.OnUpdate += Game_OnUpdate;
            Drawing.OnDraw += Drawing_OnDraw;
        }

        private void Drawing_OnDraw(EventArgs args)
        {
            var menuItem1 = DrawingsMenu.Get<MenuCheckbox>("qDraw").Checked;
            var menuItem2 = DrawingsMenu.Get<MenuCheckbox>("wDraw").Checked;
            var menuItem3 = DrawingsMenu.Get<MenuCheckbox>("eDraw").Checked;
            var menuItem4 = DrawingsMenu.Get<MenuCheckbox>("rDraw").Checked;

            if (menuItem1 && Q.IsReady())
            {
                Drawing.DrawCircle(Player.Position, Q.Range, Color.SpringGreen);
            }
            if (menuItem2 && W.IsReady())
            {
                Drawing.DrawCircle(Player.Position, W.Range, Color.Crimson);
            }
            if (menuItem3 && E.IsReady())
            {
                Drawing.DrawCircle(Player.Position, E.Range, Color.White);
            }
            if (menuItem4 && R.IsReady())
            {
                Drawing.DrawCircle(Player.Position, R.Range, Color.Yellow);
            }
            if (ChampionMenu.Item("hitChanceDraw").GetValue<bool>() && Q.IsReady())
            {
                var enemy = TargetSelector.GetTarget(Q.Range, TargetSelector.DamageType.Magical);
                if (Q.GetPrediction(enemy).Hitchance >= HitChance.High)
                {
                    var yx = Drawing.WorldToScreen(enemy.Position);
                    Drawing.DrawText(yx[0], yx[1], Color.SpringGreen, "Q Hitchance >= High");
                }
            }
        }

        private void Game_OnUpdate()
        {
            if (Orbwalk.ActiveMode == Orbwalker.OrbwalkingMode.Combo)
            {
                Combo();
            }
            if (Orbwalk.ActiveMode == Orbwalker.OrbwalkingMode.Harass)
            {
                Harass();
            }
            if (Orbwalk.ActiveMode == Orbwalker.OrbwalkingMode.LaneClear)
            {
                Jungle();
                LaneClear();
            }
            if (MiscMenu.Get<MenuCheckbox>("channelingBroker").Checked)
            {
                BrokeChannel();
            }
            if (MiscMenu.Get<MenuCheckbox>("immobileQ").Checked)
            {
                ImmobileQ();
            }
            Items();
            KillSteal();
        }

        private void AIHeroClient_OnProcessSpellCast(Obj_AI_Base sender, HesaEngine.SDK.Args.GameObjectProcessSpellCastEventArgs args)
        {
            if (sender == null || args == null || args.Target == null || args.SData == null) return;

            if (MiscMenu.Get<MenuCheckbox>("autoShield").Checked)
            {
                string[] Spells = {"AhriSeduce"
                                          , "InfernalGuardian"
                                          , "EnchantedCrystalArrow"
                                          , "InfernalGuardian"
                                          , "EnchantedCrystalArrow"
                                          , "RocketGrab"
                                          , "BraumQ"
                                          , "CassiopeiaPetrifyingGaze"
                                          , "DariusAxeGrabCone"
                                          , "DravenDoubleShot"
                                          , "DravenRCast"
                                          , "EzrealTrueshotBarrage"
                                          , "FizzMarinerDoom"
                                          , "GnarBigW"
                                          , "GnarR"
                                          , "GragasR"
                                          , "GravesChargeShot"
                                          , "GravesClusterShot"
                                          , "JarvanIVDemacianStandard"
                                          , "JinxW"
                                          , "JinxR"
                                          , "KarmaQ"
                                          , "KogMawLivingArtillery"
                                          , "LeblancSlide"
                                          , "LeblancSoulShackle"
                                          , "LeonaSolarFlare"
                                          , "LuxLightBinding"
                                          , "LuxLightStrikeKugel"
                                          , "LuxMaliceCannon"
                                          , "UFSlash"
                                          , "DarkBindingMissile"
                                          , "NamiQ"
                                          , "NamiR"
                                          , "OrianaDetonateCommand"
                                          , "RengarE"
                                          , "rivenizunablade"
                                          , "RumbleCarpetBombM"
                                          , "SejuaniGlacialPrisonStart"
                                          , "SionR"
                                          , "ShenShadowDash"
                                          , "SonaR"
                                          , "ThreshQ"
                                          , "ThreshEFlay"
                                          , "VarusQMissilee"
                                          , "VarusR"
                                          , "VeigarBalefulStrike"
                                          , "VelkozQ"
                                          , "Vi-q"
                                          , "Laser"
                                          , "xeratharcanopulse2"
                                          , "XerathArcaneBarrage2"
                                          , "XerathMageSpear"
                                          , "xerathrmissilewrapper"
                                          , "yasuoq3w"
                                          , "ZacQ"
                                          , "ZedShuriken"
                                          , "ZiggsQ"
                                          , "ZiggsW"
                                          , "ZiggsE"
                                          , "ZiggsR"
                                          , "ZileanQ"
                                          , "ZyraQFissure"
                                          , "ZyraGraspingRoots"
                                      };
                for (int i = 0; i <= 61; i++)
                {
                    if (args.SData.Name == Spells[i])
                    {
                        if (sender is AIHeroClient && sender.IsEnemy && args.Target.IsMe && !args.SData.IsAutoAttack() && W.IsReady())
                        {
                            W.Cast();
                        }
                    }
                }
            }
        }

        private void AntiGapcloser_OnEnemyGapcloser(ActiveGapcloser gapcloser)
        {
            if (MiscMenu.Get<MenuCheckbox>("agapcloser").Checked)
            {
                if (gapcloser.Sender.IsValidTarget(1000))
                {
                    Drawing.DrawCircle(gapcloser.Sender.Position, gapcloser.Sender.BoundingRadius, Color.Gold, 5);
                    var targetpos = Drawing.WorldToScreen(gapcloser.Sender.Position);
                    Drawing.DrawText(targetpos[0] - 40, targetpos[1] + 20, Color.Gold, "Gapcloser");
                }
                if (W.CanCast(gapcloser.Sender))
                {
                    W.Cast(gapcloser.Sender);
                }
            }
        }

        private void Interrupter_OnInterruptableTarget(AIHeroClient sender, Interrupter.InterruptableTargetEventArgs args)
        {
            if (MiscMenu.Get<MenuCheckbox>("ainterrupt").Checked)
            {
                if (sender.IsValidTarget(1000))
                {
                    Drawing.DrawCircle(sender.Position, sender.BoundingRadius, Color.Gold, 5);
                    var targetpos = Drawing.WorldToScreen(sender.Position);
                    Drawing.DrawText(targetpos[0] - 40, targetpos[1] + 20, Color.Gold, "Interrupt");
                }
                if (E.CanCast(sender))
                {
                    E.Cast(sender);
                }
            }
        }

        private bool ImmobileTarget(AIHeroClient target)
        {
            if (target.HasBuffOfType(BuffType.Stun) || target.HasBuffOfType(BuffType.Snare) || target.HasBuffOfType(BuffType.Knockup) ||
                target.HasBuffOfType(BuffType.Charm) || target.HasBuffOfType(BuffType.Fear) || target.HasBuffOfType(BuffType.Knockback) ||
                target.HasBuffOfType(BuffType.Taunt) || target.HasBuffOfType(BuffType.Suppression) ||
                target.IsStunned)
            {
                return true;
            }
            else
                return false;
        }

        private bool ChanellingChecker(AIHeroClient target)
        {
            if (target.HasBuff("MissFortuneBulletTime") || target.HasBuff("katarinaultibroker") || target.HasBuff("missfortunebulletsound")
                || target.IsChannelingImportantSpell())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void BrokeChannel()
        {
            var enemy = TargetSelector.GetTarget(E.Range, TargetSelector.DamageType.Magical);
            if (enemy != null && E.IsReady() && enemy.IsValidTarget(E.Range) && ChanellingChecker(enemy))
            {
                E.Cast();
            }
        }

        private void ImmobileQ()
        {
            var enemy = TargetSelector.GetTarget(R.Range, TargetSelector.DamageType.Magical);
            if (enemy != null && Q.IsReady() && enemy.IsValidTarget(Q.Range) && ImmobileTarget(enemy))
            {
                if (enemy.Distance(Player.Position) <= 300 && Q.GetPrediction(enemy).Hitchance >= HitChance.High)
                {
                    Q.Cast(enemy.Position + 150);
                }
                else if (enemy.Distance(Player.Position) >= 300 && Q.GetPrediction(enemy).Hitchance >= HitChance.High)
                {
                    Q.Cast(enemy.Position);
                }
            }
        }

        private void Items()
        {
            if (ItemsMenu.Get<MenuCheckbox>("useZhonya").Checked)
            {
                if (Player.HealthPercent <= ItemsMenu.Get<MenuSlider>("zhonyaMyHp").CurrentValue)
                {
                    Zhonya.Cast();
                }
            }
        }

        private void KillSteal()
        {
            foreach (var target in ObjectManager.Heroes.Enemies.Where(x => x.IsValidTarget()).OrderByDescending(x => x.Health))
            {
                if (Q.GetDamage(target) + R.GetDamage(target) + R.GetDamage(target) > target.Health)
                {
                    if (target.Distance(Player.Position) <= Q.Range && Q.GetPrediction(target).Hitchance >= HitChance.High)
                    {
                        Q.Cast(target);
                        if (target.HasBuff("dianamoonlight"))
                        {
                            R.Cast(target);
                        }
                        if (target.Health < R.GetDamage(target))
                        {
                            R.Cast(target);
                        }
                    }
                }
                if (Q.GetDamage(target) > target.Health && Q.GetPrediction(target).Hitchance >= HitChance.High)
                {
                    if (target.Distance(Player.Position) <= 300 && Q.GetPrediction(target).Hitchance >= HitChance.High)
                    {
                        Q.Cast(target.Position + 150);
                    }
                    else if (target.Distance(Player.Position) >= 300 && Q.GetPrediction(target).Hitchance >= HitChance.High)
                    {
                        Q.Cast(target);
                    }
                }
                if (R.GetDamage(target) > target.Health && Player.CountEnemiesInRange(R.Range) <= 1)
                {
                    R.Cast(target);
                }
            }
        }

        private void Combo()
        {
            var useQ = ComboMenu.Get<MenuCheckbox>("qCombo").Checked;
            var useW = ComboMenu.Get<MenuCheckbox>("wCombo").Checked;
            var useE = ComboMenu.Get<MenuCheckbox>("eCombo").Checked;
            var useR = ComboMenu.Get<MenuCheckbox>("rCombo").Checked;
            var useIgnite = ChampionMenu.Get<MenuCheckbox>("ignite").Checked;
            var enemy = TargetSelector.GetTarget(R.Range, TargetSelector.DamageType.Magical);

            if (enemy == null || !enemy.IsValidTarget()) return;

            if (ChampionMenu.Get<MenuCombo>("cType").CurrentValue == 1) // Misaya
            {
                if (Q.IsReady() && useQ && enemy.Distance(Player.Position) <= Q.Range)
                {
                    var prediction = Q.GetPrediction(enemy);
                    if (enemy.Distance(Player.Position) <= 300 && prediction.Hitchance >= HitChance.High)
                    {
                        Q.Cast(prediction.CastPosition + 150);
                    }
                    else if (enemy.Distance(Player.Position) >= 300 && prediction.Hitchance >= HitChance.High)
                    {
                        Q.Cast(prediction.CastPosition);
                    }
                }
                if (R.IsReady() && useR && R.CanCast(enemy) && enemy.HasBuff("dianamoonlight"))
                {
                    R.Cast(enemy);
                }
                if (W.IsReady() && useW && enemy.Distance(Player.Position) <= W.Range)
                {
                    W.Cast();
                }
                if (E.IsReady() && useE && enemy.Distance(Player.Position) <= E.Range - 50)
                {
                    E.Cast();
                }
                if (R.IsReady() && useR && enemy.Distance(Player.Position) <= R.Range &&
                    Player.GetSpellDamage(enemy, SpellSlot.R) >= enemy.Health)// && !enemy.IsZombie)
                {
                    R.Cast(enemy);
                }
                if (enemy.Health <= GetComboDamage(enemy))
                {
                    Player.Spellbook.CastSpell(IgniteSlot, enemy);
                }
            }
            if (ChampionMenu.Get<MenuCombo>("cType").CurrentValue == 0) // Advantage Hikigaya Combo
            {
                if (Q.IsReady() && useQ && enemy.Distance(Player.Position) <= Q.Range)
                {
                    if (Q.GetPrediction(enemy).Hitchance >= HitChance.High)
                    {
                        Q.CastIfHitchanceEquals(enemy, HitChance.High);
                    }
                    /*
                    var prediction = Q.GetPrediction(enemy);
                    if (enemy.Distance(Player.Position) <= 300 && prediction.Hitchance >= HitChance.High)
                    {
                        Q.Cast(prediction.CastPosition);
                    }
                    if (enemy.Distance(Player.Position) >= 300 && Q.GetPrediction(enemy).Hitchance >= HitChance.High)
                    {
                        Q.Cast(prediction.CastPosition);
                    }
                    */
                }
                if (W.IsReady() && useW && enemy.Distance(Player.Position) <= W.Range)
                {
                    W.Cast();
                }
                if (E.IsReady() && useE && enemy.Distance(Player.Position) <= E.Range - 50)
                {
                    E.Cast();
                }
                if (R.IsReady() && useR && enemy.Distance(Player.Position) <= R.Range &&
                    Player.GetSpellDamage(enemy, SpellSlot.R) > enemy.Health && !enemy.IsZombie)
                {
                    R.Cast(enemy);
                }
                if (enemy.Health <= GetComboDamage(enemy))
                {
                    Player.Spellbook.CastSpell(IgniteSlot, enemy);
                }
            }
        }

        private void Harass()
        {
            if (ObjectManager.Player.ManaPercent > HarassMenu.Get<MenuSlider>("manaHarass").CurrentValue)
            {
                if (ChampionMenu.Get<MenuCombo>("hType").CurrentValue == 0) // Basic
                {
                    BasicHarass();
                }
            }
        }

        private void Jungle()
        {
            if (ObjectManager.Player.ManaPercent > JungleClearMenu.Get<MenuSlider>("jungleMana").CurrentValue)
            {
                var mobs = MinionManager.GetMinions(Player.ServerPosition, Q.Range, MinionTypes.All, MinionTeam.Neutral, MinionOrderTypes.MaxHealth);
                if (mobs == null || (mobs != null && mobs.Count == 0))
                {
                    return;
                }
                if (Q.IsReady() && JungleClearMenu.Get<MenuCheckbox>("qJungle").Checked)
                {
                    Q.Cast(mobs[0]);
                }
                if (W.IsReady() && JungleClearMenu.Get<MenuCheckbox>("wJungle").Checked)
                {
                    W.Cast(mobs[0]);
                }
            }
        }

        private void LaneClear()
        {
            var useQ = LaneClearMenu.Get<MenuCheckbox>("qClear").Checked;
            var useW = LaneClearMenu.Get<MenuCheckbox>("wClear").Checked;
            var minionCount = LaneClearMenu.Get<MenuSlider>("qMinionHits").CurrentValue;

            if (ObjectManager.Player.ManaPercent > LaneClearMenu.Get<MenuSlider>("manaClear").CurrentValue)
            {
                var mins = MinionManager.GetMinions(1000);

                if (mins.Count <= 0)
                {
                    return;
                }

                if (Q.IsReady() && useQ)
                {
                    var qPos = Q.GetCircularFarmLocation(mins);

                    if (qPos.MinionsHit >= minionCount)
                        Q.Cast(qPos.Position);
                }

                if (W.IsReady() && useW)
                {
                    if (mins.OrderBy(x => x.Distance(Player.Position)).FirstOrDefault().Distance(Player.Position) <= W.Range)
                    {
                        W.Cast();
                    }
                }
            }
        }

        private void LastHit()
        {
            if (ObjectManager.Player.ManaPercent > LasthitMenu.Get<MenuSlider>("manaLast").CurrentValue)
            {
                if (Q.IsReady() && LasthitMenu.Get<MenuCheckbox>("qLast").Checked)
                {
                    var qMinion = MinionManager.GetMinions(ObjectManager.Player.ServerPosition, Q.Range, MinionTypes.All, MinionTeam.Enemy);
                    foreach (var minyon in qMinion)
                    {
                        if (minyon.CharData.BaseSkinName.Contains("MinionSiege"))
                        {
                            if (Q.IsKillable(minyon))
                            {
                                Q.CastOnUnit(minyon);
                            }
                        }
                    }
                }
            }
        }

        private void BasicHarass()
        {
            var useQ = HarassMenu.Get<MenuCheckbox>("qHarass").Checked;
            var useW = HarassMenu.Get<MenuCheckbox>("wHarass").Checked;
            var useR = HarassMenu.Get<MenuCheckbox>("rHarass").Checked;
            var enemy = TargetSelector.GetTarget(R.Range, TargetSelector.DamageType.Magical);

            if (enemy != null && Q.IsReady() && useQ && enemy.IsValidTarget(Q.Range, true))
            {
                if (enemy.Distance(Player.Position) <= 300 && Q.GetPrediction(enemy).Hitchance >= HitChance.High)
                {
                    Q.Cast(enemy.Position + 150);
                }
                if (enemy.Distance(Player.Position) >= 300 && Q.GetPrediction(enemy).Hitchance >= HitChance.High)
                {
                    Q.Cast(enemy);
                }
            }
            if (R.IsReady() && useR && enemy.Distance(Player.Position) <= R.Range && enemy.HasBuff("dianamoonlight"))
            {
                R.Cast(enemy);
            }
            if (W.IsReady() && useW && enemy.Distance(Player.Position) < W.Range - 50)
            {
                W.Cast();
            }
        }

        private float GetComboDamage(AIHeroClient hero) // Q+W+R+AA2+LICH+PASSIVE
        {
            var damage = 0d;
            if (Q.IsReady())
            {
                damage += Q.GetDamage(hero);
            }
            if (W.IsReady())
            {
                damage += W.GetDamage(hero);
            }
            if (R.IsReady())
            {
                damage += R.GetDamage(hero);
            }
            damage += Player.GetAutoAttackDamage(hero, true) * 2;
            if (Player.HasBuff("dianaarcready"))
            {
                damage += 15 + 5 * Player.Level;
            }
            if (Player.HasBuff("LichBane"))
            {
                damage += Player.BaseAttackDamage * 0.75 + Player.FlatMagicDamageMod * 0.5;
            }
            return (float)damage;
        }

    }
}