﻿using HesaEngine.SDK;

namespace GodDammAIO.Activators
{
    internal class Activator
    {
        static Menu Menu;

        CleanseActivator Cleanse;
        OffensivesItemActivator OffensiveItems;
        PotionsActivator Potions;
        SummonersActivator Summoners;

        public Activator(Menu menu)
        {
            Menu = menu;

            Cleanse = new CleanseActivator(Menu);
            OffensiveItems = new OffensivesItemActivator(Menu);
            Potions = new PotionsActivator(Menu);
            Summoners = new SummonersActivator(Menu);
        }
    }
}