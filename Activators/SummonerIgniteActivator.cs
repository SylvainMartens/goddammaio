﻿using HesaEngine.SDK;
using SharpDX;
using SharpDX.DirectInput;
using System;
using System.Linq;
using static GodDammAIO.Configs;

namespace GodDammAIO.Activators
{
    internal class SummonerIgniteActivator
    {
        Menu IgniteMenu;
        Menu TargetMenu;

        bool Enabled = true;
        bool UseToKS = true;
        bool DrawIgnite = true;
        int MinimumPercent = 25;

        Spell IgniteSpell;

        public SummonerIgniteActivator(Menu menu)
        {
            if (IgniteSlot == HesaEngine.SDK.Enums.SpellSlot.Unknown) return;
            IgniteMenu = menu.AddSubMenu("Ignite");
            IgniteMenu.Add(new MenuKeybind("Enabled", "Enabled", new KeyBind(Key.Space, MenuKeybindType.Hold, Enabled))).OnValueChanged += (sender, value) => { Enabled = value; };
            IgniteMenu.Add(new MenuCheckbox("UseToKS", "Use to KS ( Bypass Enabled )", UseToKS)).OnValueChanged += (sender, value) => { UseToKS = value; };
            IgniteMenu.Add(new MenuCheckbox("DrawIgnite", "Draw Range", UseToKS)).OnValueChanged += (sender, value) => { DrawIgnite = value; };

            TargetMenu = IgniteMenu.AddSubMenu("Target");

            foreach (var hero in ObjectManager.Heroes.Enemies)
            {
                TargetMenu.Add(new MenuCheckbox("Ignite_" + hero.ChampionName.ToLower(), "Ignite " + hero.ChampionName, true));
            }
            TargetMenu.Add(new MenuSlider("MinimumPercent", "HP % <=", 1, 100, MinimumPercent)).OnValueChanged += (sender, value) => { MinimumPercent = value; };

            IgniteSpell = new Spell(IgniteSlot, 550f);

            Drawing.OnDraw += Drawing_OnDraw;
            Game.OnUpdate += Game_OnUpdate;
        }

        private void Game_OnUpdate()
        {
            if (!IgniteSpell.IsReady() || !Player.CanCast) return;

            if (UseToKS)
            {
                var t = ObjectManager.Heroes.Enemies.FirstOrDefault(p => !p.IsDead && p.ServerPosition.Distance(ObjectManager.Player.ServerPosition) < IgniteSpell.Range && MinionHealthPrediction.GetHealthPrediction(p, 250) < GetDamage());
                if (t != null)
                    Player.Spellbook.CastSpell(IgniteSlot, t);
            }

            if (Enabled && IgniteSpell.IsReady())
            {
                var t = TargetSelector.GetTarget(IgniteSpell.Range, TargetSelector.DamageType.True);
                if (t != null && t.IsValidTarget() && t.HealthPercent <= MinimumPercent)
                    Player.Spellbook.CastSpell(IgniteSlot, t);
            }
        }

        private void Drawing_OnDraw(EventArgs args)
        {
            if(DrawIgnite && IgniteSpell.IsReady())
            {
                Drawing.DrawCircle(Player.Position, IgniteSpell.Range, Color.OrangeRed);
            }
        }

        public int GetDamage()
        {
            return 50 + (20 * Player.Level);
        }
    }
}