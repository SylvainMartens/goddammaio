﻿using HesaEngine.SDK;

namespace GodDammAIO.Awareness
{
    internal class Awareness
    {
        internal static Menu Menu;

        Trackers.CooldownTracker Cooldown;
        Trackers.TowersTracker Towers;
        Trackers.WardTracker Wards;

        public Awareness(Menu menu)
        {
            Menu = menu;

            Cooldown = new Trackers.CooldownTracker();
            Towers = new Trackers.TowersTracker();
            Wards = new Trackers.WardTracker();
        }
    }
}